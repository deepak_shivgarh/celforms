<?php

function get_tables($dataObject){ 
    $tables = array();
    foreach ($dataObject->data as $key => $value) { 
        $tables[] =  $key; 
    }
    return $tables;
}

function get_properties($dataObject, $table){ 
    $columns = array();
    foreach ($dataObject->data->$table->Columns as $key => $value) {
      $columns[] = $value;  
    }
    return $columns;
}

function get_properties_html($propertiesDataObject, $tableName, $dataSelected) { 
    $body = '';
    foreach ($propertiesDataObject as $value) { 
        $selectedPhpName = explode(".", $value->phpName)[1];
        $selected = ($value->phpName == $dataSelected ? 'selected="selected"' : '');
        $body .= '<option value="' . $value->phpName . '" type="' . $value->type . '" '.$selected.' >' . str_replace("_", "", $value->columnName) . '</option>'; 
    }
    return $body;
}

function getPropertiesJsonHtml($columnsDataObject, $tableName, $dataSelected) {   
    $body=$selected=""; 
    if( !empty($columnsDataObject) ) {
        foreach ($columnsDataObject as $value) { 
        $selected = ($value->phpName==$dataSelected ? 'selected="selected"' : ""); 
        if(strpos($value->phpName, '.') != false) 
          $body .= '<option '.$selected.' value="'.$value->phpName.'" '.$selected.'>'. substr(strrchr($value->phpName, "."), 1).'</option>'; 
        } 
    } 
    $body .= '<option value="'.str_replace('_', '', ucwords($tableName, "_")).'.'.str_replace('_', '', ucwords($tableName, "_")).(strtolower($tableName)=='job' ? 'Dataset' : 'Attributes').'" type="Other" '.($selected==".."?'selected="selected"':"").'>Other</option>'; 
    return $body;
}

function getAlertBody($class, $message) {
    $alert  =   '<div class="alert alert-'.$class.'">
                    '.$message.'
                </div>'; 
    return $alert;
}

function generateAdminAlert($type, $messageCode) {
    if ($type == 'S') {
        $flash_msg = getAlertBody("success", getAlertMessage($messageCode));
    } elseif ($type == 'I') {
        $flash_msg = getAlertBody("info", getAlertMessage($messageCode));
    } elseif ($type == 'W') {
        $flash_msg = getAlertBody("warning", getAlertMessage($messageCode));
    } elseif ($type == 'D') {
        $flash_msg = getAlertBody("danger", getAlertMessage($messageCode));
    }

    return $flash_msg;
}

function getCustomAlert($type, $message){
    if($type == 'S') {
        $flash_msg = getAlertBody("success", $message);
    } elseif ($type == 'I') {
        $flash_msg = getAlertBody("info", $message);
    } elseif ($type == 'W') {
        $flash_msg = getAlertBody("warning", $message);
    } elseif ($type == 'D') {
        $flash_msg = getAlertBody("danger", $message);
    }

    return $flash_msg;
} 

# METHODS USED IN JOBMASTER EDIT (quesRender.php)
# REGION STARTS
function get_input_structure($questionJson, $divId) { 
    $ques_body = '';
    $index = ''; 
    $libody = '';
    $inputType = $questionJson->Type; 
    switch($inputType) { 
        case TEXT:
            $ques_body .= '<ul  name="Length" class="length" data-type="">'; 
            $ques_body .= '<li data-min="'.($questionJson->Length != 'false' ? $questionJson->Length->Min : '').'" data-max="'.($questionJson->Length != 'false' ? $questionJson->Length->Max : '').'" name="Length"></li>'; 
            $ques_body .= '</ul>'; 
            return '<li class="hide">'.$ques_body.'</li>';
            break;
        case NUMERIC:
            $ques_body .= '<li class="hide">
                                <ul  name="Range" class="range" data-type="">  
                                    <li data-min="'.($questionJson->Range != 'false' ? $questionJson->Range->Min : '').'" data-max="'.($questionJson->Range != 'false' ? $questionJson->Range->Max : '').'" name="Range"></li> 
                                </ul>
                            </li>
                            <li class="hide">
                                <ul name="Validation" class="validation" data-type="">
                                    <li data-validation-kind="" data-condition="'.(isset($questionJson->Validation) ? ($questionJson->Validation != 'false' ? $questionJson->Validation->Condition : ''):'').'" 
                                    data-validatewith="'.(isset($questionJson->Validation) ? ($questionJson->Validation != 'false' ? $questionJson->Validation->ValidateWith : ''):'').'" 
                                    id="property_validation'.$divId.'" 
                                    data-operation="'.(isset($questionJson->Validation) ? ($questionJson->Validation != 'false' ? $questionJson->Validation->Operation : ''):'').'" 
                                    data-value="'.(isset($questionJson->Validation) ? ($questionJson->Validation != 'false' ? $questionJson->Validation->Value : ''):'').'"
                                     name="Validation"></li>
                                </ul>
                            </li>
                            <li class="controlKind property_kind hide" id="kind'.$divId.'" name="Kind">'.$questionJson->Kind.'</li>'; 
            return $ques_body;
            break;
        case SELECT_ONE:
        case SELECT_MANEY:
        case YES_NO:
            $selectType = ($inputType==SELECT_MANEY ? "checkbox" : "radio");
            $ques_body .= '<ul class="selection-translations" name="Options" data-type="AR"><span class="hide">Options</span>';
            $index = 1;
            if((array_key_exists('Options', $questionJson))){
              foreach ($questionJson->Options as $Option) {
                $selected = $Option->Default=='true' ? 'checked="checked"' :''; 
                $optionImage = ($inputType==YES_NO ? '' : '<div class="hide optionImage optionImage_'.$index.'">'.(isset($Option->mediaImage)?$Option->mediaImage:"").'</div>');
                $ques_body .= '<li name="answer_'.$index.'" class="answer answer_'.$index.'"><input type="'.$selectType.'" disabled '.$selected.' data-text="'.$Option->Text.'" default="'.$Option->Default.'" value="'.$Option->Value.'">'.$optionImage.'<span class="value">'.$Option->Text.'</span></li>';
                $index++;
              }
             } 
            $ques_body .= '</ul>'; 
            return '<li class="isShowUnderlying hide" name="IsShowUnderlying">'.$questionJson->IsShowUnderlying.'</li><li class="clearfix">'.$ques_body.'</li>';
            break; 
        case DATE:   
            $ques_body .= '<li class="isSetCurrentDate hide" id="isSetCurrentDate'.$divId.'" name="isSetCurrentDate">false</li>
                            <li class="hide">
                                <ul  name="Range" class="range" data-type="">  
                                    <li data-min="'.($questionJson->Range != 'false' ? $questionJson->Range->Min : '').'" data-max="'.($questionJson->Range != 'false' ? $questionJson->Range->Max : '').'" name="Range"></li> 
                                </ul>
                            </li>
                            <li class="hide">
                                <ul name="Validation" class="validation" data-type="">
                                    <li data-validation-kind="'.(isset($questionJson->Validation) ? ($questionJson->Validation != 'false' ? $questionJson->Validation->ValidationKind : ''):'').'" 
                                    data-condition="'.($questionJson->Validation != 'false' ? $questionJson->Validation->Condition : '').'" 
                                    data-validatewith="'.($questionJson->Validation != 'false' ? $questionJson->Validation->ValidateWith : '').'" 
                                    id="property_validation'.$divId.'" 
                                    data-operation="'.($questionJson->Validation != 'false' ? $questionJson->Validation->Operation : '').'" 
                                    data-value="'.($questionJson->Validation != 'false' ? $questionJson->Validation->Value : '').'"
                                     name="Validation"></li>
                                </ul>
                            </li>
                            <li class="controlKind property_kind hide" id="kind'.$divId.'" name="Kind">'.$questionJson->Kind.'</li> 
                            <li class="controlKind date-time-format hide" id="dateFormat'.$divId.'" name="DateFormat">'.$questionJson->DateFormat.'</li>
                            <span class="datebody" id="datebody'.$divId.'">'.getDateTime($divId,$questionJson->Kind, $questionJson->DefaultValue).'</span>';
            return $ques_body;
            break;
        case AUDIO: 
            $ques_body = '<span class="capture">Capture Audio</span>'; 
            return $ques_body;
            break;
        case VIDEO: 
            $ques_body = '<span class="capture">Capture Video</span>'; 
            return $ques_body;
            break;
        case IMAGE: 
            $ques_body = '<span class="capture">Capture Image</span>'; 
            return $ques_body;
            break;
        case LOCATION: 
            $ques_body = '<span class="capture">Capture Location</span>'; 
            return $ques_body;
            break;
        case LABEL:  
            $tag = (!empty($questionJson->Kind)?$questionJson->Kind:"");  
            $libody .= '<li class="controlKind property_kind hide" id="kind'.$divId.'" name="Kind">'.(!empty($questionJson->Kind)?$questionJson->Kind:"").'</li><li class="controlHint property_label_image hide" name="'.(!empty($questionJson->Kind)?$questionJson->Kind:"mediaImage").'">'.(!empty($questionJson->mediaImage)?$questionJson->mediaImage:"").'</li><span class="labelImage">';  
            if(!empty($questionJson->Kind)) {
                if($tag=='mediaAudio') {
                    $libody .= '<audio controls><source src="'.$questionJson->mediaImage.'" type="audio/mp3"></audio>'; 
                } elseif($tag=='mediaVideo') {
                    $libody .= '<video controls><source src="'.$questionJson->mediaImage.'" type="video/mp4"></video>'; 
                } elseif($tag=='mediaImage') {
                    $libody .= '<img src="'.$questionJson->$tag.'">'; 
                } 
            } 
            $libody .= '</span>'; 
            return $libody;
            break; 
    }
}

function getDateTime($position, $type, $defaultValue) { 
    $a = strpos($defaultValue,"AM");
    $b = strpos($defaultValue,"PM");
    $defaultValue = trim( str_replace( "AM", "", $defaultValue ) );
    $defaultValue = trim( str_replace( "PM", "", $defaultValue ) );

    $dateBody = ''; 
    if( !empty( $defaultValue ) ) {
        $mm = date( 'M', strtotime( $defaultValue ) );
        $dd = date( 'd', strtotime( $defaultValue ) );
        $yy = date( 'Y', strtotime( $defaultValue ) );
        $hh = date( 'H', strtotime( $defaultValue ) );
        $min= date( 'i', strtotime( $defaultValue ) );
        $sec = "00";
        if( $a!='' || $b!='' ) {
            $ampm = date('A', strtotime($defaultValue));
        } else {
            $ampm = '';
        }
    } else {
        $mm = date('M', time());
        $dd = date('d', time());
        $yy = date('Y', time());
        $hh = date('H', time());
        $min = date('i', time());
        $sec = "00";
        $ampm = date('A', time());
    }
    switch($type){
      case 'Date': 
        $dateBody .= '<div class="datecell"><span class="dd">'.$mm.'</span> <span class="dd">'.$dd.'</span> <span class="dd">'.$yy.'</span></div>';
        break;
      case 'Time': 
        $dateBody .= '<div class="timecell"><span class="td">'.$hh.'</span> <span class="td">'.$min.'</span> <span class="td">'.$sec.'</span> '.(!empty($ampm)   ? '<span class="td">'.$ampm.'</span>' : '').'</div>';
        break;
      case 'DateTime': 
        $dateBody .= '<div class="datetimecell"><span class="dtd">'.$mm.'</span> <span class="dtd">'.$dd.'</span> <span class="dtd">'.$yy.'</span> <span class="dtd">'.$hh.'</span> <span class="dtd">'.$min.'</span> <span class="dtd">'.$sec.'</span> '.(!empty($ampm)   ? '<span class="td">'.$ampm.'</span>' : '').'</div>';
        break;
      default: 
        $dateBody .= '<div class="datetimecell"><span class="dtd">'.$mm.'</span> <span class="dtd">'.$dd.'</span> <span class="dtd">'.$yy.'</span> <span class="dtd">'.$hh.'</span> <span class="dtd">'.$min.'</span> <span class="dtd">'.$sec.'</span> '.(!empty($ampm)   ? '<span class="td">'.$ampm.'</span>' : '').'</div>';
        break;
    }  
    return $dateBody;
}

function getRelevanceUlBody($rcdata) { 
    $htmlBody="";  
    $increment = 1;
    foreach ($rcdata as $index => $elem) {  
        $ul = ""; 
        $isJoin = 'join="'.(!array_key_exists('Join', (array) $elem) ? '' : $elem->Join).'"';
        if ($elem->Child=="false") {
            $htmlBody .= '<li name="level_'.$increment.'" class="level_'.$increment.'" TableMapping="'.$elem->Tablemapping.'" Condition="'.(isset($elem->Condition) ? $elem->Condition : '').'" Value="'.$elem->Value.'" isConstant="'.(isset($elem->Constant) ? $elem->Constant : '').'" isOther="'.(isset($elem->Other) ? $elem->Other : '').'" kind="'.(isset($elem->Kind) ? $elem->Kind : '').'" '.$isJoin.'></li>'; 
            $increment++;
        } else {
            $ulData = getChildUlBody($elem->Child, $increment+1); 
            $ul .= $ulData['resultBody']; 
            $htmlBody .= '<li name="level_'.$increment.'" class="level_'.$increment.'" TableMapping="'.$elem->Tablemapping.'" Condition="'.(isset($elem->Condition) ? $elem->Condition : '').'" Value="'.$elem->Value.'" isConstant="'.(isset($elem->Constant) ? $elem->Constant : '').'" isOther="'.(isset($elem->Other) ? $elem->Other : '').'" kind="'.(isset($elem->Kind) ? $elem->Kind : '').'" '.$isJoin.'><ul>'.$ul.'</ul></li>'; 
            $increment = $ulData['lastIndex'];
        } 
    }
    return $htmlBody;
}

function getChildUlBody($rcdata, $position) { 
    $htmlBody="";  
    $increment = $position; 
    foreach ($rcdata as $index => $elem) {  
        $ul = ""; 
        $isJoin = 'join="'.(!array_key_exists('Join', (array) $elem) ? '' : $elem->Join).'"';
        if ($elem->Child=="false") {
            $htmlBody .= '<li name="level_'.$increment.'" class="level_'.$increment.'" TableMapping="'.$elem->Tablemapping.'" Condition="'.(isset($elem->Condition) ? $elem->Condition : '').'" Value="'.$elem->Value.'" isConstant="'.(isset($elem->Constant) ? $elem->Constant : '').'" isOther="'.(isset($elem->Other) ? $elem->Other : '').'" kind="'.(isset($elem->Kind) ? $elem->Kind : '').'" '.$isJoin.'></li>'; 
            $increment++;
        } else {
            $ulData = getChildUlBody($elem->Child, $increment+1); 
            $ul .= $ulData['resultBody']; 
            $htmlBody .= '<li name="level_'.$increment.'" class="level_'.$increment.'" TableMapping="'.$elem->Tablemapping.'" Condition="'.(isset($elem->Condition) ? $elem->Condition : '').'" Value="'.$elem->Value.'" isConstant="'.(isset($elem->Constant) ? $elem->Constant : '').'" isOther="'.(isset($elem->Other) ? $elem->Other : '').'" kind="'.(isset($elem->Kind) ? $elem->Kind : '').'" '.$isJoin.'><ul>'.$ul.'</ul></li>'; 
            $increment = $ulData['lastIndex'];
        }
    }
    $arr['resultBody']=$htmlBody;
    $arr['lastIndex']=$increment;
    return $arr;
}

function getConditionUlBody($kind, $calculation) { 
    $htmlBody="";  
    $increment = 1;
    foreach ($calculation as $index => $elem) {  
        $ul = ""; 
        $isJoin = 'join="'.(!array_key_exists('Join', (array) $elem) ? '' : $elem->Join).'"';
        if($kind=="DateTime") {
        } elseif($kind=="Numeric") {

        } elseif($kind=="Text") {
            $htmlBody .= '<li class="datetimeTablemapping clevel_'.$increment.'" data-condition="'.($i==0 ? $elem->Tablemapping : "").'" data-operation="+" data-calculation-kind="" data-value="'.($elem->Constant=="true"?"Constant":$elem->Value).'" data-constant="'.($elem->Constant=="true"?$elem->Value:"").'" data-validatewith="" id="property_calculation_'.$increment.'" name="Calculation"></li>';
            $increment++;
        } 
    }
    return $htmlBody;
}

function getConditionChildUlBody($rcdata, $position) { 
    $html="";  
    $increment = $position; 
    foreach ($rcdata as $index => $elem) {  
        $ul = ""; 
        $isJoin = 'join="'.(!array_key_exists('Join', (array) $elem) ? '' : $elem->Join).'"';
        if($elem->Kind=="DateTime") {

        } elseif($elem->Kind=="Numeric") {

        } elseif($elem->Kind=="Numeric") {

        }
        if($elem->Child=="false") {  
            $htmlBody .= '<li class="datetimeTablemapping clevel_'.$increment.'" data-condition="'.$elem->Tablemapping.'" data-operation="" data-calculation-kind="" data-value="" data-validatewith="" id="property_calculation'.$increment.'" name="Calculation"></li>';
            $increment++;
        } else {
            $ulData = getConditionChildUlBody($elem->Child, $increment+1); 
            $ul .= $ulData['resultBody'];  
            $htmlBody .= '<li class="datetimeTablemapping clevel_'.$increment.'" data-condition="" data-operation="" data-calculation-kind="" data-value="" data-validatewith="" id="property_calculation'.$increment.'" name="Calculation"></li>';
            $increment = $ulData['lastIndex'];
        } 
    }
    $arr['resultBody']=$html;
    $arr['lastIndex']=$increment;
    return $arr;
}
# REGION ENDS

function sortMultidimentionalArrayByKey($array, $on, $order=SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        } 
        return reindexArray($new_array);
}

function reindexArray($array) {
    return array_values(array_filter($array));
}