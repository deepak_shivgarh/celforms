<section class="sidebar">
	<!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">  
        <li class="<?php echo ($index=='projects' ? 'active' : ''); ?>">
        	<a href="<?php echo site_url('projects');?>"><i class="manageprojects"></i><span>Manage Projects</span></a>
        </li>
        <li class="<?php echo ($index=='Manage Job Master' ? 'active' : ''); ?>">
        	<a href="<?php echo site_url('forms');?>"><i class="managejobmaster"></i><span>Manage Job Masters</span></a>
        </li>
        <!-- input type -->
        <?php if($index=='jobMaster'){ ?>
        <li>
          <a class="toolBarGroupName"><span>Input</span></a>
        </li> 
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputLabel" data-pos="1" rel="inputLabel"><i class="inputLabel_png"></i><span>Label</span></a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputSelectOne" data-pos="2" rel="inputSelectOne"><i class="inputSelectOne_png"></i><span>Choose One</span>
          </a>
        </li> 
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputSelectMany" data-pos="3" rel="inputSelectMany"><i class="inputSelectMany_png"></i><span>Select Multiple</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputNumeric" data-pos="4" rel="inputNumeric"><i class="inputNumeric_png"></i><span>Numeric</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputText" data-pos="5" rel="inputText"><i class="inputText_png"></i><span>Text</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputDate" data-pos="6" rel="inputDate"><i class="inputDate_png"></i><span>Date/Time</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputLocation" data-pos="7" rel="inputLocation"><i class="inputLocation_png"></i><span>GPS Location</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputYesNo" data-pos="8" rel="inputYesNo"><i class="inputYesNo_png"></i><span>Yes/No</span>
          </a>
        </li> 
        <li>
          <a class="toolBarGroupName"><span>Media</span></a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputAudio" data-pos="9" rel="inputAudio"><i class="inputAudio_png"></i><span>Audio</span>
          </a>
        </li>
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputImage" data-pos="10" rel="inputImage"><i class="inputImage_png"></i><span>Image</span>
          </a>
        </li> 
        <li>
          <a class="toolButton ui-draggable_ curp" id="inputVideo" data-pos="11" rel="inputVideo"><i class="inputVideo_png"></i><span>Video</span>
          </a>
        </li>
        <?php } ?>

    </ul>
</section>