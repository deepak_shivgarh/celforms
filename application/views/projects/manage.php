<?php
$statusArray = ['250ceb7584393b9176f8263811e60000' => 'OPEN',
    '250ceb7584393b917c20263811e60000' => 'CLOSED',
    '250ceb7584393b917efa263811e60000' => 'DISPATCHED',
    '250ceb7584393b91813e263811e60000' => 'RECEIVED',
    '250ceb7584393b918382263811e60000' => 'INITIATED',
    '250ceb7584393b9185b2263811e60000' => 'COMPLETED',
    '250ceb7584393b9187e2263811e60000' => 'ACTIVE',
    '250ceb7584393b918a12263811e60000' => 'SUSPENDED',
    '250ceb7584393b918c60263811e60000' => 'INACTIVE',
    '250ceb75843993ca4ca8265311e60000' => 'DELETED'
];
?> 
<div class="content-wrapper">
    <section class="content-header">
        <h1> Projects <small>manage</small> </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Projects</li>
        </ol>
    </section>
    <section class="content">
        <?php echo $this->session->flashdata('message'); ?>
        <div class="row">
            <div class="col-xs-12"> 
                <div class="box-body">
                    <a class="curp btn btn-success pull-right" data-toggle="modal" data-target="#add-duplicate-project-modal">Add Duplicate Project</a>
                    <a href="<?php echo base_url('projects/add'); ?>" class="curp btn btn-primary pull-right" style="margin-right: 10px;">Add New Project</a><br><br> 
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Project Id</th>
                                <th>Project Name</th>
                                <th>Status</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                            $i = 1;
                            foreach ($projectList->data as $key => $project) {
                                ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $project->ProjectEntityId ?></td>
                                    <td><a href="<?php echo base_url('projects/navigateUserToLinkPage/') . '/' . $project->ProjectEntityId . '/' . $project->ProjectName; ?>"><?php echo $project->ProjectName; ?></a></td>
                                    <td><?php echo $statusArray[$project->StatusStatusEntityId] ?></td>
                                    <td><a href="<?php echo base_url('projects/add/' . $project->ProjectEntityId); ?>" >Edit</a></td> 
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false,
            "pageLength": 100
        });
    });
</script>


<script type="text/javascript">
    function changeStatus(mid, status)
    {
        var table = 'merchant_master';
        if (status == '3') {
            var res = confirm('Are you sure you want to delete this entry?');
            if (res === false) {
                return false;
            }
        }

        $.post("<?php echo base_url('changeStatus') ?>", {id: mid, status: status, table: table}, function (response) {
           
            if (response == '1') {
                if (status == '1') {
                    var updateFunction = "return changeStatus('" + mid + "', 2)";
                    $('#status_' + mid).html('<a onClick="' + updateFunction + '" class="btn btn-xs btn-warning" title="Inactivate">Inactive</a>');
                } else if (status == '2') {
                    var updateFunction = "return changeStatus('" + mid + "', 1)";
                    $('#status_' + mid).html('<a  onClick="' + updateFunction + '" class="btn btn-success btn-xs" title="Active">Active</a>');
                } else if (status == '3') {
                    $('#merchant_' + mid).remove();
                }
            }
        });
    }
</script>

<div id="add-duplicate-project-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="border: 1px solid #d2d6de;border-radius: 5px;">
            <div class="modal-header" style="color: #fff;background-color: #337ab7;border-color: #337ab7;margin: -1px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Duplicate Project</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal add-duplicate-project-form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="projectName">Project Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" date-key="ProjectName" id="projectName" placeholder="Enter Project Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="projectStatus">Project Status:</label>
                        <div class="col-sm-10"> 
                            <select class="form-control" id="projectStatus" date-key="ProjectStatus">
                                <option value="">--SELECT STATUS--</option>
                                <option value="OPEN">OPEN</option>
                                <option value="ACTIVE">ACTIVE</option>
                                <option value="SUSPENDED">SUSPENDED</option>
                                <option value="COMPLETED">COMPLETED</option>                                 
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="projectTemplate">Project Template:</label>
                        <div class="col-sm-10"> 
                            <select class="form-control" id="projectTemplate" date-key="ProjectEntityId">
                                <option value="">--SELECT TEMPLATE--</option>
                                <?php
                                foreach ($projectList->data as $key => $project) {
                                    echo "<option value='" . $project->ProjectEntityId . "'>" . $project->ProjectName . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-primary add-duplicate-project-button" id="add-duplicate-project-button">Add Project</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel-duplicate-project-button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>          
        </div>
    </div>
</div>
<style>
    .form-invalid:focus{
        border: 1px solid red;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $("body").on("click", ".add-duplicate-project-button", function () {
            var validationFalseFlag = true;
            var projectAddJsonTemplate = {
                "ProjectEntityId": "",
                "ProjectName": "",
                "ProjectStatus": "",
                "ProjectAttributes": {
                    "AllowableProperties": [],
                    "EligibilityCriteria": [],
                    "ClosureCriteria": []
                },
                "Token": ""
            };
            $(".add-duplicate-project-form").find(":input").each(function () {
                $(this).removeClass("form-invalid");
                if ($.trim(this.value) === '' && this.id !== "add-duplicate-project-button" && this.id !== "cancel-duplicate-project-button") {
                    validationFalseFlag = false;
                    $(this).focus().addClass("form-invalid");
                    return false;
                }
            });
            if (validationFalseFlag) {
                $(".add-duplicate-project-form").find(":input").each(function () {
                    if (this.id !== "add-duplicate-project-button" && this.id !== "cancel-duplicate-project-button") {
                        projectAddJsonTemplate[$(this).attr("date-key")] = $(this).val();
                    }
                });
                projectAddJsonTemplate = JSON.stringify(projectAddJsonTemplate);
                showLoader();
                $.post(base_url + 'ajax/projectAddDuplicate', {action: 'addDuplicate', requestJson: projectAddJsonTemplate}).done(function (data) {
                    hideLoader();
                    console.log(data);
                    window.location.href = base_url + 'projects';
                });
            }
        });
    });
</script>