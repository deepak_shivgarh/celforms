<div class="content-wrapper" style="min-height: 916px;">
    <section class="content">
        <div class="row">
            <h3 style="margin-left: 16px;">Add Project</h3>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs wizardinfo">
                        <li class="active tab1"><a>Basic Info</a></li>
                        <li class="tab2"><a>Eligibility Criteria</a></li> 
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="basicinfo">
                            <form class="box-body form-horizontal" id="basicinfo_form" data-node="" multilevel="false">
                                <input type="hidden" id="createdAt" data-key="createdAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
                                <input type="hidden" id="LastUpdatedAt" data-key="LastUpdatedAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
                                <div class="form-group">
                                    <label for="projectName" class="col-sm-2 control-label">Project Name <span>*</span></label> 
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control project-name" id="projectName" data-key="ProjectName" is-object="true" placeholder="Project Name" value="<?php echo!empty($projectData) ? $projectData->ProjectName : ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ProjectStatus" class="col-sm-2 control-label">Status <span>*</span></label> 
                                    <div class="col-sm-6">
                                        <select class="form-control" data-key="StatusStatusEntityId" is-object="true" id="StatusStatusEntityId">
                                            <?php
                                            foreach ($allStatusList as $k => $v) {
                                                if (in_array($v->StatusName, $allowedStatus)) {
                                                    echo '<option value="' . $v->StatusEntityId . '">' . $v->StatusName . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> 
                            </form> 
                            <div class="row">
                                <div class="col-xs-12"> 
                                    <div style="width: 707px;">
                                        <a href="#criteria" data-toggle="tab" class="basicinfo"></a>
                                        <a class="curp btn btn-primary pull-right validate" id="validate-project-initials" container="basicinfo" data-id="tab2">Next >></a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="criteria"> 
                            <form class="box-body form-horizontal" id="criteria_form" data-node="ProjectAttributes.EligibilityCriteria" data-type="AR" multilevel="true">
                                <div class="border-bottom ec-container" id="level_0">
                                    <div class="form-group table-name-input">
                                        <label for="tableName" class="col-sm-2 control-label">Select Table <span>*</span></label> 
                                        <div class="col-sm-10"> 
                                            <select class="form-control tableName" data-key="TableName" data-selected="" is-object="false">
                                                <option value="">--SELECT--</option>
                                                <?php
                                                $allowableTables = array("enrolment_instance", "job", "person");
                                                foreach ($tables as $key => $value) {
                                                    $valueWithoutCamelCasing = $value;
                                                    /* CONDITIONS TO REMOVE TABLENAMES WITH "_has" AND CAMELCASE the tableNames */
                                                    if (strpos($value, '_has') == false && in_array($valueWithoutCamelCasing, $allowableTables)) {
                                                        $value = str_replace(" ", "", ucwords(str_replace("_", " ", $value)));
                                                        echo '<option value="' . $valueWithoutCamelCasing . '" >' . $value . '</option>';
                                                    }
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group condition-group-input">
                                        <label for="Condition" class="col-sm-2 control-label">Select Condition <span>*</span></label>  
                                        <div class="col-sm-10"> 
                                            <select class="form-control Condition" data-selected="" data-key="Condition" is-object="true">
                                                <option value="">--SELECT--</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="operatorName" class="col-sm-2 control-label">Select Operator <span>*</span></label>  
                                        <div class="col-sm-10">  
                                            <select class="form-control" id="operatorName" data-key="Operator" is-object="true">
                                                <option value="">--SELECT--</option>
                                                <option value="<"> < </option>
                                                <option value="<="> <= </option>
                                                <option value=">"> > </option>
                                                <option value=">="> >= </option>
                                                <option value="<>"> <> </option>
                                                <option value="="> = </option> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group value-type-input">
                                        <label for="valueType" class="col-sm-2 control-label">Value Type <span>*</span></label>  
                                        <div class="col-sm-10">
                                            <select class="form-control typeValueSelectorManipulation" id="valueType" data-key="ValueType" is-object="true">
                                                <option value="">--SELECT--</option>
                                                <option value="Single">Single</option>
                                                <!--<option value="Multiple">Multiple</option>
                                                <option value="Range">Range</option>
                                                <option value="Eval">Eval</option>-->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group value-condition-based-input" style="display:none;">
                                        <label for="value" class="col-sm-2 control-label">Value</label>
                                        <div class="col-sm-10 dtype" ></div>
                                    </div>
                                    <div class="form-group allowable-property-type-input">
                                        <label for="propertyType" class="col-sm-2 control-label">Property Type <span>*</span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control" data-selected="" data-key="PropertyType" is-object="true">
                                                <option value="">--SELECT--</option>
                                                <option value="string">String</option>
                                                <option value="date">Date</option>
                                                <option value="integer">Integer</option>
                                                <option value="decimal">Decimal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group target-project-input">
                                        <label for="targetProject" class="col-sm-2 control-label">Target Project</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="target-project-input-field">
                                                <option value="">--SELECT--</option>
                                                <?php
                                                foreach ($allProjectList->data as $k => $v) {
                                                    echo '<option value="' . $v->ProjectEntityId . '">' . $v->ProjectName . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="width: 707px;">
                                        <a href="#basicinfo" data-toggle="tab" class="btn btn-primary" data-id="tab1" style="margin-left: 20px;"><< Back</a>
                                        <a href="#criteria" data-toggle="tab" class="criteria"></a>
                                        <a class="curp btn btn-primary pull-right validate" id="save" container="criteria" data-id="tab3"> Save </a>
                                        <a class="curp btn pull-right" id="newcondition" data-id="">Add New Condition</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<input type="hidden" id="action" name="action" value="<?php echo $action; ?>">