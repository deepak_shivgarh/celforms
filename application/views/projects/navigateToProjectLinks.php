<style>
    img{
        height: 75px;
        width: 75px;
    }
    .container-box-image{
        margin: 10px;
        padding: 10px;
        display: inline-flex;
        border: 1px solid #cdd0d3;
        border-radius: 2px;
    }
    .container-box-image,.container-box-image:hover, .container-box-image:visited {
        text-decoration: none;
        color: #333;
        width: 310px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1> Manage <a href="<?php echo base_url('/projects/index'); ?>">
                <?php echo $projectName; ?>
            </a> Project Content<small> Visit Modules</small> </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">
                Projects
            </li>
            <li class="active">Projects - Visit Modules</li>
        </ol>
    </section>
    <section class="content">
        <div class="container">
            <div class="row">
                <a href="<?php echo base_url('/projects/manageEnrolmentUsers') . '/' . $projectEntityId . "/" . $projectName; ?>" class="col-sm-3 container-box-image">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/user.png'); ?>">
                    <h3 style="margin-top: 14px;">&nbsp; Manage Enrolment</h3>
                    <p style="display: inline-table;position: absolute;left: 97px;top: 52px;">Total Enrolment : <?php echo $enrolmentUserCount; ?></p>
                </a>
                <a href="<?php echo base_url('/projects/manageJobMaster') . '/' . $projectEntityId . "/" . $projectName; ?>" class="col-sm-3 container-box-image">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/job-master.png'); ?>">                    
                    <h3 style="margin-top: 14px;">&nbsp; Manage Job Master</h3>
                    <p style="display: inline-table;position: absolute;left: 97px;top: 52px;">Total Job Masters : <?php echo $manageJobMasterCount; ?></p>
                </a>
                <a href="https://secure2.community.org.in/data/tableMapping" class="col-sm-3 container-box-image" target="_blank">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/workflow.png'); ?>">
                    <h3 style="margin-top: 14px;">&nbsp; Manage Workflow</h3>
                    <p style="display: inline-table;position: absolute;left: 97px;top: 52px;"></p>
                </a>
            </div>
            <div class="row">
                <a href="<?php echo base_url('/projects/monitorJobs') . "/" . $projectName . '/' . $projectEntityId . "/CLOSED"; ?>" class="col-sm-3 container-box-image">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/data.png'); ?>">
                    <h3 style="margin-top: 14px;">&nbsp; Manage Data</h3>
                    <p style="display: inline-table;position: absolute;left: 97px;top: 52px;">Total Closed Jobs : <?php echo $manageDataCount; ?></p>
                </a>
                <a href="<?php echo base_url('/projects/monitorJobs') . "/" . $projectName . '/' . $projectEntityId; ?>" class="col-sm-3 container-box-image">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/monitor-jobs.png'); ?>">
                    <h3 style="margin-top: 14px;">&nbsp; Monitor Jobs</h3>
                    <p style="display: inline-table;position: absolute;left: 97px;top: 52px;">Total Jobs : <?php echo $monitorJobsCount; ?></p>
                </a>
                <a href="https://secure2.community.org.in/data/analysis" class="col-sm-3 container-box-image" target="_blank">
                    <img src="<?php echo base_url('/assets/admin/images/project-navigator-icons/extract-data.png'); ?>">
                    <h3 style="margin-top: 14px;">&nbsp; Extract Data</h3>
                </a>
            </div>
        </div>
    </section>
</div>