<?php
if ($currentAction === "manage-data") {
    echo '<script type="text/javascript">
                var finalJsonForPreviewFunctionality = {};
         </script>';
    echo '<script src="' . base_url('assets/admin/js/preview.js') . '"></script>';
    echo '<style>.modal-open {overflow: auto;}</style>'; 
} else {
    echo '<script src="' . base_url("assets/admin/js/workforce.js") . '"></script>';
}
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?php echo $pageHeading; ?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <a href="<?php echo base_url('projects/index'); ?>">Projects</a>
            </li>
            <li>
                <a href="<?php echo base_url('projects/navigateUserToLinkPage') . "/" . $projectDetails["projectEntityId"] . "/" . $projectDetails["projectName"]; ?>">
                    Visit Modules
                </a>
            </li>
            <li class="active"><?php echo $pageHeading; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12"> 
                <div class="box-body">
                    <?php if ($currentAction === "monitor-jobs") { ?>
                        <form class="form-inline">
                            <label>Filter Status :</label>
                            <select class="form-control custom-datatable-filter">
                                <option value="_RESET_">--SELECT STATUS--</option>
                                <option value="OPEN">OPEN</option>
                                <option value="DISPATCHED">DISPATCHED</option>
                                <option value="RECEIVED">RECEIVED</option>
                                <option value="INITIATED">INITIATED</option>
                                <option value="CLOSED">CLOSED</option>
                            </select>
                            <button class="btn btn-primary assigned-job-button" type="button">Assign JOB</button>
                        </form>
                    <?php } ?>                    
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Sr No.</th>
                                <th>Job Enrollment Id</th>
                                <th>Job Name</th>
                                <th>Job Scheduled Date</th>
                                <?php
                                if ($currentAction === 'monitor-jobs') {
                                    echo '<th>Person Action</th>';
                                }
                                ?>
                                <th>Job Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            if (!isset($jobsList->Error)) {
                                foreach ($jobsList as $k => $v) {
                                    echo '<tr>';
                                    echo '<td><input type="checkbox" value="' . $v->JobEntityId . '" class="assign-job-checkbox"/></td>';
                                    echo '<td>' . $count++ . '</td>
                                            <td>' . $v->JobEnrolmentInstanceEntityId . '</td>
                                            <td>';
                                    if ($currentAction === "manage-data") {
                                        echo "<a href='javascript:void(0);' class='open-job-inputs-fields' data-job-entity-id='" . $v->JobEntityId . "'>" . $v->JobName . "</a>";
                                    } else {
                                        echo $v->JobName;
                                    }
                                    echo '</td><td>' . $v->JobScheduledDate . '</td>';
                                    if ($currentAction === 'monitor-jobs') {
                                        echo "<td><a href='javascript:void(0);' class='view-person-detail-job' data-job-enrolment-instance='" . $v->JobEnrolmentInstanceEntityId . "'>View Person Detail</a></td>";
                                    }
                                    echo '<td>' . $v->JobStatus . '</td></tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <button class="display-preview-button" style="display:none;"></button>
    <link rel="stylesheet" href="<?php echo base_url('/assets/admin/css/bootstrap-datetimepicker.min.css') ?>">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script> 
    <script src="<?php echo base_url('/assets/admin/js/bootstrap-datetimepicker.js') ?>"></script>
    <?php $this->load->view('preview-fileonlyincludeuse'); ?>
</div>
<div id="person-display-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="right: -600px;width: 367px;height: 550px;overflow: auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Person Detail(s)</h4>
            </div>
            <div class="modal-body dynamic-table-person-data"></div>            
        </div>
    </div>
</div>
<div id="list-all-workforcess-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">All Workforce(s)</h4>
            </div>
            <div class="modal-body" style="height: 100%;">
                <select class="form-control worker-autocomplete-dropdown" style="width: 100%;">
                    <?php
                    foreach ($listOfAllWorkforces as $k => $v) {
                        if (isset($v->WorkforceAttributes->WorkforceOperatorId)) {
                            echo "<option value='" . $v->WorkforceEntityId . "'>" . $v->WorkforceAttributes->WorkforceOperatorId . "</option>";
                        }
                    }
                    ?>
                </select>
                <div style="text-align: center;">
                    <h3>Selected Jobs</h3>
                    <div class="dynamic-display-table"></div>
                </div>
                <div style="text-align: center;">
                    <button type="button" class="btn btn-primary assign-jobs-start-button" style="width: 50%;">Confirm Assignment of JOBS</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var mergeTwoObjectsAndPreparePreviewJson = function (questionJson, answerJson) {
        $.each(questionJson, function (k, v) {
            if (answerJson[k].QuestionEntityId === questionJson[k].QuestionEntityId) {
                questionJson[k].DefaultValue = answerJson[k].Value;
            }
        });
        return questionJson;
    };
    $(function () {
        var jobsListJson = '<?php echo $jobsListJson ?>';
        jobsListJson = JSON.parse(jobsListJson);
        var selectedJobs = [];
        var dataTableRef = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength": 100,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
        $(".custom-datatable-filter").change(function () {
            if ($(this).val()) {
                dataTableRef.draw();
            }
        });
        $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var statusSearch = $(".custom-datatable-filter").val();
                    if (statusSearch !== '_RESET_') {
                        if (statusSearch === data[6]) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                }
        );
        $(".open-job-inputs-fields").click(function () {
            var currentEntityId = $(this).attr("data-job-entity-id");
            var currentSelectedJob = "";
            $.each(jobsListJson, function (k, v) {
                if (currentEntityId === v.JobEntityId) {
                    currentSelectedJob = v;
                    return false;
                }
            });
            finalJsonForPreviewFunctionality = currentSelectedJob.JobDataset.FormDesign.Questions;
            finalJsonForPreviewFunctionalityValue = currentSelectedJob.JobDataset.FormDataset.Responses;
            finalJsonForPreviewFunctionality = mergeTwoObjectsAndPreparePreviewJson(currentSelectedJob.JobDataset.FormDesign.Questions, currentSelectedJob.JobDataset.FormDataset.Responses);
            $(".display-preview-button").click();
            $("body").scrollTop(0);
        });
        $(".worker-autocomplete-dropdown").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $("body").on("click", ".assigned-job-button", function () {
            var jobEntityIdsGetAssign = [];
            selectedJobs = [];
            var selectedJobsCounter = 1;
            $(".dynamic-display-table").html("");
            var htmlTableFormat = "<table class='table table-bordered table-striped'><tr><th>Sr No</th><th>Job Entity Id</th><th>Job Name</th><th>Job Status</th></tr>";
            $(".assign-job-checkbox").each(function () {
                if ($(this).is(":checked")) {
                    jobEntityIdsGetAssign.push($(this).val());
                }
            });
            $.each(jobsListJson, function (k, v) {
                if ($.inArray(v.JobEntityId, jobEntityIdsGetAssign) !== -1) {
                    htmlTableFormat += "<tr><td>" + (selectedJobsCounter++) + "</td><td>" + v.JobEntityId + "</td><td>" + v.JobName + "</td><td>" + v.JobStatus + "</td></tr>";
                    selectedJobs.push(v);
                }
            });
            if (selectedJobs.length > 0) {
                htmlTableFormat += "</table>";
                $(".dynamic-display-table").html(htmlTableFormat);
                $("#list-all-workforcess-modal").modal('show');
            }
        });
        $("body").on("click", ".assign-jobs-start-button", function () {
            var selectedWorker = $(".worker-autocomplete-dropdown").val();
            if (selectedJobs.length > 0) {
                $.each(selectedJobs, function (k, v) {
                    v.WorkforceWorkforceEntityId = selectedWorker;
                });
                selectedJobs = JSON.stringify(selectedJobs);
                showLoader();
                $.post(base_url + "ajax/multipleJobsUpdate", {updatedJobs: selectedJobs}).done(function (r) {
                    hideLoader();
                    console.log(r);
                });
            }
        });
    });
</script>