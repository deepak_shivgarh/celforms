<?php
/* echo "<pre>";
  print_r(json_encode($projectData));
  exit(); */
// $EC = (!empty($projectData) ? $projectData->ProjectAttributes->ProjectAttributes->EligibilityCriteria : ''); // in sone dat it work\
$EC = (!empty($projectData) ? $projectData->ProjectAttributes->EligibilityCriteria : '');
?>
<div class="content-wrapper" style="min-height: 916px;">
    <section class="content">
        <div class="row">
            <h3 style="margin-left: 16px;">Update Project</h3>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs wizardinfo">
                        <li class="active tab1"><a>Basic Info</a></li>
                        <li class="tab2"><a>Eligibility Criteria</a></li> 
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="basicinfo">
                            <form class="box-body form-horizontal" id="basicinfo_form" data-node="" multilevel="false"> 
                                <div class="form-group">
                                    <label for="projectName" class="col-sm-2 control-label">Project Name <span>*</span></label> 
                                    <div class="col-sm-6">
                                        <input type="hidden" class="form-control" id="ProjectEntityId" data-key="ProjectEntityId" is-object="true" value="<?php echo $ProjectEntityId; ?>">
                                        <input type="text" class="form-control project-name" id="projectName" data-key="ProjectName" is-object="true" placeholder="Project Name" value="<?php echo!empty($projectData) ? $projectData->ProjectName : ''; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ProjectStatus" class="col-sm-2 control-label">Status <span>*</span></label>
                                    <div class="col-sm-6">
                                        <select class="form-control" data-key="StatusStatusEntityId" is-object="true" id="StatusStatusEntityId">
                                            <?php
                                            foreach ($allStatusList as $k => $v) {
                                                if (in_array($v->StatusName, $allowedStatus)) {
                                                    echo '<option ' . ($projectData->StatusStatusEntityId == $v->StatusEntityId ? "selected='selected'" : "") . ' value="' . $v->StatusEntityId . '">' . $v->StatusName . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="width: 707px;">
                                        <a href="#criteria" data-toggle="tab" class="basicinfo"></a>
                                        <a class="curp btn btn-primary pull-right validate" id="validate-project-initials-update" container="basicinfo" data-id="tab2">Next >></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="criteria"> 
                            <form class="box-body form-horizontal" id="criteria_form" data-node="ProjectAttributes.EligibilityCriteria" data-type="AR" multilevel="true">  
                                <?php
                                $i = 0;
                                foreach ($EC as $value) {
                                    ?>
                                    <div class="border-bottom ec-container" id="level_<?php echo $i; ?>">
                                        <?php if ($i > 0) { ?>
                                            <div class="form-group remove-condition-button-container" style="margin-top: -7px;">
                                                <a class="pull-right remove-condition-button" data-remove-id="<?php echo $i; ?>">Remove this Condition</a>
                                            </div>
                                            <h4 class="eligibility-condition-heading" style="margin-top: -7px;">Condition - <?php echo ($i + 1) ?></h4>
                                        <?php } ?>
                                        <?php if ($i > 0) { ?>
                                            <div class="form-group">
                                                <label for="JoinType" class="col-sm-2 control-label">Join Type</label> 
                                                <div class="col-sm-10">
                                                    <select class="form-control JoinType" data-key="JoinType" data-selected="" is-object="true">
                                                        <option value="AND" <?php echo (!empty($EC[$i]->JoinType) ? (strtolower($EC[$i]->JoinType) == 'and' ? 'selected="selected"' : '') : ''); ?> > AND </option> 
                                                        <option value="OR" <?php echo (!empty($EC[$i]->JoinType) ? (strtolower($EC[$i]->JoinType) == 'or' ? 'selected="selected"' : '') : ''); ?> > OR </option> 
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group table-name-input">
                                            <label for="tableName" class="col-sm-2 control-label">Select Table <span>*</span></label> 
                                            <div class="col-sm-10"> 
                                                <select class="form-control tableName" data-key="TableName" data-selected="<?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Condition); ?>"  is-object="false">
                                                    <option value="">--SELECT--</option>
                                                    <?php
                                                    $allowableTables = array("enrolment_instance", "job", "person");
                                                    foreach ($tables as $key => $value) {
                                                        $explodedTableName = explode(".", $projectData->ProjectAttributes->EligibilityCriteria[$i]->Condition);
                                                        $valueWithoutCamelCasing = $value;
                                                        $value = str_replace(" ", "", ucwords(str_replace("_", " ", $value)));
                                                        if (strpos($value, '_has') == false && in_array($valueWithoutCamelCasing, $allowableTables)) {
                                                            if ($value == $explodedTableName[0]) {
                                                                echo '<option value="' . $valueWithoutCamelCasing . '" selected="selected">' . $value . '</option>';
                                                            } else {
                                                                echo '<option value="' . $valueWithoutCamelCasing . '">' . $value . '</option>';
                                                            }
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group condition-group-input">
                                            <label for="Condition" class="col-sm-2 control-label">Select Condition <span>*</span></label>  
                                            <div class="col-sm-10"> 
                                                <?php
                                                /*
                                                 * ATTRIBUTE [data-date-type-selected] CAN ONLY HANDLE VALUE OBJECT AND PHP KEY SO IN ORDER TO ADD
                                                 * JAVA KEY WE NEED TO FIND A ALTERNATE WAY TO APPEND BOTH THE KEYS [MAY BE JSON!]
                                                 */
                                                ?>
                                                <select class="form-control Condition" 
                                                        data-prefilled="<?php echo $projectData->ProjectAttributes->EligibilityCriteria[$i]->Condition; ?>" 
                                                        data-selected="<?php echo (is_object($EC[$i]->Value) === false ? $EC[$i]->Value : ""); ?>" 
                                                        data-date-type-selected="<?php echo (is_object($EC[$i]->Value) === true ? $EC[$i]->Value->php : ""); ?>" 
                                                        data-key="Condition" 
                                                        is-object="true">
                                                    <option value="">--SELECT--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="operatorName" class="col-sm-2 control-label">Select Operator <span>*</span></label>  
                                            <div class="col-sm-10">  
                                                <select class="form-control" id="operatorName" data-key="Operator" is-object="true">
                                                    <option value="">--SELECT--</option>
                                                    <option value="<" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == "<") ? "selected='selected'" : ""; ?>> < </option>
                                                    <option value="<=" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == "<=") ? "selected='selected'" : ""; ?>> <= </option>
                                                    <option value=">" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == ">") ? "selected='selected'" : ""; ?>> > </option>
                                                    <option value=">=" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == ">=") ? "selected='selected'" : ""; ?>> >= </option>
                                                    <option value="<>" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == "<>") ? "selected='selected'" : ""; ?>> <> </option>
                                                    <option value="=" <?php echo ($projectData->ProjectAttributes->EligibilityCriteria[$i]->Operator == "=") ? "selected='selected'" : ""; ?>> = </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group value-type-input">
                                            <label for="valueType" class="col-sm-2 control-label">Value Type <span>*</span></label>
                                            <div class="col-sm-10">
                                                <select class="form-control typeValueSelectorManipulation" id="valueType" data-key="ValueType" is-object="true" data-prefilled="">
                                                    <option value="">--SELECT--</option>
                                                    <option value="Single" >Single</option> 
                                                </select>
                                            </div>
                                        </div>  
                                        <div class="form-group value-condition-based-input" style="<?php echo @($isEvalValueType == true ? 'display:none' : ''); ?>">
                                            <label for="value" class="col-sm-2 control-label">Value <span>*</span></label>  
                                            <div class="col-sm-10 dtype"></div>
                                        </div>
                                        <div class="form-group allowable-property-type-input">
                                            <label for="propertyType" class="col-sm-2 control-label">Property Type</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" data-selected="" data-key="PropertyType" is-object="true">
                                                    <option value="">--SELECT--</option>                                                    
                                                    <option value="string" <?php echo ($projectData->ProjectAttributes->AllowableProperties[$i]->PropertyType == "string") ? "selected='selected'" : ""; ?>>String</option>
                                                    <option value="date" <?php echo ($projectData->ProjectAttributes->AllowableProperties[$i]->PropertyType == "date") ? "selected='selected'" : ""; ?>>Date</option>
                                                    <option value="integer" <?php echo ($projectData->ProjectAttributes->AllowableProperties[$i]->PropertyType == "integer") ? "selected='selected'" : ""; ?>>Integer</option>
                                                    <option value="decimal" <?php echo ($projectData->ProjectAttributes->AllowableProperties[$i]->PropertyType == "decimal") ? "selected='selected'" : ""; ?>>Decimal</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group target-project-input">
                                            <label for="targetProject" class="col-sm-2 control-label">Target Project</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="target-project-input-field">
                                                    <option value="">--SELECT--</option>
                                                    <?php
                                                    foreach ($allProjectList->data as $k => $v) {
                                                        if ($projectData->ProjectAttributes->EligibilityCriteria[$i]->TargetProject == $v->ProjectEntityId) {
                                                            echo '<option value="' . $v->ProjectEntityId . '" selected="selected">' . $v->ProjectName . '</option>';
                                                        } else {
                                                            echo '<option value="' . $v->ProjectEntityId . '">' . $v->ProjectName . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </form>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="width: 707px;">
                                        <a href="#basicinfo" data-toggle="tab" class="btn btn-primary" data-id="tab1" style="margin-left: 20px;"><< Back</a> 
                                        <a href="#criteria" data-toggle="tab" class="criteria"></a>
                                        <a class="curp btn btn-primary pull-right validate" id="save" container="criteria" data-id="tab3"> Update </a>
                                        <a class="curp btn pull-right" id="newcondition" data-id="">Add New Condition</a>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div> 
                </div> 
            </div>
        </div>
    </section>
</div>
<input type="hidden" id="action" name="action" value="<?php echo $action; ?>">