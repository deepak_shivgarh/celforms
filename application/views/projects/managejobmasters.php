<?php
$statusArray = [ '250ceb7584393b9176f8263811e60000' => 'OPEN',
    '250ceb7584393b917c20263811e60000' => 'CLOSED',
    '250ceb7584393b917efa263811e60000' => 'DISPATCHED',
    '250ceb7584393b91813e263811e60000' => 'RECEIVED',
    '250ceb7584393b918382263811e60000' => 'INITIATED',
    '250ceb7584393b9185b2263811e60000' => 'COMPLETED',
    '250ceb7584393b9187e2263811e60000' => 'ACTIVE',
    '250ceb7584393b918a12263811e60000' => 'SUSPENDED',
    '250ceb7584393b918c60263811e60000' => 'INACTIVE',
    '250ceb75843993ca4ca8265311e60000' => 'DELETED'
];
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1> Manage Job Masters for <a href="<?php echo base_url('projects/navigateUserToLinkPage') . "/" . $projectDetails["projectEntityId"] . "/" . $projectDetails["projectName"]; ?>"><?php echo $projectDetails["projectName"]; ?></a></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <a href="<?php echo base_url('projects/index'); ?>">Projects</a>
            </li>
            <li>
                <a href="<?php echo base_url('projects/navigateUserToLinkPage') . "/" . $projectDetails["projectEntityId"] . "/" . $projectDetails["projectName"]; ?>">
                    Visit Modules
                </a>
            </li>
            <li class="active">Manage Job Masters for <?php echo $projectDetails["projectName"]; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row"> 
            <div class="col-xs-12"> 
                <div class="box-body">
                    <a href="<?php echo base_url('forms'); ?>" class="curp btn btn-primary pull-right">Add Job Master</a><br><br>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr. No.</th> 
                                <th>Job Master Description</th>
                                <th>Project Name</th>
                                <th class="hide">Enrolment</th>
                                <th class="hide">Status</th>
                                <th class="hide">Total JobMasters</th> 
                                <th class="">Action</th>
                            </tr>
                        </thead> 
                        <tbody>  
                            <?php
                            if (count($jobMasterList) > 0) {
                                $i = 1;
                                foreach ($jobMasterList->data as $key => $jobMaster) {
                                    ?> 
                                    <tr>
                                        <td><?php echo $i ?></td> 
                                        <td> 
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title curp" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $i; ?>"> <?php echo $jobMaster->JobMasterName; ?> </h4>
                                                        </div>
                                                        <div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse in">
                                                            <div class="panel-body"> 
                                                                <?php foreach ($jobMaster->JobMasterSpecs->FormDesigns as $question) { ?>
                                                                    <div class="quesBody">
                                                                        <div>
                                                                            <strong>FormVersion: </strong><?php echo (isset($question->FormVersion) ? $question->FormVersion : ''); ?>
                                                                            <span class="pull-right"> 
                                                                                <?php
                                                                                if (isset($question->StatusStatusEntityId)) {
                                                                                    if ($statusArray[$question->StatusStatusEntityId] != 'ACTIVE') {
                                                                                        echo '<a href="' . base_url('forms/index/' . (isset($question->FormVersion) ? $question->FormVersion : '') . '/' . $jobMaster->JobMasterEntityId) . '" >Edit</a> <strong>&nbsp;&nbsp;/&nbsp;&nbsp;</strong>';
                                                                                    }

                                                                                    if ($statusArray[$question->StatusStatusEntityId] == 'ACTIVE') {
                                                                                        echo '<a href="' . base_url('forms/index/' . (isset($question->FormVersion) ? $question->FormVersion : '') . '/' . $jobMaster->JobMasterEntityId) . '" >Preview</a> <strong>&nbsp;&nbsp;/&nbsp;&nbsp;</strong>';
                                                                                    }
                                                                                }
                                                                                ?>  
                                                                                <a href="<?php echo base_url('/forms/version/' . (isset($question->FormVersion) ? $question->FormVersion : '') . '/' . $jobMaster->JobMasterEntityId); ?>" class="showLoader" >New Version</a> 
                                                                            </span>
                                                                        </div> 
                                                                        <div>
                                                                            <strong>EnrolmentForm: </strong><?php echo (isset($question->EnrolmentForm) ? ($question->EnrolmentForm == 'true' ? 'Yes' : 'No') : 'No'); ?>
                                                                        </div>
                                                                        <div>
                                                                            <strong>Status: </strong><?php echo (isset($question->StatusStatusEntityId) ? $statusArray[$question->StatusStatusEntityId] : ''); ?>
                                                                        </div>
                                                                    </div> 
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>

                                        </td>
                                        <td><?php echo $jobMaster->ProjectProjectEntityId; ?></td>
                                        <td class="hide"><?php echo (isset($question->EnrolmentForm) ? ($question->EnrolmentForm == 'true' ? 'Yes' : 'No') : ''); ?></td>
                                        <td class="hide"><?php echo (isset($jobMaster->StatusStatusEntityId) ? $statusArray[$jobMaster->StatusStatusEntityId] : ''); ?></td>
                                        <td class="center hide"><?php echo (isset($question->FormVersion) ? count($jobMaster->JobMasterSpecs->FormDesigns) : ''); ?></td>
                                        <td class="">
                                            <a href="<?php echo base_url('jobMaster/updateEligibiltyCriteriaJobMaster/' . $jobMaster->JobMasterEntityId); ?>" > Add Eligibility Criteria</a>
                                        </td> 
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength": 100,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>