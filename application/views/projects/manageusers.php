<script src="<?php echo base_url("assets/admin/js/workforce.js"); ?>"></script>
<div class="content-wrapper">
    <section class="content-header">
        <h1> Manage Enrolled Users for <a href="<?php echo base_url('projects/navigateUserToLinkPage') . "/" . $projectDetails["projectEntityId"] . "/" . $projectDetails["projectName"]; ?>"><?php echo $projectDetails["projectName"]; ?></a></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <a href="<?php echo base_url('projects/index'); ?>">Projects</a>
            </li>
            <li>
                <a href="<?php echo base_url('projects/navigateUserToLinkPage') . "/" . $projectDetails["projectEntityId"] . "/" . $projectDetails["projectName"]; ?>">
                    Visit Modules
                </a>
            </li>
            <li class="active">Manage Enrolled Users  for <?php echo $projectDetails["projectName"]; ?></li>
        </ol>
    </section>
    <section class="content">
        <div class="row" style="overflow: auto;">
            <div class="col-xs-12"> 
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Enrollment Instance Entity Id</th>
                                <th>Person Entity ID</th>
                                <th>Enrollment Instance Start Date</th>
                                <th>Enrollment Instance End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($allEnrolledUsers as $k => $v) {
                                echo '<tr>';
                                echo "<td>" . $count++ . "</td>";
                                echo "<td>" . $v->EnrolmentInstanceEntityId . "</td>";
                                echo "<td><a href='javascript:void(0);' class='view-person-detail' data-person-entity-id='" . $v->EnrolmentInstanceSubjectId . "'>" . $v->EnrolmentInstanceSubjectId . "</a></td>";
                                echo "<td>" . $v->EnrolmentInstanceStartDate . "</td>";
                                echo "<td>" . $v->EnrolmentInstanceEndDate . "</td>";
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength": 100,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<div id="person-display-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Person Detail(s)</h4>
            </div>
            <div class="modal-body dynamic-table-person-data"></div>            
        </div>
    </div>
</div>