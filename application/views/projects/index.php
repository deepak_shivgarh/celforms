<?php
$tempArray = [];
foreach ($allProjectList->data as $k => $v) {
    $tempArray[] = $v->ProjectName;
}
?>
<script type="text/javascript">
    var allowedProjectNames = <?php echo json_encode($tempArray); ?>;
</script>
<script src="<?php echo base_url('assets/admin/js/projects.js'); ?>"></script>
<style>
    .ec-container{
        padding: 15px !important;
        border: 1px solid #dadee2;
        width: 707px;
    }
    .form-group{
        margin-bottom: 3px;
    }
    .control-label{
        padding-top: 4px !important;
        font-size: 12px;
        padding-right: 6px !important;
    }
    .form-control{
        height: 25px;
        padding: 2px 10px;
        font-size: 12px;
    }
    .eligibility-condition-heading{ 
        border-bottom: 1px solid #7f7f7f;
        margin-bottom: 10px;
        padding-bottom: 10px;
    }
    .remove-condition-button-container{ 
        padding-right: 16px; 
        cursor: pointer;
        float: right;
        margin-top: 11px;
    }

    .form-field-invalid{
        border: 1px solid rgba(255, 0, 0, 0.48) !important;
    }

    .form-field-invalid-message{
        font-size: 12px;
        color: red !important;
        margin-top: 2px !important;
        margin-bottom: 0px !important;
        float: right;
    }

    .form-field-valid{
        border:2px solid #4CAF50;
    }

    .control-label >span{
        color:red;
    }
</style>
<?php
if ($action == 'add') {
    include('add.php');
} else if ($action == 'update') {
    include('edit.php');
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("body").on("focus", ".date-sub-input-field", function () {
            $(this).datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });
        });
    });
</script>