<div class="content-wrapper">
    <section class="content-header">
        <h1> Workforce <small>Manage</small> </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Workforce</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12"> 
                <div class="box-body">
                    <a href="<?php echo base_url('workforce/personGetForm'); ?>" class="curp btn btn-primary pull-right" style="margin-bottom: 10px;">
                        Add New Workforce
                    </a>
                    <div style="clear:both;"></div>
                    <table id="workforce-table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Person Name</th>
                                <th>Workforce Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($workforceList->data as $k => $v) {
                                echo "<tr>";
                                echo "<td>" . $count++ . "</td>";
                                echo "<td>";
                                if (@($v->WorkforceAttributes->PersonFirstName || $v->WorkforceAttributes->PersonMidName || $v->WorkforceAttributes->PersonLastName)) {
                                    echo @($v->WorkforceAttributes->PersonFirstName . " " . $v->WorkforceAttributes->PersonMidName . " " . $v->WorkforceAttributes->PersonLastName);
                                }
                                echo "</td>"; 
                                echo "<td>" . $v->WorkforceStatus . "</td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#workforce-table').DataTable({
            "paging": true,
            "lengthChange": false,
            "pageLength": 100,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": false
        });
    });
</script>
