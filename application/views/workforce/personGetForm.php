<script src="<?php echo base_url("assets/admin/js/workforce.js"); ?>"></script>
<style>
    .invalid-field{
        border: 1px solid red;
    }
    .invalid-field:focus{
        border: 1px solid red;
    }
    .invalid-field-message{
        color: red;
        font-size: 13px;
        float: right;
        margin-bottom: -10px;
    }
    .warning-field{
        border: 1px solid rgba(0, 0, 255, 0.71);
    }
    .warning-field:focus{
        border: 1px solid rgba(0, 0, 255, 0.71);
    }
    .warning-field-message{
        color: rgba(0, 0, 255, 0.71);
        font-size: 13px;
        float: right;
        margin-bottom: -10px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1> Workforce Person Search</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>
                <?php echo anchor('workforce/index', 'Workforce'); ?>
            </li>
            <li class="active">Person Search</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form class="box-body form-horizontal person-search-form" action="<?php echo base_url('workforce/showPersonSearchResult'); ?>" method="post">
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">First Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter First Name" class="form-control person-search-form-field validate-required" name="personParameters[PersonFirstName]" required value="<?php ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Mid Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Mid Name" class="form-control person-search-form-field" name="personParameters[PersonMidName]" value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonMidName"] : ""; ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Last Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Last Name" class="form-control person-search-form-field validate-warning" name="personParameters[PersonLastName]" required value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonLastName"] : ""; ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Gender : </label> 
                        <div class="col-sm-8">
                            <select class="form-control person-search-form-field validate-required" name="personParameters[PersonGender]" required>
                                <option value="">--SELECT GENDER--</option>
                                <option value="Male" <?php echo isset($preFilledDataForWorkforceSearch) && $preFilledDataForWorkforceSearch["PersonGender"] == "Male" ? "selected" : ""; ?>>Male</option>
                                <option value="Female" <?php echo isset($preFilledDataForWorkforceSearch) && $preFilledDataForWorkforceSearch["PersonGender"] == "Female" ? "selected" : ""; ?>>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Father First Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Father First Name" class="form-control person-search-form-field validate-required" name="personParameters[PersonFatherFirstName]" required value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonFatherFirstName"] : ""; ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Father Mid Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Father Mid Name" class="form-control person-search-form-field" name="personParameters[PersonFatherMidName]" value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonFatherMidName"] : ""; ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Father Last Name : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Father Last Name " class="form-control person-search-form-field validate-warning" name="personParameters[PersonFatherLastName]" required value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonFatherLastName"] : ""; ?>">
                        </div>
                    </div>                    
                    <div class="form-group person-search-form-field-box">
                        <label class="col-sm-2 control-label">Place of Birth : </label> 
                        <div class="col-sm-8"> 
                            <input type="text" placeholder="Enter Place of Birth" class="form-control person-search-form-field validate-required" name="personParameters[PersonPlaceOfBirth]" required value="<?php echo isset($preFilledDataForWorkforceSearch) ? $preFilledDataForWorkforceSearch["PersonPlaceOfBirth"] : ""; ?>">
                        </div>
                    </div>
                    <div class="form-group person-search-form-field-box">
                        <div class="col-md-offset-2 col-sm-8">
                            <button class="btn btn-primary person-search-form-button" type="button">Search</button>
                            <button class="btn btn-danger person-search-form-reset-button" type="button">Clear</button>
                        </div>
                    </div>
                </form>
            </div>      
        </div>
    </section>
</div>