<script type="text/javascript">
    var personsJsonEncodedData = '<?php echo $personsJsonEncodedData; ?>';
</script>
<script src="<?php echo base_url("assets/admin/js/workforce.js"); ?>"></script>
<style>
    .list-item-custom{
        margin-bottom: 10px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Person Search Results</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li>
                <?php echo anchor('workforce/index', 'Workforce'); ?>
            </li>
            <li>
                <?php echo anchor('workforce/personGetForm', 'Person Search'); ?>
            </li>
            <li class="active">Person Search Results</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <?php if (isset($noRecordFound)) { ?>
                    <h4 class="list-group-item-heading">No Records Found !</h4>
                <?php } else { ?>
                    <ul class="list-group">
                        <?php
                        foreach ($personList as $k => $v) {
                            if (is_file($v->PersonPhotograph)) {
                                $image = $v->PersonPhotograph;
                            } else {
                                if ($v->PersonGender == "MALE") {
                                    $image = base_url("assets/admin/images/male_selected.png");
                                } else if ($v->PersonGender == "FEMALE") {
                                    $image = base_url("assets/admin/images/female_selected.png");
                                }
                            }
                            echo '<li class="list-group-item list-item-custom" style="height: 126px;">                                    
                                    <img src="' . $image . '" style="float:left;height:100px;width:100px;margin-right:15px;">
                                    <div>
                                        <div class="pull-right">
                                            <button data-person-entity-id="' . $v->PersonEntityId . '" class="btn btn-primary view-person-detail" style="margin-bottom:10px;margin-top: 10px;">View Details</button><br/>
                                            <button class="btn btn-success" data-toggle="modal" data-target="#add-job-master-form">Add Workforce</button>
                                        </div>
                                        <h5 class="list-group-item-heading"><b>Name: </b>' . $v->PersonFirstName . " " . $v->PersonMidName . " " . $v->PersonLastName . '</h5>
                                        <h5 class="list-group-item-heading"><b>Father\'s Name: </b>' . $v->PersonFatherFirstName . " " . $v->PersonFatherMidName . " " . $v->PersonFatherLastName . '</h5>
                                        <h5 class="list-group-item-heading"><b>Gender: </b>' . $v->PersonGender . '</h5>
                                        <h5 class="list-group-item-heading"><b>Place of birth: </b>' . $v->PersonPlaceBirth . '</h5>
                                    </div>                                    
                                </li>';
                        }
                        ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </section>
</div>
<div id="person-display-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="right: -600px;width: 367px;height: 550px;overflow: auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Person Detail(s)</h4>
            </div>
            <div class="modal-body dynamic-table-person-data"></div>            
        </div>
    </div>
</div>
<div id="add-job-master-form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="top: 77px;right: -600px;width: 367px;height: 500px;overflow: auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Workforce</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Username:</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Select Role:</label>
                        <select class="form-control">
                            <option value="">--SELECT ROLE--</option>
                            <option>Super User</option>
                            <option>Web User</option>
                            <option>Mobile User</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Select Status:</label>
                        <select class="form-control">
                            <option value="">--SELECT STATUS--</option>
                            <option>Active</option>
                            <option>De-Active</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary">Add Workforce</button>
                </form>
            </div>
        </div>
    </div>
</div>