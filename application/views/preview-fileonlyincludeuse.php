<link rel="stylesheet" href="<?php echo base_url('/assets/admin/css/celforms-preview.css') ?>">
<script type="text/javascript"> 
</script>
<div id="display-celform-preview" class="modal fade tablet-view-modal-main-window" role="dialog" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog tablet-view-modal-main-dialog">
        <div class="modal-content tablet-view-modal-main-content">
            <div class="modal-header tablet-view-modal-main-header">
                <button type="button" class="close" data-dismiss="modal">
                    <img class="icon icons8-Cancel" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAADm0lEQVRoQ92a4XXUMAzHpQlgA+gEtBNwtwGdgHaDMgFlAm4D2gnoBrQT0E5AuwFMIN4/KFcncWJJcbiA37sv9yJbP0uWJdtMCzQReUtEr/WXG+GRiB6Z+a728FyjQxGB8u+JaKM/T7e3RITfNTMDdFabBSQigLggouNZWjwL3xPRjpmvo/2FgBTkcsKlovq0crDUZQTMBaSu9SXgVlFAuOK5xxXNQCJyRkSfiehlVLug3E8i+sDMVxZ5E5CIwCoAOmS7YubzkgJFoJXAtBxFqEmglcGYoEaBVgpThMoCaQDAullzQ/QbBIoBkIbm7weIZt7JQ/Q76Yf0HNC3v7jPeCH6398y8zb9swP0j7haH6rjen2gH4F05hcRvUP2TEQ3RPTGOe0PKo8EF/IvnPLI2o9amT1Q0DqA2TAzkkoSEWQRSFesUICBPNYD5JHkQt4LtbdSCoRA4MmaOzDtDDmgOjCJfATqnplP0EcDpJEN7uZpW2bGbA6aASoLk0ChrkJw8rQjRLwWCKXAR480ETUdjMlMQE3CzJhgJLC7FggzjbLZ07BuYKXG/42WssBgHcI6HvfH8HfMvGmBxEOSfOuBgtg+AExMQASm6Y7RRCTir6k+VigMWLJmGEYV2gIIdc7cvK0INeUBut7mwmCIUwBFAkJOvxBURRjo9KkmEDp0QVWGWQTIDLUAzGJAxdCse403TbIE4uouZ4JpNTNkFBaI9JuqQC6YhaAaoBphOwSzAFQTtudurEUYdS3LxuopPXLuuJ2b+lhh2szZm/u51lCT+mjEiSSnHpg20SzuUzMCRSc5jWQLlvIhl85YoFCOe+uzTvkQ6aBU4E3lZpNQwXX9XOCp22EQ61kARJA5A6o5T+hFLEuimYXScwXIe245Hpi5ceu5hyQdqEA604EKwoBjeEiiVkJJ/coVWv5Y6lSPsb4GKk1AQR5uD3mPZaDqEzNDtmn/90HjjBDuNGq1z5tQnfY2dlgPN/Ae9lXT0tgRzgWPi4f1aqUa+Z1Rr/BntuuUJPzi7gXvENbY8Egje+dbupJcI9QozCDK5UwhImuCmoQxAemaWgNUEcYMlASK3QGiH6LZRdWHF0mgwI4Ma3nPwaOBBc/PzhZ5GpNqpGU7Sg5vmmQFe9LHS6bnMJMbq3XExA3xvMyTpU8NgaIRz8vcIG2nxacxFkC9MMO+gDTE645wK9yt3nhca0yvKkD9zrVAszzRzN4AWiZx7JvfC0XkRlquzKUAAAAASUVORK5CYII=" width="25" height="25">
                </button>
            </div>
            <div class="modal-body mobile-main-body">
                <div class="loader-icon-container">
                    <img class="loader-icon" src="<?php echo base_url('/assets/admin/images/loader-preview.gif'); ?>"/>
                </div>
                <div class="mobile-layout-full-body">
                    <div class="mobile-layout-header">
                        <span>
                            <img style="margin: 8px;width: 10%;" src="<?php echo base_url('/assets/admin/images/icon-preview-upper-left.png'); ?>">
                        </span>
                        <span style="font-size: 15px;font-weight: bold;position: relative;top: 3px;left: 7px;">Saksham Preview Display</span>
                        <img class="tablet-view-upper-right-icon" width="25" height="25" style="margin: 8px;width: 10%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAA0CAYAAADFeBvrAAACYklEQVRoQ+3a7VHDMAwG4FcbwAZ0AmACygSwAbABnQQ2oJ0EmAA6Ad0ANhCnnt1LXMdW7MTx9ZK/+fJjyaktl3BiB52YBzOo9oiOHiFmPgPwYDpiQ0R/mk5h5iWAFwBX5vpvACsi+gjdPyrIYN6dRt1qUMz8A+DCafyOiBaTgDwY2w7p6SiKmdnXcCIKBmGUCAUwalQ1IAVGhaoCFMBsjeLSSaPO9JscFMHIF0sO+UKpUJOCYhj7VTPXqVCTgbQYm2pa1CQgLYaZHwHcENGTwDSo4qCemDcTobUWBeC32O9QIsa2T4uyU56Wa/Af1kxMH5R3hjMoaCBMFmow0MCYZNQgoJEwSahs0MiY3qgsUCFMX9R5aD3VuXwojOmDCq6nvKCJMIOgjkATY7JRLVAlmCzUAVQZJhm1B1WKSUJZ0Fej1GQfJMvmZWNxJksAO2sOVZLGOKea0BLRNZm1itvQmjB9IrXygWrEaFErm3JrU67dAHiuJM26UtdNP2n7HYBPAPehmULqmJEHPxLRLjSYmFnKvNKYm4RBd0C593bNFFIx8vxFDNMomAhKatgphxflmynkYBCbDbst7yqGKIVHKB/IWyRXvqA06Oh9MygWqcIpN0coFpCj83OEOnbqtD3pduD8UYj13Jxyc8q1c2QeQ7Ex4zkfLAQ2rzcz7tTJ6f5RmghJIc/d2O3jkj3Up9iM22BkpWw3lPu841AmIKLWPpLvs/1s/mOT8oLS90jHyZrqcHSth3KjVAK2daOzT0HfmwObuiUaqnlHq+4RjZC9wFSEJAVzxpSmgdprBPLqplnz5n+HhUA6JRUGkAAAAABJRU5ErkJggg==" >
                        <img class="tablet-view-upper-right-icon" width="25" height="25" style="margin: 8px;width: 10%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABaUlEQVRoQ+1Z0W1CMQy824BNKBvAJnSCdhQ2aJkERoBNuoGrSIn0VEEdkosEkd/vs198vnMcvxCTPJwEBwKIx6SZ7QF8AHjLthcAB5JHz7flvZwRM1sBOC0A/I0rAdqR/GkJ+J7PCCAp0LUT5IXk5mmBZDl9VQb4TvK70tY1kzJiZjVslKCkrKiBmJu6hQFJ2fqyD6X4zGwaII9I60qybM2PEHnTVs1I6h2vX+xZXjWsSNlI60oZyUBSQzz/00uuALZP3xCLgHNP+VwASgDSEUXWO5bFImeku2obPxBAGhM3zC0Y8VIb84iXoTvv5dKqPAFLT77yhhjzSKOchjXEmY7xMY/0qku6a01T7DGP9OpqxGAV80gnK9Ji74ylyz2AdKVvgPMwRmIeaWRLzkjMI41MFDcpI5VslLWlU6IayDTH+GmA1PyJL9KS/pFXSyvuRzo3rbgfcRMY9yNuim4bSIu9MQaJ2zRAfgHqo7ozXVxHngAAAABJRU5ErkJggg==" >
                    </div>
                    <div class="tablet-view-below-header-strip">
                        <img src="<?php echo base_url('/assets/admin/images/icon-preview-upper-header-border-bottom.png'); ?>" style="width:100%;">
                    </div>
                    <div class="mobile-layout-body">
                        <div class="tablet-view-upper-header-information" style="font-size: 12px;margin-bottom: 5px;margin-top: 5px">
                            <img style="float: right;height: 31px;width: 31px;" src="<?php echo base_url('/assets/admin/images/icon-preview-upper-header.png'); ?>">
                            <b>Person Name (Saksham ID):</b> Your name <br/>
                            <b>Father Name :</b> Your father's name
                        </div>
                        <div class="tablet-view-full-border"></div>
                        <form class="tablet-view-form" enctype="multipart/form-data"></form>
                    </div>
                    <div class="mobile-layout-footer"></div>
                </div>
            </div>
        </div>
    </div>
</div>