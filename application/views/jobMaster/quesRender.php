<?php 
	$dt = array(
			LABEL=>"Label",
		    SELECT_ONE=>"Choose One",
		    SELECT_MANEY=>"Select Multiple",
		    NUMERIC=>"Numeric",
		    TEXT=>"Text",
		    DATE=>"Date/Time",
		    LOCATION=>"Location",
		    YES_NO=>"Yes/No",
		    AUDIO=>"Audio",
		    IMAGE=>"Image",
		    VIDEO=>"Video",
		    CALCULATE=>"Calculations"
		);   
?>
<?php 
	$i=1; 
	$inc=0;
	foreach ($questions as $key => $question) { 
		$isRelHide = ($question->Relevance=='false'? 'hide' :''); ?> 
		<div class="control <?php echo $question->Type; ?> dragablediv ui-draggable" data-type="<?php echo $question->Type; ?>" data-id="selectedCtrlFocus<?php echo $i; ?>" id="<?php echo $i; ?>"><span class="dragablespan"><i class="<?php echo $question->Type; ?>_head" ></i> <?php echo $dt[$question->Type]; ?> <i class="controlRelevanceConditions <?php echo $isRelHide; ?>" title="Relevance"> </i></span>
		    <br>

		    
		    <div class="controlInfo">
			    <i class="deleteControl curp" id="deleteControl<?php echo $i; ?>" title="Delete this question."></i>
			    <a class="duplicateControl hide" id="duplicateControl<?php echo $i; ?>" title="Duplicate question">Duplicate</a>
			    <a class="moveDownControl hide" id="moveDownControl<?php echo $i; ?>" title="Move question down">Move down</a>
			    <a class="moveUpControl hide" id="moveUpControl<?php echo $i; ?>" title="Move question up">Move up</a> 
			    
		        <div class="rightdiv col-md-12">
		            <ul id="dataul<?php echo $i; ?>" class="parent"> 
		                <li class="controlSequenceOrder property_sequence hide" name="SequenceOrder"><?php echo $inc; ?></li>
		                <li class="controlLabel property_label" id="controlLabel<?php echo $i; ?>" name="Label"><?php echo $question->Label; ?></li>
		                <li class="controlHint property_hint" name="Hint"><?php echo $question->Hint; ?></li>
		                <div class="clearfix"></div>
		                <li class="controlTMapping property_tableMapping" name="TableMapping"><?php echo $question->TableMapping; ?></li>
		                <div class="clearfix"></div>
		                <li class="controlType property_type hide" name="Type"><?php echo $question->Type; ?></li> 
		                <?php $css_class = (!in_array($question->Type, array(TEXT,NUMERIC)) ? 'hide' : ''); ?> 
		                <li class="property_hideFormLinks property_hide  <?php echo $question->Hide=='false' ? 'hide' :''; ?>" id="property_hideFormLinks<?php echo $i; ?>" name="Hide"><?php echo $question->Hide; ?></li>
		                <li class="controlType property_piping hide" name="ReferredQuestion" id="property_piping<?php echo $i; ?>"><?php echo (isset($question->ReferredQuestion) ? $question->ReferredQuestion : ''); ?></li>	
		                <li class="property_readOnlyFormLinks property_readonly  <?php echo $question->ReadOnly=='false' ? 'hide' :''; ?>" id="property_readOnlyFormLinks<?php echo $i; ?>" name="ReadOnly"><?php echo $question->ReadOnly; ?></li>
		                <li class="property_requiredFormLinks property_required  <?php echo $question->Required=='false' ? 'hide' :''; ?>" id="property_requiredFormLinks<?php echo $i; ?>" name="Required"><?php echo $question->Required; ?></li>
		                <li class="property_requiredMessage hide" id="property_requiredMessage<?php echo $i; ?>" name="RequiredMessage"><?php echo (isset($question->RequiredMessage) ? $question->RequiredMessage : ''); ?></li>
		                <?php $dv = $question->DefaultValue; ?>
		                <li class="controlDefaultValue property_defaultValue <?php echo $css_class; ?> clearfix" id="controlDefaultValue<?php echo $i; ?>" name="DefaultValue"><?php echo (strtolower($dv)=='nan' ? "" : $dv) ?></li>
		                <div class="viewProperties_<?php echo $i; ?>"></div>
		                <?php echo get_input_structure($question, $i); ?>
		                <?php
		                	$relevance = '';
						    $relData = $question->Relevance;
						    $isObject = ($question->Relevance=='false'? 'false' :'true');
					        $relevance .= '<li class="clearfix"><ul name="Relevance" class="property_relevance" is-object="'.$isObject.'" id="property_relevance'.$i.'">'; 
					        if($relData=='false'){ }
					        else { $relevance .= getRelevanceUlBody($relData); } 
					        $relevance .= '</ul></li>';
					        echo $relevance;
					    ?>
					    <?php  
					    	if( $question->Type==CALCULATE ) {
					    		echo '<li class="controlKind radio_validation_kind hide" id="kind'.$i.'" name="Kind">'.$question->Kind.'</li>';
					    		echo '<li class="clearfix hide">
					    				<ul class="selection-translations" name="Calculation" id="Calculation'.$i.'" is-object="true" data-type="AR">';
					    		if($question->Calculation=="false")
					    			echo '<li class="datetimeTablemapping clevel_1" data-condition="" data-operation="+" data-calculation-kind="" data-value="" data-validatewith="" id="property_calculation_1" name="Calculation"></li>';
					    		else
					    			echo getConditionUlBody($question->Kind, $question->Calculation);
					    		echo '</ul></li>';
					    	}
					    ?>

		            </ul>
		        </div>
		        <div class="controlProperties" id="controlProperties<?php echo $i; ?>" style="margin: 0pt 100px;clear:both"></div>
		        <div class="controlFlowArrow"></div>
		        <div class="error" id="error<?php echo $i; ?>" style="text-align: center;color: red;font-size: 16px"></div>
		    </div>
		</div>
<?php $i++; $inc++; } ?>

