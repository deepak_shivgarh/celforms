<div class="content-wrapper" style="min-height: 916px;">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs wizardinfo">
						<li class="active tab1"><a>Basic Info</a></li>
						<li class="tab2"><a>Eligiblity Criteria</a></li> 
					</ul>
					<div class="tab-content">
						<div class="active tab-pane" id="basicinfo">
							<form class="box-body form-horizontal" id="basicinfo_form" data-node="" multilevel="false">
								<input type="hidden" id="createdAt" data-key="createdAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
								<input type="hidden" id="LastUpdatedAt" data-key="LastUpdatedAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
								<div class="form-group">
									<label for="projectName" class="col-sm-2 control-label">Project Name</label> 
									<div class="col-sm-10">
										<input type="text" class="form-control" id="projectName" data-key="ProjectName" is-object="true" placeholder="Project Name" value="<?php echo !empty($projectData) ? $projectData->ProjectName : ''; ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="ProjectStatus" class="col-sm-2 control-label">Status</label> 
									<div class="col-sm-10">
										<select class="form-control" data-key="ProjectStatus" is-object="true" id="ProjectStatus">
												<option >OPEN</option>
												<option>ACTIVE</option>
												<option>SUSPENDED</option>
												<option>COMPLETED</option> 
											</select>
									</div>
								</div> 
							</form> 
							<div class="row">
								<div class="col-xs-12"> 
									<a href="#criteria" data-toggle="tab" class="basicinfo"></a>
									<a class="curp btn btn-primary pull-right validate" container="basicinfo" data-id="tab2">Next >></a>
								</div>
							</div>
						</div> 
						<div class="tab-pane" id="criteria"> 
							<form class="box-body form-horizontal" id="criteria_form" data-node="ProjectAttributes.EligibilityCriteria" data-type="AR" multilevel="true"> 	   
								<div class="border-bottom ec-container" id="level_0">
									<div class="form-group">
										<label for="tableName" class="col-sm-2 control-label">Select Table</label> 
										<div class="col-sm-10"> 
											<select class="form-control tableName" data-key="TableName" data-selected="" is-object="false">
												<?php foreach ($tables as $key => $value) { 
													echo '<option value="'.ucfirst($value).'" >'.ucfirst($value).'</option>';
												} ?> 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="Condition" class="col-sm-2 control-label">Select Condition</label>  
										<div class="col-sm-10"> 
											<select class="form-control Condition" data-selected="" data-key="Condition"  is-object="true">
												 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="operatorName" class="col-sm-2 control-label">Select Operator</label>  
										<div class="col-sm-10">  
											<select class="form-control" id="operatorName" data-key="Operator" is-object="true">
												<option value="<"> < </option>
												<option value="<="> <= </option>
												<option value=">"> > </option>
												<option value=">="> >= </option>
												<option value="<>"> <> </option>
												<option value="="> = </option> 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="valueType" class="col-sm-2 control-label">Value Type</label>  
										<div class="col-sm-10"> 
											<select class="form-control" id="valueType" data-key="ValueType" is-object="true">
												<option value="Single">Single</option>
												<option value="Multiple">Multiple</option>
												<option value="Range">Range</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="value" class="col-sm-2 control-label">Value</label>  
										<div class="col-sm-10 dtype" >  
										</div>
									</div> 
								</div> 
							</form>
							<div class="row">
								<div class="col-xs-12">
									<a href="#basicinfo" data-toggle="tab" class="btn btn-primary" data-id="tab1" ><< Back</a> 
									<a href="#criteria" data-toggle="tab" class="criteria"></a>
									<a class="curp btn btn-primary pull-right validate" id="save" container="criteria" data-id="tab3"> Save </a>
									<a class="curp btn pull-right" id="newcondition" data-id="">Add New Condition</a>
									<a class="curp btn pull-right hide" id="removecondition" data-id="">Remove Condition</a>
								</div>
							</div>
						</div>  
					</div> 
				</div> 
			</div>
		</div>
	</section>
</div>
<input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
  