<script src="<?php echo base_url('assets/admin/js/projects.js'); ?>"></script>
<style>
    .ec-container{
        padding: 15px !important;
        border: 1px solid #dadee2;
        width: 707px;
    }
    .form-group{
        margin-bottom: 3px;
    }
    .control-label{
        padding-top: 4px !important;
        font-size: 12px;
    }
    .form-control{
        height: 25px;
        padding: 2px 10px;
        font-size: 12px;
    }
    .eligibility-condition-heading{ 
        border-bottom: 1px solid #7f7f7f;
        margin-bottom: 10px;
        padding-bottom: 10px;
    }
    .remove-condition-button-container{ 
        padding-right: 16px; 
        cursor: pointer;
        float: right;
        margin-top: 11px;
    }
</style>
<?php $EC = $jobMasterData->JobMasterSpecs->EligibilityCriteria; ?>
<div class="content-wrapper" style="min-height: 916px;">
    <section class="content">
        <div class="row">
            <h3 style="margin-left: 16px;">Update Job Master</h3>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs wizardinfo">
                        <li class="active tab1"><a>Basic Info</a></li>
                        <li class="tab2"><a>Eligibility Criteria</a></li> 
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="basicinfo">
                            <form class="box-body form-horizontal" id="basicinfo_form" data-node="" multilevel="false">
                                <input type="hidden" id="createdAt" data-key="createdAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
                                <input type="hidden" id="LastUpdatedAt" data-key="LastUpdatedAt" value="<?php echo date("Y-d-mTG:i:sz", time()); ?>" is-object="true">
                                <div class="form-group">
                                    <label for="JobMasterName" class="col-sm-2 control-label">Job Master Name</label> 
                                    <div class="col-sm-6">
                                        <input type="hidden" class="form-control" id="JobMasterEntityId" data-key="JobMasterEntityId" is-object="true" value="<?php echo $jobMasterData->JobMasterEntityId; ?>">
                                        <input type="text" class="form-control" id="jobMasterName" data-key="JobMasterName" is-object="true" placeholder="Job Master Name" value="<?php echo $jobMasterData->JobMasterName; ?>">
                                    </div>
                                </div>
                            </form> 
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="width: 707px;">
                                        <a href="#criteria" data-toggle="tab" class="basicinfo"></a>
                                        <a class="curp btn btn-primary pull-right validate" container="basicinfo" data-id="tab2">Next >></a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="criteria"> 
                            <form class="box-body form-horizontal" id="criteria_form" data-node="ProjectAttributes.EligibilityCriteria" data-type="AR" multilevel="true">
                                <?php if (count($EC) === 0) { ?>
                                    <div class="border-bottom ec-container" id="level_0">
                                        <div class="form-group table-name-input">
                                            <label for="tableName" class="col-sm-2 control-label">Select Table</label> 
                                            <div class="col-sm-10"> 
                                                <select class="form-control tableName" data-key="TableName" data-selected="" is-object="false">
                                                    <option value="">--SELECT--</option>
                                                    <?php
                                                    $allowableTables = array("enrolment_instance", "job", "person");
                                                    foreach ($tables as $key => $value) {
                                                        $valueWithoutCamelCasing = $value;
                                                        /* CONDITIONS TO REMOVE TABLENAMES WITH "_has" AND CAMELCASE the tableNames */
                                                        if (strpos($value, '_has') == false && in_array($valueWithoutCamelCasing, $allowableTables)) {
                                                            $value = str_replace(" ", "", ucwords(str_replace("_", " ", $value)));
                                                            echo '<option value="' . $valueWithoutCamelCasing . '" >' . $value . '</option>';
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group condition-group-input">
                                            <label for="Condition" class="col-sm-2 control-label">Select Condition</label>  
                                            <div class="col-sm-10"> 
                                                <select class="form-control Condition" data-selected="" data-key="Condition" is-object="true">
                                                    <option value="">--SELECT--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="operatorName" class="col-sm-2 control-label">Select Operator</label>  
                                            <div class="col-sm-10">  
                                                <select class="form-control" id="operatorName" data-key="Operator" is-object="true">
                                                    <option value="">--SELECT--</option>
                                                    <option value="<"> < </option>
                                                    <option value="<="> <= </option>
                                                    <option value=">"> > </option>
                                                    <option value=">="> >= </option>
                                                    <option value="<>"> <> </option>
                                                    <option value="="> = </option> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group value-type-input">
                                            <label for="valueType" class="col-sm-2 control-label">Value Type</label>  
                                            <div class="col-sm-10">
                                                <select class="form-control typeValueSelectorManipulation" id="valueType" data-key="ValueType" is-object="true">
                                                    <option value="">--SELECT--</option>
                                                    <option value="Single">Single</option> 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group value-condition-based-input" style="display:none;">
                                            <label for="value" class="col-sm-2 control-label">Value</label>
                                            <div class="col-sm-10 dtype" ></div>
                                        </div> 
                                        <div class="form-group target-project-input">
                                            <label for="targetProject" class="col-sm-2 control-label">Target Project</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="target-project-input-field">
                                                    <option value="">--SELECT--</option>
                                                    <?php
                                                    foreach ($allProjectList->data as $k => $v) {
                                                        echo '<option value="' . $v->ProjectEntityId . '">' . $v->ProjectName . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    $i = 0;
                                    foreach ($EC as $value) {
                                        ?>
                                        <div class="border-bottom ec-container" id="level_<?php echo $i; ?>">
                                            <?php if ($i > 0) { ?>
                                                <div class="form-group remove-condition-button-container" style="margin-top: -7px;">
                                                    <a class="pull-right remove-condition-button" data-remove-id="<?php echo $i; ?>">Remove this Condition</a>
                                                </div>
                                                <h4 class="eligibility-condition-heading" style="margin-top: -7px;">Condition - <?php echo ($i + 1) ?></h4>
                                            <?php } ?>
                                            <?php if ($i > 0) { ?>
                                                <div class="form-group">
                                                    <label for="JoinType" class="col-sm-2 control-label">Join Type</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control JoinType" data-key="JoinType" data-selected="" is-object="true">
                                                            <option value="AND" <?php echo (!empty($EC[$i]->JoinType) ? (strtolower($EC[$i]->JoinType) == 'and' ? 'selected="selected"' : '') : ''); ?> > AND </option>
                                                            <option value="OR" <?php echo (!empty($EC[$i]->JoinType) ? (strtolower($EC[$i]->JoinType) == 'or' ? 'selected="selected"' : '') : ''); ?> > OR </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group table-name-input">
                                                <label for="tableName" class="col-sm-2 control-label">Select Table</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control tableName" data-key="TableName" data-selected="<?php echo explode(".", $EC[$i]->Condition)[1] ?>"  is-object="false">
                                                        <option value="">--SELECT--</option>
                                                        <?php
                                                        $allowableTables = array("enrolment_instance", "job", "person");
                                                        foreach ($tables as $key => $value) {
                                                            $explodedTableName = explode(".", $EC[$i]->Condition);
                                                            $valueWithoutCamelCasing = $value;
                                                            $value = str_replace(" ", "", ucwords(str_replace("_", " ", $value)));
                                                            if (strpos($value, '_has') == false && in_array($valueWithoutCamelCasing, $allowableTables)) {
                                                                if ($value == $explodedTableName[0]) {
                                                                    echo '<option value="' . $valueWithoutCamelCasing . '" selected="selected">' . $value . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $valueWithoutCamelCasing . '">' . $value . '</option>';
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group condition-group-input">
                                                <label for="Condition" class="col-sm-2 control-label">Select Condition</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control Condition"
                                                            data-prefilled="<?php echo $EC[$i]->Condition; ?>"
                                                            data-selected="<?php echo (is_object($EC[$i]->Value) === false ? $EC[$i]->Value : ""); ?>"
                                                            data-date-type-selected="<?php echo (is_object($EC[$i]->Value) === true ? $EC[$i]->Value->php : ""); ?>"
                                                            data-key="Condition"
                                                            is-object="true">
                                                        <option value="">--SELECT--</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="operatorName" class="col-sm-2 control-label">Select Operator</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="operatorName" data-key="Operator" is-object="true">
                                                        <option value="">--SELECT--</option>
                                                        <option value="<" <?php echo ($EC[$i]->Operator == "<") ? "selected='selected'" : ""; ?>> < </option>
                                                        <option value="<=" <?php echo ($EC[$i]->Operator == "<=") ? "selected='selected'" : ""; ?>> <= </option>
                                                        <option value=">" <?php echo ($EC[$i]->Operator == ">") ? "selected='selected'" : ""; ?>> > </option>
                                                        <option value=">=" <?php echo ($EC[$i]->Operator == ">=") ? "selected='selected'" : ""; ?>> >= </option>
                                                        <option value="<>" <?php echo ($EC[$i]->Operator == "<>") ? "selected='selected'" : ""; ?>> <> </option>
                                                        <option value="=" <?php echo ($EC[$i]->Operator == "=") ? "selected='selected'" : ""; ?>> = </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group value-type-input">
                                                <label for="valueType" class="col-sm-2 control-label">Value Type</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control typeValueSelectorManipulation" id="valueType" data-key="ValueType" is-object="true" data-prefilled="">
                                                        <option value="">--SELECT--</option>
                                                        <option value="Single" <?php 
                                                        echo 'selected="selected"'; ?> >Single</option> 
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group value-condition-based-input" style="<?php echo @($isEvalValueType == true ? 'display:none' : ''); ?>">
                                                <label for="value" class="col-sm-2 control-label">Value</label>
                                                <div class="col-sm-10 dtype"></div>
                                            </div> 
                                            <div class="form-group target-project-input">
                                                <label for="targetProject" class="col-sm-2 control-label">Target Project</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="target-project-input-field">
                                                        <option value="">--SELECT--</option>
                                                        <?php
                                                        foreach ($allProjectList->data as $k => $v) {
                                                            if ($EC[$i]->TargetProject == $v->ProjectEntityId) {
                                                                echo '<option value="' . $v->ProjectEntityId . '" selected="selected">' . $v->ProjectName . '</option>';
                                                            } else {
                                                                echo '<option value="' . $v->ProjectEntityId . '">' . $v->ProjectName . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </form>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div style="width: 707px;">
                                        <a href="#basicinfo" data-toggle="tab" class="btn btn-primary" data-id="tab1" style="margin-left: 20px;"><< Back</a> 
                                        <a href="#criteria" data-toggle="tab" class="criteria"></a>
                                        <a class="curp btn btn-primary pull-right validate" id="update-eligibility-criteria-job-master" container="criteria" data-id="tab3">
                                            Update 
                                        </a>
                                        <a class="curp btn pull-right update-jobmaster-add-new-condition" id="newcondition" data-id="">Add New Condition</a>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div> 
                </div> 
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("body").on("focus", ".date-sub-input-field", function () {
            $(this).datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });
        });
    });
</script>