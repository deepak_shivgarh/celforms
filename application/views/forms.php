<script type="text/javascript" src="<?php echo base_url('assets/admin/js/celforms.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/preview.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/bootstrap-filestyle.min.js'); ?>"></script> 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script> 
<style type="text/css">.input-group {float: left;}.bootstrap-filestyle.input-group > input {display: none;}</style>
<div class="content-wrapper">
   <section class="content-header">
    <h1> Job Master <small>
    <?php 
      if( $action=="update" && $questionData->StatusStatusEntityId=='250ceb7584393b9187e2263811e60000' ) { 
        echo 'Preview'; 
      } else {
          echo ($action=='Versioning' ? 'Versioning': ($action=='update'?'Edit':'Add')); 
      }
    ?></small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Job Master</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <?php echo $this->session->flashdata('message'); ?>
      <div class="row"> 
          <div class="col-md-12 col-xs-12" style="padding:2px;"> 
            <?php if($action=='update' || $action == 'Versioning'){ ?>
              <div class="jobdetails">
                <span class="viewall pull-left"><h5><strong>Job Master Name:</strong> <?php echo $jobMasterData->JobMasterName; ?></h5> </span>
                
                <span class="viewall pull-left"><h5><strong>Job Enrolment:</strong> <?php echo (isset($jobMasterData->JobMasterSpecs->FormDesigns[0]->EnrolmentForm) ? ($questionData->EnrolmentForm=='true' ? 'Yes' :'No') : ''); ?></h5> </span>

                <?php if($action == 'Versioning'){ ?>
                  <span class="viewall pull-left"><h5><strong>Version:</strong> <?php echo $version = $newJobVersion; ?></h5> </span>
                <?php } else {?>
                  <span class="viewall pull-left"><h5><strong>Version:</strong> <?php echo $version =(isset($questionData->FormVersion) ? $questionData->FormVersion : ""); ?></h5> </span>
                <?php } ?>

              </div>
            <?php } ?> 
              <?php if( $action=="update" && $questionData->StatusStatusEntityId=='250ceb7584393b9187e2263811e60000' ) {} else { ?>
              <button type="button" class="btn btn-primary pull-right" id="save_form" style="margin-right: 15px;">
                <?php echo ($action=='update'? "UPDATE": "SAVE"); ?></button>
                <?php } ?>
              <button type="button" class="btn btn-primary pull-right display-preview-button" style="margin-right: 15px;">
                  PREVIEW
              </button>
              <a class="btn btn-primary curp viewall pull-right" href="<?php echo base_url('jobMaster'); ?>">VIEW JOB MASTERS</a>
              <a  data-toggle="modal" data-target="#myModal" id="targetmodel"></a>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12"> 
          <div class="col-md-9 col-xs-12" style="padding:2px;">
            <div id="workspaceScrollArea" class="ui-sortable workspaceScrollArea copy-paste-version">  
              <?php if($action=='update' || $action=='Versioning') include('jobMaster/quesRender.php'); ?>
              
            </div>
          </div>
          <div class="col-md-3 col-xs-12" style="padding:2px;">
            <div id="sticky-anchor"></div>
            <div class="propertiesPane" id="sticky" ondrop="return false;" ondrag="return false;" ondragenter="return false;" ondragover="return false;">
              <ul style="padding: 2px 1px;list-style-type: none;margin-right: 4px;">
                <li>
                  <div id="input_type" style="display: block;"></div> 
                </li>
              </ul>
              <div class="propertiesPaneContent"> 
                  <ul class="propertyList" id="ctrl_id_"> 
                      <li class="labels">
                        <div class="uiText">
                          <div class="propertyHeaderInfo">
                              <h4>Caption Text<a class="required">&nbsp;*</a></h4>
                              <input type="text" class="editorTextfield Label" id="property_label" data-key="Value" is-object="true">
                          </div> 
                        </div>
                      </li>
                      <li class="piping">
                        <div class="uiText">
                          <div class="propertyHeaderInfo">
                              <h4>Question Piping</h4>
                              <select class="form-control Piping h20" data-key="Value"  id="property_piping" is-object="true"></select> 
                          </div> 
                        </div>
                      </li>
                      <li class="hint">
                          <div class="uiText">
                              <div class="propertyHeaderInfo">
                                  <h4>Hint</h4>
                                  <input type="text" class="editorTextfield Hint" id="property_hint" data-key="Value" is-object="true">
                              </div> 
                          </div>
                      </li> 
                      <li class="tableMapping">
                        <div class="">
                          <h4>TableMapping</h4> 
                          <select class="form-control tableName h20" data-key="Value" id="tables" is-object="false">
                            <option value="" selected="selected"></option>
                            <?php 
                              $allowableTables = array("enrolment_instance", "job", "person");
                              $i=1; 
                              foreach ($tables as $key => $value) { 
                                $selected = ''; 
                                $valueWithoutCamelCasing = $value;
                                /* CONDITIONS TO REMOVE TABLENAMES WITH "_has" AND CAMELCASE the tableNames */
                                if ( strpos( $value, '_has' ) == false && in_array( $valueWithoutCamelCasing, $allowableTables ) ) 
                                {
                                  $v = str_replace(" ", "", ucwords(str_replace("_", " ", $value))); 
                                  echo '<option '.$selected.' value="'.ucfirst($value).'" >'.ucfirst($v).'</option>';
                                }
                                $i++;
                              } 
                            ?> 
                          </select>
                        </div>
                        <div class="c1div"> 
                          <select class="form-control Condition h20" data-key="Value"  id="columns" is-object="false"></select>
                        </div>
                        <div class="c2div"></div>
                        <div class="c3div"></div>
                        <div class="">   
                          <input type="text" class="TableMapping hide" value="" id="property_tableMapping" data-key="Value" is-object="true">
                        </div>
                      </li>
                      <li class="hideProperty">
                        <div class="">  
                          <span class="bool_grp bool hideFormLinks">
                            <input type="checkbox" class="editorCheckbox Hide minimal" data-text="hide view: " id="property_hideFormLinks" data-key="Value" is-object="true">
                            <label for="property_hideFormLinks"><h4>Hide</h4></label>
                          </span> 
                        </div>
                      </li> 
                      <li class="readOnlyGroup">
                        <div class="readOnly">  
                          <span class="bool_grp bool readOnlyFormLinks">
                            <input type="checkbox" class="editorCheckbox ReadOnly" data-text="readonly view: " id="property_readOnlyFormLinks" data-key="Value" is-object="true">
                            <label for="property_readOnlyFormLinks"><h4>Read only</h4></label>
                          </span> 
                        </div>
                      </li> 
                      <li class="requiredGroup">
                        <div class="required">  
                          <span class="bool_grp bool requiredFormLinks clearfix" id="requiredspan">
                            <input type="checkbox" class="editorCheckbox Required" data-text="required field: " id="property_requiredFormLinks" data-key="Value" is-object="true">
                            <label for="property_requiredFormLinks"><h4>Required</h4></label>
                          </span> 
                        </div>
                      </li>  
                  </ul>
              </div>
            </div>
          </div>
          <!-- /.box --> 
        </div>
      </div>
    </div>
  </section>
      <!-- /.content -->
    </div>
<!-- /.content-wrapper -->
 

<!-- Modal 1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 99999;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
        <h4 class="modal-title" id="myModalLabel">
          <?php if($action=='add') echo 'Job Master Add'; ?>
          <?php if($action=='update') echo 'Job Master Edit'; ?>
          <?php if($action == 'Versioning') echo 'Job Master Versioning'; ?>
        </h4>
      </div>
      <div class="modal-body" id="jobmaster">
        <form class="box-body form-horizontal" id="jobmaster_form" data-node="" data-type="AR" multilevel="false">      
          <div class="ec-container" id="">
            <div class="form-group">
              <label for="valueType" class="col-sm-3 cl-md-3 control-label">Job Master Name</label>  
              <div class="col-sm-8 cl-md-8"> 
                <input type="text" class="form-control" id="JobMasterName" data-key="JobMasterName" is-object="true" value="<?php echo (!empty($jobMasterData) ? $jobMasterData->JobMasterName  : ''); ?>" /> 
              </div>
            </div> 
            <div class="form-group">
              <label for="valueType" class="col-sm-3 cl-md-3 control-label">Work Force Role</label>  
              <div class="col-sm-8 cl-md-8"> 
                <select class="form-control" id="WorkforceRoleName" data-key="WorkforceRoleName" is-object="true"> 
                 <?php foreach ($WorkforceRole as $value) { 
                    echo '<option value="'.$value->WorkforceRoleName  .'" >'.$value->WorkforceRoleName.'</option>';
                    } ?> 
                </select> 
              </div>
            </div> 
            <div class="form-group">
              <label for="tableName" class="col-sm-3 cl-md-3 control-label">Project</label> 
              <div class="col-sm-8 cl-md-8"> 
                <select class="form-control tableName_bkp" id="ProjectEntityId" data-key="ProjectEntityId" is-object="<?php echo ($action=='add' ? 'true' : ''); ?>">
                  <option value="">--Select Project--</option> 
                  <?php $i=0; foreach ($projectData as $value) {  
                    $selected = ($value->ProjectEntityId === $jobMasterData->ProjectProjectEntityId ? 'selected="selected"' : '');
                    echo '<option value="'.$value->ProjectEntityId.'" allowableProperties="'.$i.'" '.$selected.'>'.$value->ProjectName.'</option>';
                    $i++; } ?> 
                </select>
              </div>
            </div> 
            <div class="form-group">
              <label for="valueType" class="col-sm-3 cl-md-3 control-label">Status</label>  
              <div class="col-sm-8 cl-md-8">  
                <select class="form-control" id="Status" data-key="Status" is-object="false"> 
                  <option value="250ceb7584393b9176f8263811e60000" <?php echo (isset($questionData->StatusStatusEntityId) ? ($questionData->StatusStatusEntityId === '250ceb7584393b9176f8263811e60000' ? 'selected="selected"' : '') : ''); ?> >OPEN</option>
                  <option value="250ceb7584393b9187e2263811e60000" <?php echo (isset($questionData->StatusStatusEntityId) ? ($questionData->StatusStatusEntityId === '250ceb7584393b9187e2263811e60000' ? 'selected="selected"' : '') : ''); ?> >ACTIVE</option>
                  <option value="250ceb7584393b918c60263811e60000" <?php echo (isset($questionData->StatusStatusEntityId) ? ($questionData->StatusStatusEntityId === '250ceb7584393b918c60263811e60000' ? 'selected="selected"' : '') : ''); ?> >INACTIVE</option>
                </select> 
              </div>
            </div> 
            <div class="form-group">
              <label for="valueType" class="col-sm-3 cl-md-3 control-label">Enrolment Form</label>  
              <div class="col-sm-8 cl-md-8">  
                <select class="form-control" id="EnrolmentForm" data-key="EnrolmentForm" is-object="false"> 
                  <option value="true"selected>Yes</option>  
                  <option value="false">No</option> 
                </select> 
              </div>
            </div>
          </div> 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary validate" container="jobmaster" id="save_form_json" >Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- ShowModal -->
<a  data-toggle="modal" data-target="#errorModal" id="targeterrormodel"></a>
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" aria-hidden="true" style="z-index: 99999;">
  <div class="modal-dialog" role="document">
   <div class="box box-danger box-solid error-box">
      <div class="bxbody center"> </div>
      <!-- /.box-body -->
      <div class="bxfooter center">
        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
 
<input type="hidden" id="action" name="action" value="<?php echo $action; ?>"> 
<link rel="stylesheet" href="<?php echo base_url('/assets/admin/css/bootstrap-datetimepicker.min.css') ?>">
<script src="<?php echo base_url('/assets/admin/js/bootstrap-datetimepicker.js') ?>"></script>
<script src="<?php echo base_url('/assets/admin/js/locales/bootstrap-datetimepicker.fr.js') ?>"></script>
 
 <?php require_once 'preview-fileonlyincludeuse.php'; ?>

<script type="text/javascript"> 
  var projectData = <?php echo json_encode($projectData, JSON_UNESCAPED_UNICODE); ?>;
  var tableMapping = {}; 
  



  var oldJobMasterName = "<?php echo (!empty($jobMasterData) ? $jobMasterData->JobMasterName : '') ?>";
  var jobMasterAddJson =  {
                            "JobMasterEntityId": "<?php echo (!empty($jobMasterData) ? $jobMasterData->JobMasterEntityId : '') ?>",
                            "JobMasterName": "<?php echo (!empty($jobMasterData) ? $jobMasterData->JobMasterName : '') ?>",
                            "StatusStatusEntityId": "<?php echo (!empty($jobMasterData) ? isset($jobMasterData->StatusStatusEntityId) ? $jobMasterData->StatusStatusEntityId : '' : '') ?>",
                            "ProjectProjectEntityId": "<?php echo (!empty($jobMasterData) ? isset($jobMasterData->ProjectProjectEntityId) ? $jobMasterData->ProjectProjectEntityId : '' : '') ?>",
                            "WorkforceRoleName": "",
                            "JobMasterSpecs": {
                              "AllowableProperties": [],
                              "DisallowedProperties": [],
                              "EligibilityCriteria": [], 
                              "FormDesign": { 
                                  "EnrolmentForm": "",
                                  "StatusStatusEntityId": "",
                                  "FormVersion": "<?php echo (!empty($questions) ? ( $action=='Versioning' ? $newJobVersion : $questionData->FormVersion) : "1.0"); ?>",
                                  "Questions": [<?php echo (!empty($questions) ? utf8_decode(json_encode($questions, JSON_UNESCAPED_UNICODE)) : ''); ?>]
                              }
                            },
                            "Token": ""
                          };
  var rcTableMapping = {};  
  var dragabledivCount = <?php echo (!empty($questions) ? count($questions) : 0); ?>;          
  var quesJson = {};

  $.each( jobMasterAddJson.JobMasterSpecs.FormDesign.Questions[0], function( key, value ) { 
    quesJson[key] = value;  
  }); 

  var enrolmentTableOptions = '<?php echo $enrolmentTableOptions; ?>';
  var jobTableOptions = '<?php echo $jobTableOptions; ?>';
  var personTableOptions = '<?php echo $personTableOptions; ?>';
  var enrolmentPropertiesJson = '<?php echo $enrolmentPropertiesJson; ?>';
  var jobPropertiesJson = '<?php echo $jobPropertiesJson; ?>';
  var personPropertiesJson = '<?php echo $personPropertiesJson; ?>';

  $('body').on('click', '.validate', function(e) { 
    var containerDiv = $(this).attr('container');
    var ID = $(this).attr('id');
    var inputData = '';
    var flag = true;  
    $('#'+containerDiv).find(':input').each(function(i, elem){   
      if($.trim(this.value)==''){ this.focus(); flag = false;  };
    });
   if(flag==true) {
      // GET JSON OF ANY FORM IN KEY : VALUE PAIR
      var formId = '#'+$('#'+containerDiv).children('form').attr('id'); 
      /* node where newJson object will be apend or add IF node is empty newJson will be override to root nodes */
      var node = $('#'+containerDiv).children('form').attr('data-node'); 
      var key = 'data-key';
      var newJson = getInputJson(formId, key, 'is-object', 'multilevel'); 
 
      var type = $(formId).attr('data-type');

      jobMasterAddJson.JobMasterSpecs.FormDesign.EnrolmentForm = $('#EnrolmentForm option:selected').val();
      jobMasterAddJson.JobMasterSpecs.FormDesign.StatusStatusEntityId = $('#Status option:selected').val();
      jobMasterAddJson.StatusStatusEntityId = (jobMasterAddJson.StatusStatusEntityId == '250ceb7584393b9187e2263811e60000' ? jobMasterAddJson.StatusStatusEntityId : $('#Status option:selected').val());
      jobMasterAddJson.JobMasterSpecs.FormDesign.Questions = quesJson; 

      var finalJson = mergeJson(jobMasterAddJson, node, newJson, type);
      if(ID == 'save_form_json') {
        showLoader();
        setJobMasterDetails();  
        var check = isJobMasterNameUnique( $("#JobMasterName").val(), $("#ProjectEntityId option:selected").val() );
        if( check==true ) {
          $.ajax({
            type: "POST",
            url: base_url+'ajax/jobMasterAdd',
            data: { action: $('#action').val(), requestJson: jobMasterAddJson },
            cache: false, 
            contentType: "application/x-www-form-urlencoded",
            success: function (data) { 
              console.log(data);
              data = JSON.parse(data);
              console.log(data); 
              if($.trim(data.isSuccess)=="1") { 
                $(window).unbind('beforeunload');
                window.location.href = base_url+'forms'; 
              } else {
                  hideLoader();
                  showError( data.error );
              }
            },
            error: function(e) {
              showError('Something went wrong. Please check and save again.');
            }
          }); 
        } else {
            showError( '<strong>' + $("#JobMasterName").val() + "</strong> is already assigned in <strong>" + $("#ProjectEntityId option:selected").text() + "</strong>, please choose a different name." );
        }
        console.log(jobMasterAddJson);
      }
    }
  });

  var isJobMasterNameUnique = function( JobMasterName, ProjectEntityId ) {
        var jobMaster = <?php echo (!empty($jobMasters) ? json_encode($jobMasters, JSON_UNESCAPED_UNICODE) : '""') ?>;
        var arr = [], counter = 0;  
        if( JobMasterName !== oldJobMasterName ) {
          $.each(jobMaster.data, function(i, obj) {   
            if( ProjectEntityId.toLowerCase()==obj.ProjectProjectEntityId.toLowerCase() && JobMasterName.toLowerCase()==obj.JobMasterName.toLowerCase() ) { 
              console.log(obj); 
              counter++;
            }  
          });
        }
        if( counter>0 ) { hideLoader(); return false; } else { return true; }
      };
 
  $('body').on('change', '#ProjectEntityId', function(e){ 
    isProjectEnrolled('<?php echo (!empty($jobMasterData) ? $jobMasterData->JobMasterEntityId : '') ?>', $("#ProjectEntityId option:selected").val(), '');
    jobMasterAddJson.ProjectProjectEntityId = $("#ProjectEntityId option:selected").val(); 
  });

  $(window).load(function(){ $("#ProjectEntityId").val($("#ProjectEntityId option:selected").val()).change(); });
 
  function isProjectEnrolled(jobMasterId, projectID, formVersion) { 
    console.log("jobMasterId="+jobMasterId+" and projectID="+projectID);
    var jobMaster = <?php echo (!empty($jobMasters) ? json_encode($jobMasters, JSON_UNESCAPED_UNICODE) : '""') ?>;
    console.log(jobMaster.data);
    var flag=0;
    var arr=[];
    arr['selfJMFlag']=0; 
    arr['isAlreadyEnrolled']=0;
    if(jobMaster!='') { 
      console.log(jobMaster.data);
      $.each(jobMaster.data, function(i, obj) {   
        if(projectID==obj.ProjectProjectEntityId) { 
          if(jobMasterId==obj.JobMasterEntityId && projectID==obj.ProjectProjectEntityId && obj.JobMasterSpecs.FormDesigns[0].EnrolmentForm==1 ) { 
            arr['selfJMFlag']+=1;
          } 
          if(obj.JobMasterSpecs.FormDesigns[0].EnrolmentForm==1 ) { 
            arr['isAlreadyEnrolled']+=1; 
          } else { console.log('not enrolled'); }
        }  
      });
    } else {  
      $.each(jobMaster.data, function(i, obj) {   
        if(projectID==obj.ProjectProjectEntityId) {   
          if(obj.JobMasterSpecs.FormDataset[0].EnrolmentForm==1) { 
            arr['isAlreadyEnrolled']+=1; 
          } else { console.log('not enrolled'); }
        }  
      });
    } 
    if(arr['selfJMFlag'] > 0 || (arr['selfJMFlag']==0 && arr['isAlreadyEnrolled']==0) || (arr['isAlreadyEnrolled']==0)) {
      $("#EnrolmentForm").html('<option value="true" selected="selected">Yes</option><option value="false">No</option>');
    } else { $("#EnrolmentForm").html('<option value="false" selected="selected">No</option>'); }
    $("#EnrolmentForm").change();  
  } 

  function getAllTableMapping() {
    $.each( quesJson, function( key, value ) { 
      var Property = value.TableMapping;
      var PropertyType = "";
      if( value.Type==DATE ) {
        PropertyType = "date";
      } else if( value.Type==NUMERIC ) {
          PropertyType = "integer";
      } else { PropertyType = "string"; }

      tableMapping[key] = {"Property": Property,"PropertyType": PropertyType};
    });
    return tableMapping;
  }

  function setJobMasterDetails() {  
    tableMapping = getAllTableMapping();
    var dataType = ""; 
    /* add propertiesAttributes of questions and project both */
    var dataType = ""; 
    var tempArray = [];
    var tempObj = []; 

    var inc=0;
    $.each(tableMapping, function (key, value) {
      if( $.isEmptyObject(value)==!1 ) {
        var v = value.Property; 
        if( typeof v !== "undefined" ) {
          if( v.indexOf("Job.JobDataset.") === -1 ) {
            tempObj[inc] = {"Property": value.Property,"PropertyType": value.PropertyType};
            inc++;
          } 
        }
      }
    }); 

    var projectAllowableProperties = projectData[$('#ProjectEntityId option:selected').attr('allowableproperties')].ProjectAttributes.AllowableProperties;  
    var counter = parseInt(getObjectLength(tableMapping))+1;
    $.each(projectAllowableProperties, function (key, value) {
        tempObj[counter] = {"Property": value.Property,"PropertyType": value.PropertyType};
        counter++;
    });  

    jobMasterAddJson.JobMasterSpecs.AllowableProperties = tempObj;
    jobMasterAddJson.JobMasterSpecs.EligibilityCriteria.push([]);
    jobMasterAddJson.JobMasterSpecs.DisallowedProperties.push([]); 
  }

  function getObjectLength(object) {
    var len = $.map(object, function(n, i) { return i; }).length;
    return len;
  }

  function getInputJson(containerDiv, key, condition, isMultilevel) {
    var newJson = {}; 
    if($(containerDiv).attr(isMultilevel)=='false') {
      jQuery(containerDiv).find(':input').each(function(i, elem) {
        if($.trim(condition) != '') {
          if(this.getAttribute(condition)=='true')
            newJson[this.getAttribute(key)] = this.value; 
        } else {
          newJson[this.id] = this.value; 
        }
      });
    } else {  
        var arr = [];   
        jQuery(containerDiv).find('.ec-container').each(function(j, elem) { 
          var subArray = {};
          $(this).find(':input').each(function(i, elem) {
            if($.trim(condition) != '') {
              if(this.getAttribute(condition)=='true')
                subArray[this.getAttribute(key)] = this.value; 
            } else { subArray[this.id] = this.value;  } 
          });    
          arr.push(subArray);
        });
        newJson = arr;
    }
    return newJson;  
  }

  /* add / merge json data according to each step to the default json skelton */
  function mergeJson(defaultJson, node, newJson, type) {  
    if($.trim(node) != '') { 
    } else {
        for(var _obj in defaultJson) defaultJson[_obj ]=defaultJson[_obj];
        for(var _obj in newJson) defaultJson[_obj ]=newJson[_obj];
    } 
    return defaultJson;
  }
  
  function getProperties(tableName, divLevel) { 
    var position = divLevel.replace('ctrl_id_', '');
    var res = $("#"+position+" .property_tableMapping").text().split(".");  
    var selectedColumn = res[0]+"."+res[1];  
    if(tableName!="") { 
      tableName = tableName.toLowerCase();
      switch(tableName) {
        case 'enrolment_instance':
          $('#'+divLevel).children().children().children('.Condition').html(enrolmentTableOptions);
          break;
        case 'job':
          $('#'+divLevel).children().children().children('.Condition').html(jobTableOptions);
          break;
        case 'person':
          $('#'+divLevel).children().children().children('.Condition').html(personTableOptions);
          break; 
      }  
      if(res.length == 2) {
        $(".Condition").val(selectedColumn).prop('selected', true); 
        $(".Condition2").html(""); 
        $(".Condition3").html("");
      }
      if(res.length > 2) { 
        $(".Condition").val(selectedColumn).prop('selected', true);
        $(".Condition").change();
      } 
    }
  }

  function getPropertiesJson(tableName, selectedAttribute) {  
    if( tableName !="" ) { 
      var data = '';
      if(tableName.toLowerCase() === 'enrolment_instance')
        data = enrolmentPropertiesJson;
      else if(tableName.toLowerCase() === 'job')
        data = jobPropertiesJson;
      else if(tableName.toLowerCase() === 'person')
        data = personPropertiesJson; 

      $('.c2div').html('<select class="form-control condition2 h20" data-key="Value"  id="condition2" is-object="false">' + data + '</select>'); 
        var tmAttr = $('#condition2 option:selected').val();
        var tmType = $('#condition2 option:selected').attr('type');
        setTableMappingProperty(tmAttr, tmType);  
        var res = selectedAttribute.split("."); 
        var opt = res[res.length-1]; 
        var selectedColumn = res[0] + "." + res[1];   
        if($('#condition2 option').filter(function() { 
          if ($(this).text() == opt)
            $(this).prop('selected', true).change();
          return $(this).text() === opt; }).length > 0) {  
        } else { 
          $("#condition2 option:last").prop("selected", true).change();
        }
        if(res.length > 3) {
          $("#condition3").val(res[res.length-1]).keyup();
        }   
    }
  }
 
  $('body').on('change', '#condition2', function() { 
    var tmAttr = $('option:selected', this).val();
    var tmType = $('option:selected',this).attr('type');
    setTableMappingProperty(tmAttr, tmType);
  });

  $('body').on('keyup', '#condition3', function() { 
      var tmAttr = $('#condition2 option:selected').val() + '.' + $(this).val();
      if( tmAttr.indexOf("Job.JobDataset.") === 0) {
        tmAttr = tmAttr.replace("Job.JobDataset.", "");
      }
      $('#property_tableMapping').val(tmAttr);
      $('#property_tableMapping').attr('title', tmAttr);
      $('#property_tableMapping').change(); 
  });

  $(window).load(function(){ $('.tableName').change(); }); 

  $('body').on('change', '.tableName', function() { 
    var divLevel = $(this).parent().parent().parent().attr('id');
    getProperties($(this).val(), divLevel);
  });

  $('body').on('change', '#property_tableMapping', function(){ 
    var position = $('.propertyList').attr('id').replace('ctrl_id_', '');   
    var dataType = $("#" + position + " .property_type").text();
    switch(dataType) {
      case "selectDate":
        dataType="date";
        break;
      case "inputNumeric":
        dataType="integer";
        break; 
      default:
        dataType="string";
    } 
    tableMapping[position-1] = {"Property":$(this).val(),"PropertyType":dataType};   
  });

  $('body').on('change', '.Condition', function(){  
    var divLevel = $(this).parent().parent().parent().attr('id');
    var dataTypeString = $('option:selected',this).attr('type'); 
    getDataType(dataTypeString, $('#tables option:selected').val()); 
    var res = selectedAttribute.split("."); 
    if(dataTypeString !== 'json'){
      var tmAttr = $('option:selected',this).val(); 
      setTableMappingProperty(tmAttr, ''); 
    }
  }); 
 
  $('body').on('keyup', '.spchar', function(){ 
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,."; 
    var value = $(this).val(); 
    value = value.replace(/[^A-Z0-9]+/ig, "_");
    $(this).val(value); 
  }); 

  $('body').on('keyup', '.number', function(){ 
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,."; 
    var value = $(this).val();
    value = value.replace('.', "");
    $(this).val(value); 
  });

  function setTableMappingProperty(tmAttr, tmType) {   
    var res = selectedAttribute.split(".");
    $('#condition3').remove(); 
    if( tmType == Other ) {   
      $('#property_tableMapping').val(tmAttr);
      $('#property_tableMapping').attr('title', tmAttr);
      $('#property_tableMapping').change(); 
      $('.c3div').html('<input type="text" class="form-control condition3 spchar" data-key="Value" value="" id="condition3" is-object="false">');
      $("#condition3").val( res[res.length-1] ).keyup(); 
    } else {  
        $('#property_tableMapping').val(tmAttr);
        $('#property_tableMapping').attr('title', tmAttr);
        $('#property_tableMapping').change(); 
        $("#condition3").keyup(); 
    }
  }

  function getDataType(dataTypeString, tableName){ 
    var dataType="",midVal=""; 
      if(dataTypeString != 'json') {
        dataType = dataTypeString.substring(0, dataTypeString.lastIndexOf("(")); 
        if( $.trim(dataType) == '' ) {
          dataType = dataTypeString;
        } else {  
            midVal = dataTypeString.substring(dataTypeString.lastIndexOf("(") + 1); 
            midVal = midVal.substring(0, midVal.lastIndexOf(")")); 
        }
      } else { dataType = dataTypeString; } 
      switch(dataType) {
        case 'json':
          getPropertiesJson(tableName, selectedAttribute); 
          break;
        default:
          $('#condition2').remove();
          break;
      }    
  }

  function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    } catch (err) {
        alert(err.Description);
    }
  }
</script> 

<script type="text/javascript"> 
  function sticky_relocate() {
    if($(window).outerWidth()>986) {
      var window_top = $(window).scrollTop();
      var div_top = $('#sticky-anchor').offset().top;
      if (parseInt(window_top) >= parseInt(div_top) - parseInt($(".navbar").outerHeight())) {
          $('#sticky').addClass('stick');
          $('#sticky-anchor').height($('#sticky').outerHeight());
      } else {
          $('#sticky').removeClass('stick');
          $('#sticky-anchor').height(0);
      }
    }
  }

  $(function() {
      $(window).scroll(sticky_relocate);
      sticky_relocate();
  });

  $(window).bind('beforeunload', function(){
    return 'Are you sure you want to leave?';
  });  
</script>
 
<style type="text/css">
  #sticky.stick {
      margin-top: 0 !important;
      position: fixed;
      top: 60px;
      width: 20%;
      z-index: 10000; 
  }
   scrollbar[orient="vertical"] scrollbarbutton,
  scrollbar[orient="vertical"] slider {
   width: 25px !important;
   height: 25px !important;
   -moz-appearance: none !important;
  }
</style>
 