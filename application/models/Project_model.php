<?php

class Project_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
        $this->load->database();
    }

    public function projectGet($projectId = '') {
        $data = '';
        $project["GetAllStatus"] = true;
        if (!empty($projectId)) {
            $project['ProjectEntityId'] = $projectId;
            $project['ProjectName'] = '';
            $project['GeographyGeographyEntityId'] = '';
            $project['ProjectAttributes'] = '';
        }
        $data .= json_encode($project);
        $result = $this->getRequest(PROJECT_GET, $data);
        return json_decode(utf8_decode($result));
    }

    public function WorkforceRoleGet() {
        $result = $this->getRequest(WORK_FORCE_ROLE_GET, '');
        return json_decode(utf8_decode($result));
    }

    public function jobMasterGet($jobMasterId = '') {
        $data = '';
        if (!empty($jobMasterId)) {
            $jobMaster['JobMasterEntityId'] = $jobMasterId;
            $jobMaster['JobMasterName'] = '';
            $jobMaster['ProjectEntityId'] = '';
            $jobMaster['WorkflowMasterEntityId'] = '';
            $jobMaster['Token'] = '';
            $data = json_encode($jobMaster);
        }
        $result = $this->getRequest(JOB_MASTER_GET, $data);
        return json_decode($result);
    }

    public function propertiesGet() {
        $result = $this->getRequest(PROPERTIES_GET, '');
        return json_decode(utf8_decode($result));
    }

    public function jsonPropertiesGet($table) {
        $data['Tables'] = [strtolower($table)];
        $data['GetAttributes'] = true;
        $data = json_encode($data);
        $result = $this->getRequest(PROPERTIES_GET, $data); 
    }

    public function questionsAllowableProperties($questions) {
        $TableMapping = array();
        $i = 1;
        foreach ($questions as $question) {
            $TableMapping[$i] = $question->TableMapping;
            $i++;
        }
        return $TableMapping;
    }

    public function getUsers($projectEntityId = '', $enrolmentInstanceEntityId = '') {
        $data["ProjectProjectEntityId"] = $projectEntityId;
        if ($enrolmentInstanceEntityId != '') {
            $data["EnrolmentInstanceEntityId"] = $enrolmentInstanceEntityId;
            $data["GetPerson"] = "true";
        }
        $data = json_encode($data);
        $result = $this->getRequest(ENROLMENT_INSTANCE_GET, $data);
        return $result;
    }

    public function getJobMaster($projectEntityId = '') {
        $data = '';
        if (!empty($projectEntityId)) {
            $jobMaster['JobMasterEntityId'] = '';
            $jobMaster['JobMasterName'] = '';
            $jobMaster['ProjectEntityId'] = $projectEntityId;
            $jobMaster['WorkflowMasterEntityId'] = '';
            $jobMaster['Token'] = '';
            $data = json_encode($jobMaster);
        }
        $result = $this->getRequest(JOB_MASTER_GET, $data);
        return json_decode($result);
    }

    public function getJobsOnBasisOfStatus($projectEntityId = '', $jobStatus = '') {
        $data = '';
        if (!empty($projectEntityId)) { 
            $getJob["JobEntityId"] = "";
            $getJob["JobEntityId"] = "";
            $getJob["JobEnrolmentInstanceEntityId"] = "";
            $getJob["JobMasterEntityId"] = "";
            $getJob["JobScheduledDate"] = "";
            $getJob["JobAddress"] = "";
            $getJob["JobStatus"] = ($jobStatus != "") ? $jobStatus : "";
            $getJob["JobCompletedDateTime"] = "";
            $getJob["JobDataset"] = "";
            $getJob["PersonEntityId"] = "";
            $getJob["ProjectEntityId"] = $projectEntityId;
            $getJob["WorkflowMasterEntityId"] = "";
            $getJob["Token"] = "";
            $data = json_encode($getJob);
            $result = $this->getRequest(JOB_GET, $data);
            return json_decode($result);
        }
    }

    public function getRequest($url, $data) { 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_URL, $url . $data);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "User-Agent" => USER_AGENT,
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = utf8_decode(curl_exec($curl));
        curl_close($curl); 
        return $response;
    }

    public function getQuestionierFromFormVersion($formVersion, $formDesigns) {
        $formDesign = [];
        foreach ($formDesigns as $formObject) { 
            if ($formObject->FormVersion == $formVersion) {
                $formDesign[] = $formObject;
                break;
            }
        }  
        return $formDesign[0];
    }

    public function getNewFormVersion($questions) { 
        $formVersionsList = [];
        foreach ($questions as $question) {
            $formVersionsList[] = (int)$question->FormVersion;
        }
        return (max($formVersionsList)+1).'.0'; 
    }

} ?>