<?php

class Workforce_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
    }

    public function workforceGet($workforceId = '') {
        $data = '';
        if (!empty($projectId)) {
            $workforce['ProjectEntityId'] = $projectId;
            $data .= json_encode($project);
        }
        $result = $this->getRequest(WORK_FORCE_GET, $data);
        return json_decode(utf8_decode($result));
    }

    public function workforceRoleGet() {
        $result = $this->getRequest(WORK_FORCE_ROLE_GET);
        return json_decode(utf8_decode($result));
    }

    public function statusGet() {
        $result = $this->getRequest(STATUS_GET);
        return json_decode(utf8_decode($result));
    }

    public function retrievePersonSearch($url, $method, $params = null) {
        $data = 'data=' . htmlspecialchars(rawurlencode($params));
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_URL, $url . $method);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "User-Agent" => USER_AGENT,
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = json_decode(utf8_decode(rawurldecode(trim(curl_exec($curl)))));
        curl_close($curl);
        return $response->data;
    }

    public function getRequest($url, $data = null) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_URL, $url . $data);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "User-Agent " => USER_AGENT,
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = utf8_decode(curl_exec($curl));
        curl_close($curl);
        return $response;
    }

}
