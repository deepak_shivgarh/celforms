<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends CI_Controller {
 
    public function __construct() {
    parent::__construct();
    $this->load->helper('celform');
    $this->load->model('project_model');
  }


    public function index($formVersion='', $jobMasterId='') { 
        $data['index'] = 'jobMaster';
        $data['title'] = 'JobMaster';
        $data['jobMasterData'] = '';
        $data['AllowableProperties'] = '';
        $data['questions'] = '';
        $data['jobMasters'] = $this->project_model->jobMasterGet('');
        $tableDataObject = json_decode(file(PROPERTIES_GET)[0]); 

        $table1='enrolment_instance'; $table2='job'; $table3='person';

        $data['enrolmentTableOptions'] = addslashes(get_properties_html(get_properties($tableDataObject, $table1), $table1, $dataSelected=''));
 
        $data['jobTableOptions'] = '<option value="" type=""></option><option value="Job.JobDataset" type="json">JobDataset</option>';

        $data['personTableOptions'] = addslashes(get_properties_html(get_properties($tableDataObject, $table3), $table3, $dataSelected=''));  

        $data['enrolmentPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table1), $table1, $dataSelected='')); 

        $data['jobPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table2), $table2, $dataSelected=''));

        $data['personPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table3), $table3, $dataSelected='')); 

        $data['action'] = (!empty($jobMasterId) ? 'update' : 'add' ); 
        if(!empty($jobMasterId)) { 
          $jd = $this->project_model->jobMasterGet($jobMasterId); 
          $data['jobMasterData'] = $jd->data[0]; 
          
          $data['questionData'] = $this->project_model->getQuestionierFromFormVersion($formVersion, $jd->data[0]->JobMasterSpecs->FormDesigns);
          $data['questions'] = $data['questionData']->Questions;   
          $data['questionsAllowableProperties'] = $jd->data[0]->JobMasterSpecs->AllowableProperties; 
        } 
        $this->load->view('include/header', $data);
        $data['projectData'] = $this->project_model->projectGet()->data;  
        $data['WorkforceRole'] = $this->project_model->WorkforceRoleGet()->data; 
        $data['tables'] = get_tables($this->project_model->propertiesGet()); 
        $this->load->view('forms', $data); 
        $this->load->view('include/footer', $data); 
    }

    public function version($formVersion='', $jobMasterId='') { 
        if ( empty($jobMasterId) ) {
          $this->session->set_flashdata('message', getCustomAlert('D', 'Something went wrong.'));
          redirect('jobMaster');
        }
        $data['index'] = 'jobMaster';
        $data['title'] = 'JobMaster';
        $data['jobMasterData'] = '';
        $data['AllowableProperties'] = '';
        $data['questions'] = '';
        $data['jobMasters'] = $this->project_model->jobMasterGet('');
        $tableDataObject = json_decode(file(PROPERTIES_GET)[0]); 

        $table1='enrolment_instance'; $table2='job'; $table3='person';

        $data['enrolmentTableOptions'] = addslashes(get_properties_html(get_properties($tableDataObject, $table1), $table1, $dataSelected=''));
        
        $data['jobTableOptions'] = '<option value="" type=""></option><option value="Job.JobDataset" type="json">JobDataset</option>';

        $data['personTableOptions'] = addslashes(get_properties_html(get_properties($tableDataObject, $table3), $table3, $dataSelected=''));  

        $data['enrolmentPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table1), $table1, $dataSelected='')); 

        $data['jobPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table2), $table2, $dataSelected=''));

        $data['personPropertiesJson'] = addslashes(getPropertiesJsonHtml($this->project_model->jsonPropertiesGet($table3), $table3, $dataSelected='')); 

        $data['action'] = 'Versioning'; 
          
        $jd = $this->project_model->jobMasterGet($jobMasterId); 
        $data['jobMasterData'] = $jd->data[0]; 

        $data['newJobVersion'] = $this->project_model->getNewFormVersion($jd->data[0]->JobMasterSpecs->FormDesigns);
        $data['questionData'] = $this->project_model->getQuestionierFromFormVersion($formVersion, $jd->data[0]->JobMasterSpecs->FormDesigns);
        $data['questions'] = $data['questionData']->Questions;   
        $data['questionsAllowableProperties'] = $jd->data[0]->JobMasterSpecs->AllowableProperties; 

        $this->load->view('include/header', $data);
        $data['projectData'] = $this->project_model->projectGet()->data;  
        $data['WorkforceRole'] = $this->project_model->WorkforceRoleGet()->data; 
        $data['tables'] = get_tables($this->project_model->propertiesGet()); 
        $this->load->view('forms', $data); 
        $this->load->view('include/footer', $data); 
    }
 

}
