<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class JobMaster extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
        $this->load->model('project_model');
        date_default_timezone_get();
    }

    public function index() {
        $data['index'] = 'Manage Job Master';
        $data['title'] = 'Manage Job Master';

        $data['jobMasterList'] = $this->project_model->jobMasterGet('');
        $this->load->view('include/header', $data);
        $this->load->view('jobMaster/manage', $data);
        $this->load->view('include/footer', $data);
    }

    public function add($projectId = '') {
        $data['index'] = 'Manage Job Master';
        $data['title'] = 'Manage Job Master';
        $data['action'] = (!empty($projectId) ? 'update' : 'add');
        $data['projectData'] = ''; 
        $this->load->view('jobMaster/index', $data);
        $this->load->view('include/footer', $data);
    }

    public function updateEligibiltyCriteriaJobMaster($jobMasterId = '') {
        if ($jobMasterId != '') {
            $data['index'] = 'Update Eligibility Criteria - JOB MASTER';
            $data['title'] = 'Update Eligibility Criteria - JOB MASTER';
            $jobMasterOBJ = $this->project_model->jobMasterGet($jobMasterId); 
            if(empty($jobMasterOBJ)) { redirect('/jobMaster'); }
            if (!isset($jobMasterOBJ->data[0]->EligibilityCriteria)) {
                $jobMasterOBJ->data[0]->JobMasterSpecs = (array) $jobMasterOBJ->data[0]->JobMasterSpecs;
                $jobMasterOBJ->data[0]->JobMasterSpecs["EligibilityCriteria"] = [];
                $jobMasterOBJ->data[0]->JobMasterSpecs = (object) $jobMasterOBJ->data[0]->JobMasterSpecs;
            }
            $data["jobMasterData"] = $jobMasterOBJ->data[0];
            $data['tables'] = get_tables(json_decode(file(PROPERTIES_GET)[0]));
            $data['allProjectList'] = $this->project_model->projectGet('');
            $this->load->view('include/header', $data);
            $this->load->view('/jobMaster/updateEligibilityCriteriaJobMaster', $data);
            $this->load->view('include/footer', $data);
        } else {
            show_404();
        }
    }

}
