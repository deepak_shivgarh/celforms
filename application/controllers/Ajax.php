<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('project_model');
        $this->load->library('common');
    }

    public function index() {
        $tableName = strtolower($this->input->post('tableName'));
        $dataSelected = $this->input->post('dataSelected');
        $tableDataObject = json_decode(file(PROPERTIES_GET)[0]);
        $this->get_properties_html(get_properties($tableDataObject, $tableName), $tableName, $dataSelected);
    }

    public function get_properties_html($propertiesDataObject, $tableName, $dataSelected) {
        $body = '';
        foreach ($propertiesDataObject as $value) { 
            $selectedPhpName = explode(".", $value->phpName)[1];
            $selected = ($value->phpName == $dataSelected ? 'selected="selected"' : '');
            $body .= '<option value="' . $value->phpName . '" type="' . $value->type . '" '.$selected.' >' . str_replace("_", "", $value->columnName) . '</option>'; 
        }
        echo $body;
    }

    public function getPropertiesJson() {
      $tableName = strtolower($this->input->post('tableName'));
      $dataSelected = $this->input->post('dataSelected');   
      $requestJson = '{"Tables":["'.ucfirst($tableName).'"],"GetAttributes":true}'; 
      $tableDataObject = json_decode(file(PROPERTIES_GET.$requestJson)[0]);  
      $tableName = strtolower($tableName);
      $columns = $tableDataObject->data->$tableName->Columns; 
      $body=$selected="";
      foreach ($columns as $value) { 
        $selected = ($value->phpName==$dataSelected ? 'selected="selected"' : ""); 
        if(strpos($value->phpName, '.') != false) 
          $body .= '<option '.$selected.' value="'.$value->phpName.'" '.$selected.'>'. substr(strrchr($value->phpName, "."), 1).'</option>'; 
      }  
      $body .= '<option value="'.str_replace('_', '', ucwords($tableName, "_")).'.'.str_replace('_', '', ucwords($tableName, "_")).(strtolower($tableName)=='job' ? 'Dataset' : 'Attributes').'" type="Other" '.($selected==".."?'selected="selected"':"").'>Other</option>'; 
      echo $body;
    }

    public function getPropertiesJsonAddProject() {
        $tableName = strtolower($this->input->post('tableName'));
        $dataPreSelected = $this->input->post('dataPreSelected'); 
        $requestJson = '{"Tables":["' . ucfirst($tableName) . '"],"GetAttributes":true}';
        $tableDataObject = json_decode(file(PROPERTIES_GET . $requestJson)[0]);
        $tableName = strtolower($tableName);
        $columns = $tableDataObject->data->$tableName->Columns;
        $testCaseArrayForOtherOption = []; 
        $isAlreadySelected = false;
        $body = '';
        foreach ($columns as $value) {
            $selected = '';
            if ($value->type == "json attribute") {
                $tmpForPhpNameSeperation = explode(".", $value->phpName);
                $tmpForPhpNameSeperation = array_pop($tmpForPhpNameSeperation);
                $testCaseArrayForOtherOption[] = $tmpForPhpNameSeperation;
                if ($value->phpName == $dataPreSelected) {
                    $isAlreadySelected = true;
                    $body .= '<option value="' . $value->phpName . '" selected="selected">' . ucwords(substr(strrchr($value->phpName, "."), 1)) . '</option>';
                } else {
                    $body .= '<option value="' . $value->phpName . '">' . ucwords(substr(strrchr($value->phpName, "."), 1)) . '</option>';
                }
            } 
        }
        $dataPreSelectedTmp = explode(".", $dataPreSelected);
        $dataPreSelectedTmp = array_pop($dataPreSelectedTmp);
        if ($isAlreadySelected == false && !in_array($testCaseArrayForOtherOption, $dataPreSelectedTmp) && $dataPreSelected != "") {
            $body .= '<option value="' . str_replace('_', '', ucwords($tableName, "_")) . '.' . str_replace('_', '', ucwords($tableName, "_")) .(strtolower($tableName)=='job' ? 'Dataset' : 'Attributes').'" type="Other" selected="selected">Other</option>';
        } else {
            $body .= '<option value="' . str_replace('_', '', ucwords($tableName, "_")) . '.' . str_replace('_', '', ucwords($tableName, "_")) . (strtolower($tableName)=='job' ? 'Dataset' : 'Attributes').'" type="Other">Other</option>';
        }
        echo $body;
    }
    
    public function projectAdd() {
        $requestJson = $this->input->post('requestJson');
        $action = $this->input->post('action'); 
        $requestJson = json_encode(json_decode($requestJson), JSON_UNESCAPED_UNICODE);
        $records = 0;
        $resData  = "";
        if ($action == 'add')
            $resData = $this->send(PROJECT_ADD, 'POST', $requestJson);
        if ($action == 'update')
            $resData = $this->send(PROJECT_UPDATE, 'POST', $requestJson);

        $records = $resData['totalRecords'];
        $error = $resData['error'];

        if ($records > 0) {
            if ($action == 'add')
                $this->session->set_flashdata('message', getCustomAlert('S', 'Project has been added successfully.'));
            if ($action == 'update')
                $this->session->set_flashdata('message', getCustomAlert('S', 'Project has been updated successfully.'));
            echo 1;
        }
        else {
            echo 0;
        }   
        die;
    } 

    public function jobMasterAdd() {
        $requestJson = $this->input->post('requestJson');
        $action = $this->input->post('action'); 
        $finalQuestionire = sortMultidimentionalArrayByKey (
                                $requestJson['JobMasterSpecs']['FormDesign']['Questions'], 
                                'SequenceOrder', 
                                SORT_ASC
                            );   
         
        # Reindexing array if elemented deleted from between and array index are in format like (0,1,3,5,6...)=> (0,1,2,3,4...) 
        $requestJson['JobMasterSpecs']['FormDesign']['Questions'] = $finalQuestionire;  
        $requestJson['JobMasterSpecs']['FormDesign']['EnrolmentForm'] = ($requestJson['JobMasterSpecs']['FormDesign']['EnrolmentForm']=='true');
        $requestJson['JobMasterSpecs']['AllowableProperties'] = reindexArray (
                                                                    array_unique (
                                                                        $requestJson['JobMasterSpecs']['AllowableProperties'],
                                                                        SORT_REGULAR
                                                                    )
                                                                );   
        $requestJson = json_encode($requestJson, JSON_UNESCAPED_UNICODE); 
        $records = 0;
        $resData = "";
        $error = "";
        $flag=0;
        if ($action == 'add')
            $resData = $this->send(JOB_MASTER_ADD, 'POST', $requestJson);         
        if ($action == 'update' || $action == 'Versioning') 
            $resData = $this->send(JOB_MASTER_UPDATE, 'POST', $requestJson);  

        $records = $resData['totalRecords'];
        $error = $resData['error'];

        if ($records > 0) {
            if ($action == 'add')
                $this->session->set_flashdata('message', getCustomAlert('S', 'Job Master has been added successfully.'));
            if ($action == 'update' || $action == 'Versioning')
                $this->session->set_flashdata('message', getCustomAlert('S', 'Job Master has been updated successfully.'));
            $flag=1;
        } else {
            $this->session->set_flashdata('message', getCustomAlert('D', 'Something went wrong.'));
            $flag=0;
        }
        $arr['isSuccess']=$flag;
        $arr['error'] = $error;
        $arr['jsonData']=json_decode($requestJson);
        echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        die;
    }

    public function isProjectEnrolled() {
        $projectId=$this->input->post('projectId'); 
        $jobMasterId=$this->input->post('jobMasterId');
        $projectName=$this->input->post('projectName');
        print_r($this->input->post()); 
        $jobMaster = $this->project_model->jobMasterGet('')->data;
        $flag=0;
        $selfJMFlag=0;
        $isAlreadyEnrolled=0;

        foreach ($jobMaster as $key => $value) { 
            if($projectName==strstr($value->WorkflowMasterWorkflowMasterName, '-', true)) { 
                if($jobMasterId==$value->JobMasterEntityId && $projectName==strstr($value->WorkflowMasterWorkflowMasterName, '-', true)) { $selfJMFlag=1; }
                if($value->JobMasterSpecs->FormDataset[0]->EnrolmentForm=='true') { $isAlreadyEnrolled=1; }
                else { echo 'not enrolled'; }
            } 
        }
        $arr['selfJMFlag']=$selfJMFlag;
        $arr['isAlreadyEnrolled']=$isAlreadyEnrolled;
        print_r($arr); 
    }
 
    public function send($url, $method, $params = null) {
        $data = 'data=' . htmlspecialchars(rawurlencode($params));
        $curl = curl_init();  
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_URL, $url . $method);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "User-Agent" => USER_AGENT, 
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = json_decode(utf8_decode(rawurldecode(trim(curl_exec($curl)))));
        curl_close($curl); 
        $arr['totalRecords'] = $response->totalRecords;
        $arr['error'] = (isset($response->error) ? $response->error : '');
        return $arr;
    }

}
