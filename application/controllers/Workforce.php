<?php

class Workforce extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
        $this->load->model('Workforce_model', 'workforceModel');
        date_default_timezone_get();
    }

    public function index() {
        $data['index'] = 'workforce';
        $data['title'] = 'Manage Workforce';
        $data["workforceList"] = $this->workforceModel->workforceGet();
        $this->load->view('include/header', $data);
        $this->load->view('workforce/manage', $data);
        $this->load->view('include/footer', $data);
    }

    public function personGetForm() {
        $data['index'] = 'workforce';
        $data['title'] = 'Search Person';
        $this->load->view('include/header', $data);
        $this->load->view('workforce/personGetForm', $data);
        $this->load->view('include/footer', $data);
    }

    public function getPersonAllDetails() {
        $requestJson = $this->input->post('requestJson');
        $serviceResponse = $this->workforceModel->retrievePersonSearch(WORK_FORCE_PERSON_SEARCH, 'POST', $requestJson);
        $serviceResponse = array_shift($serviceResponse);
        $response = json_encode($serviceResponse);
        echo $response;
    }

    public function showPersonSearchResult() {
        $data['index'] = 'workforce';
        $data['title'] = 'Person Search Results';
        $requestJson = $this->input->post('personParameters');
        $requestJson["pagenum"] = "";
        $requestJson["pagesize"] = "";
        $requestJson = json_encode($requestJson, JSON_UNESCAPED_UNICODE);
        if (!is_null($requestJson)) {
            $persons = $this->workforceModel->retrievePersonSearch(WORK_FORCE_PERSON_SEARCH, 'POST', $requestJson);
            if (!isset($persons->Error)) {
                $data["personList"] = $persons;
                $data['personsJsonEncodedData'] = json_encode($persons);
            } else {
                $data["noRecordFound"] = true;
            } 
            $this->load->view('include/header', $data);
            $this->load->view('workforce/personSearchResult', $data);
            $this->load->view('include/footer', $data);
        } else {
            show_404();
        }
    }

}
