<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('celform');
        $this->load->model('project_model');
        $this->load->model('Workforce_model', 'workforceModel');
        date_default_timezone_get();
    }

    public function index() {
        $data['index'] = 'projects';
        $data['title'] = 'Manage Projects';
        $data['projectList'] = $this->project_model->projectGet(''); 
        $this->load->view('include/header', $data);
        $this->load->view('projects/manage', $data);
        $this->load->view('include/footer', $data);
    }

    public function add($projectId = '') {
        $data['index'] = 'projects';
        $data['action'] = (!empty($projectId) ? 'update' : 'add' );
        if ($data['action'] == "add") {
            $data['title'] = 'Add Project';
        } else {
            $data['title'] = 'Update Project';
        }
        $data['projectData'] = '';
        $this->load->view('include/header', $data);
        $data['tables'] = get_tables(json_decode(file(PROPERTIES_GET)[0]));
        if (!empty($projectId)) {
            $data['ProjectEntityId'] = $projectId; 
            $data['projectData'] = $this->project_model->projectGet($projectId)->data[0];
        } 
        $data['allProjectList'] = $this->project_model->projectGet('');
        $data['allStatusList'] = $this->workforceModel->statusGet()->data;
        $data['allowedStatus'] = array("OPEN", "ACTIVE", "SUSPENDED", "COMPLETED");
        $this->load->view('projects/index', $data);
        $this->load->view('include/footer', $data);
    }

    public function navigateUserToLinkPage($projectEntityId, $projectName) {
        if ($projectEntityId && $projectName) {
            $data['index'] = 'projects';
            $data['title'] = 'Manage ' . $projectName . ' Project Content Visit Modules';
            $data["projectEntityId"] = $projectEntityId;
            $data["projectName"] = $projectName;
            $data["enrolmentUserCount"] = count(json_decode($this->project_model->getUsers($projectEntityId))->data);
            $data["monitorJobsCount"] = count($this->project_model->getJobsOnBasisOfStatus($projectEntityId, '')->data);
            $data["manageDataCount"] = count($this->project_model->getJobsOnBasisOfStatus($projectEntityId, 'CLOSED')->data);
            $data["manageJobMasterCount"] = count($this->project_model->getJobMaster($projectEntityId)->data);
            $this->load->view('include/header', $data);
            $this->load->view('projects/navigateToProjectLinks', $data);
            $this->load->view('include/footer', $data);
        } else {
            show_404();
        }
    }

    /* NAVIGATE TO USER LINKS ACTIONS STARTS HERE */

    public function manageEnrolmentUsers($projectEntityId, $projectName = '') {
        $data['index'] = 'projects';
        $data['title'] = 'Manage Users - ' . $projectName;
        $data["projectDetails"] = array(
            "projectEntityId" => $projectEntityId,
            "projectName" => $projectName,
        );
        $data["allEnrolledUsers"] = json_decode($this->project_model->getUsers($projectEntityId))->data;
        $this->load->view('include/header', $data);
        $this->load->view('projects/manageusers', $data);
        $this->load->view('include/footer', $data);
    }

    public function manageJobMaster($projectEntityId, $projectName = '') {
        $data['index'] = 'projects';
        $data['title'] = 'Manage Job Masters - ' . $projectName;
        $data["projectDetails"] = array(
            "projectEntityId" => $projectEntityId,
            "projectName" => $projectName,
        );
        $data["jobMasterList"] = $this->project_model->getJobMaster($projectEntityId);
        $this->load->view('include/header', $data);
        $this->load->view('projects/managejobmasters', $data);
        $this->load->view('include/footer', $data);
    }

    public function monitorJobs($projectName = '', $projectEntityId, $jobStatusRequired = '') {
        $data["index"] = 'projects';
        $data["projectDetails"] = array(
            "projectEntityId" => $projectEntityId,
            "projectName" => $projectName,
        );
        if ($jobStatusRequired == 'CLOSED') {
            $data["currentAction"] = "manage-data";
            $data["title"] = "Manage Data - " . $projectName;
            $data["pageHeading"] = "Manage Data - <a href='" . base_url('projects/navigateUserToLinkPage') . "/" . $data["projectDetails"]["projectEntityId"] . "/" . $data["projectDetails"]["projectName"] . "'>" . $projectName . "</a>";
        } else {
            $data["currentAction"] = "monitor-jobs";
            $data["title"] = "Monitor Jobs - " . $projectName;
            $data["pageHeading"] = "Monitor Jobs - <a href='" . base_url('projects/navigateUserToLinkPage') . "/" . $data["projectDetails"]["projectEntityId"] . "/" . $data["projectDetails"]["projectName"] . "'>" . $projectName . "</a>";
            $data["listOfAllWorkforces"] = $this->workforceModel->workforceGet()->data;
        }
        $data["jobsList"] = json_decode('{"version":"1.0","server":"ws2.community.org.in","timestamp":"2017-03-07 10:08:09","type":null,"totalRecords":1,"data":[{"JobEntityId":"11e7030f9b05f917aa6f06ba1a9d4ed7","JobEnrolmentInstanceEntityId":"11e7030e71ca3274aa6f06ba1a9d4ed7","JobName":"JMproject6.3","JobMasterEntityId":"11e70253d449ca87aa6f06ba1a9d4ed7","JobScheduledDate":"2017-03-07T08:25:28+00:00","JobAddress":null,"JobGps":null,"JobPlannedGeolocation":null,"JobActualGeolocation":null,"WorkforceWorkforceEntityId":"11e6cb2eea8ba062aa6f06ba1a9d4ed7","JobStatus":"DISPATCHED","JobCompletedDatetime":null,"JobDataset":{"updates":[{"NewValue":"250ceb7584393b917efa263811e60000","FieldUpdated":"StatusStatusEntityId","LastUpdatedAt":"2017-03-07 08:25:48","LastUpdatedBy":null,"OriginalValue":"11e6fa9dbd10295daa6f06ba1a9d4ed7"}],"FormDesign":{"Questions":[{"Hide":"false","Hint":"","Kind":"","Type":"inputLabel","Label":"label","ReadOnly":"false","Required":"false","Relevance":"false","mediaImage":"","DefaultValue":"","TableMapping":"Person.PersonAttributes.label","SequenceOrder":"3","RequiredMessage":"","QuestionEntityId":"11e7030f4c71085886ce06ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"selectOne","Label":"one","Options":[{"Text":"1","Value":"1","Default":"false"},{"Text":"2","Value":"2","Default":"false"}],"ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.1","SequenceOrder":"4","RequiredMessage":"","IsShowUnderlying":"false","QuestionEntityId":"11e7030f4c7117128b7406ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"selectMany","Label":"many","Options":[{"Text":"1","Value":"1","Default":"false"},{"Text":"2","Value":"2","Default":"false"},{"Text":"gfyu","Value":"gfyu","Default":"false"}],"ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.many","SequenceOrder":"5","RequiredMessage":"","IsShowUnderlying":"false","QuestionEntityId":"11e7030f4c7124d2ad8206ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Kind":"Integer","Type":"inputNumeric","Label":"Num","Range":"false","ReadOnly":"false","Required":"false","Relevance":"false","Validation":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.Num","SequenceOrder":"6","RequiredMessage":"","QuestionEntityId":"11e7030f4c71327e94f006ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"inputText","Label":"TEXT","Length":"false","ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.TEXT","SequenceOrder":"7","RequiredMessage":"","QuestionEntityId":"11e7030f4c7140fc921a06ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Kind":"DateTime","Type":"selectDate","Label":"DATE/TIME","Range":"false","ReadOnly":"false","Required":"false","Relevance":"false","DateFormat":"12","Validation":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.DT","SequenceOrder":"8","RequiredMessage":"","QuestionEntityId":"11e7030f4c714ec69a6a06ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"selectLocation","Label":"loc","ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.loc","SequenceOrder":"9","RequiredMessage":"","QuestionEntityId":"11e7030f4c715c68822406ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"inputYesNoDontKnow","Label":"YN","Options":[{"Text":"Yes","Value":"Yes","Default":"false"},{"Text":"No","Value":"No","Default":"false"}],"ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.YN","SequenceOrder":"10","RequiredMessage":"","IsShowUnderlying":"false","QuestionEntityId":"11e7030f4c716a14a66306ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"getMediaAudio","Label":"aud","ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.aud","SequenceOrder":"11","RequiredMessage":"","QuestionEntityId":"11e7030f4c7177aca4ae06ba1a9d4ed7","ReferredQuestion":"false"},{"Hide":"false","Hint":"","Type":"getMediaImage","Label":"img","ReadOnly":"false","Required":"false","Relevance":"false","DefaultValue":"","TableMapping":"Person.PersonAttributes.img","SequenceOrder":"12","RequiredMessage":"","QuestionEntityId":"11e7030f4c718594a1b106ba1a9d4ed7","ReferredQuestion":"false"}],"FormVersion":"1.0","EnrolmentForm":true,"StatusStatusEntityId":"250ceb7584393b9187e2263811e60000"},"FormDataset":{"Responses":[{"Type":"text","Value":"","Remark":"","TableMapping":"Person.PersonAttributes.label","QuestionEntityId":"11e7030f4c71085886ce06ba1a9d4ed7"},{"Type":"text","Value":"1","Remark":"","TableMapping":"Person.PersonAttributes.1","QuestionEntityId":"11e7030f4c7117128b7406ba1a9d4ed7"},{"Type":"text","Value":"2","Remark":"","TableMapping":"Person.PersonAttributes.many","QuestionEntityId":"11e7030f4c7124d2ad8206ba1a9d4ed7"},{"Type":"text","Value":"3","Remark":"","TableMapping":"Person.PersonAttributes.Num","QuestionEntityId":"11e7030f4c71327e94f006ba1a9d4ed7"},{"Type":"text","Value":"4","Remark":"","TableMapping":"Person.PersonAttributes.TEXT","QuestionEntityId":"11e7030f4c7140fc921a06ba1a9d4ed7"},{"Type":"text","Value":"5","Remark":"","TableMapping":"Person.PersonAttributes.DT","QuestionEntityId":"11e7030f4c714ec69a6a06ba1a9d4ed7"},{"Type":"text","Value":"6","Remark":"","TableMapping":"Person.PersonAttributes.loc","QuestionEntityId":"11e7030f4c715c68822406ba1a9d4ed7"},{"Type":"text","Value":"7","Remark":"","TableMapping":"Person.PersonAttributes.YN","QuestionEntityId":"11e7030f4c716a14a66306ba1a9d4ed7"},{"Type":"text","Value":"8","Remark":"","TableMapping":"Person.PersonAttributes.aud","QuestionEntityId":"11e7030f4c7177aca4ae06ba1a9d4ed7"},{"Type":"text","Value":"9","Remark":"","TableMapping":"Person.PersonAttributes.img","QuestionEntityId":"11e7030f4c718594a1b106ba1a9d4ed7"}],"FormVersion":"1.0","EnrolmentForm":true},"AllowableProperties":[{"Property":"Person.PersonAttributes.label","PropertyType":"string"},{"Property":"Person.PersonAttributes.1","PropertyType":"string"},{"Property":"Person.PersonAttributes.many","PropertyType":"string"},{"Property":"Person.PersonAttributes.Num","PropertyType":"Integer"},{"Property":"Person.PersonAttributes.TEXT","PropertyType":"string"},{"Property":"Person.PersonAttributes.DT","PropertyType":"date"},{"Property":"Person.PersonAttributes.loc","PropertyType":"string"},{"Property":"Person.PersonAttributes.YN","PropertyType":"string"},{"Property":"Person.PersonAttributes.aud","PropertyType":"string"},{"Property":"Person.PersonAttributes.img","PropertyType":"string"},{"Property":"Person.PersonGender","PropertyType":"string"},""],"EligibilityCriteria":null,"WorkforceOperatorId":"heeba.kausar"},"StatusStatusEntityId":"250ceb7584393b917efa263811e60000"}],"Token":"NzkwNmM4ZDY4NjgxYzljYTBjMzI4NjMwOGE3ZmI1MzQgMTQ4ODg4MTI4OQ","ACK":"42695df671e149cfb44e6f4f1dc94a36"}')->data;
        $data["jobsListJson"] = json_encode($data["jobsList"]);
        $this->load->view('include/header', $data);
        $this->load->view('projects/monitorjobs', $data);
        $this->load->view('include/footer', $data);
    }

    public function getEnrolmentUsersByInstanceId() {
        if ($_POST['enrolmentInstanceId']) {
            echo json_encode(json_decode($this->project_model->getUsers('', $_POST['enrolmentInstanceId']))->data);
        }
    }

}
