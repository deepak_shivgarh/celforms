Date.prototype.manipulateHours = function (h, operatorUse) {
    var dateTime = this;
    if (operatorUse === "-") {
        dateTime = moment(dateTime).subtract(h, "hours").format();
    } else if (operatorUse === "+") {
        dateTime = moment(dateTime).add(h, "hours").format();
    }
    dateTime = new Date(dateTime);
    return dateTime;
};
var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
var merge24HourTimeToDateObject = function (time) {
    var dateObj = new Date();
    var twentyFourHourFormatTime = [];
    twentyFourHourFormatTime["hour"] = moment(time, ["hh:mm:ss a"]).format("HH");
    twentyFourHourFormatTime["minute"] = moment(time, ["hh:mm:ss a"]).format("mm");
    twentyFourHourFormatTime["seconds"] = moment(time, ["hh:mm:ss a"]).format("ss");
    dateObj.setHours(twentyFourHourFormatTime["hour"]);
    dateObj.setMinutes(twentyFourHourFormatTime["minute"]);
    dateObj.setSeconds(twentyFourHourFormatTime["seconds"]);
    return dateObj;
};
var checkDateTimeFormatAndReturnInteger = function (dateTime) {
    var numericDateFormat = "";
    if (moment(dateTime, 'hh:mm:ss a').format('hh:mm:ss a') === dateTime) {
        numericDateFormat = merge24HourTimeToDateObject(dateTime);
    } else if (moment(dateTime, 'HH:mm:ss').format('HH:mm:ss') === dateTime) {
        numericDateFormat = merge24HourTimeToDateObject(dateTime);
    } else if (moment(dateTime, 'YYYY-MM-DD hh:mm:ss a').format('YYYY-MM-DD hh:mm:ss a') === dateTime) {
        numericDateFormat = new Date(moment(dateTime, 'YYYY-MM-DD hh:mm:ss a').format('YYYY-MM-DD HH:mm:ss'));
    } else if (moment(dateTime, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss') === dateTime) {
        numericDateFormat = new Date(dateTime);
    } else if (moment(dateTime, 'YYYY-MM-DD').format('YYYY-MM-DD') === dateTime) {
        numericDateFormat = new Date(dateTime + " 00:00:00");
    }
    return numericDateFormat;
};
var bindDynamicValueWithStaticHtmlTemplate = function (fieldType, dynamicValue, indexSequence) {
    var v = dynamicValue;
    var returnableHtmlTemplate = "";
    var questionNumber = (parseInt(indexSequence) + 1);
    var isCheckRequired = function (reqKey) {
        if (reqKey === "true") {
            return '<span>*</span>';
        } else {
            return '';
        }
    };
    var isCheckHidden = function (hideKey, relevanceKey) {
        if (hideKey === "true") {
            return 'style="display:none;"';
        } else {
            return '';
        }
    };
    var isCheckReadOnly = function (readOnlyKey) {
        if (readOnlyKey === "true") {
            return 'readonly="true"';
        } else {
            return '';
        }
    };
    var assignRangeAttribute = function (completeValueObject) {
        if (completeValueObject.hasOwnProperty("Range")) {
            return ' min="' + completeValueObject.Range.Min
                    + '" onblur="validateNumericRange(this,' + completeValueObject.Range.Min + ',' + completeValueObject.Range.Max + ');"';
        } else {
            return '';
        }
    };
    var assignLengthAttribute = function (completeValueObject) {
        if (completeValueObject.hasOwnProperty("Length")) {
            return '" onblur="validateTextRange(this,' + completeValueObject.Length.Min + ',' + completeValueObject.Length.Max + ');"';
        } else {
            return '';
        }
    };
    var assignDefaultValueAttribute = function (completeValueObject) {
        if (completeValueObject.hasOwnProperty("DefaultValue") && completeValueObject.DefaultValue) {
            return ' value="' + completeValueObject.DefaultValue + '"';
        } else {
            return '';
        }
    };
    var isCheckUnderLying = function (completeValueObject, currentOptionIndexObj) {
        if (completeValueObject.IsShowUnderlying === "true") {
            return currentOptionIndexObj.Text;
        } else {
            return currentOptionIndexObj.Value;
        }
    };
    var printCheckBoxAndRadio = function (completeValueObject, currentOptionIndexObj, typeOfInput, itemCount) {
        var isReadOnly = (v.ReadOnly === "true" ? "disabled" : "");
        if (typeOfInput === "yesNoDontKnow") {
            if (currentOptionIndexObj.Text === "Yes" || currentOptionIndexObj.Text === "No") {
                return '<div class="radio tablet-view-yesno-radio-buttons tablet-view-radio-' + currentOptionIndexObj.Text + '-field">\n\
                            <label class="tablet-view-radio-' + currentOptionIndexObj.Text + '-field-label" for="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '">' + isCheckUnderLying(completeValueObject, currentOptionIndexObj) + '</label>\n\
                            <input data-relevance-tablemapping="' + v.TableMapping + '" ' + isReadOnly + ' class="tablet-view-track-relevance" type="radio" data-is-checked="' + (currentOptionIndexObj.Default === "true" ? currentOptionIndexObj.Text + '-' + itemCount : "") + '" value="' + currentOptionIndexObj.Value + '" name="' + completeValueObject.Label + '" id="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '" ' + (currentOptionIndexObj.Default === "true" ? "checked='true'" : "") + '>\n\
                            <div class="check"></div>\n\
                        </div>';
            } else {
                return '<div class="radio tablet-view-yesno-radio-buttons tablet-view-radio-dont-know-field">\n\
                            <label class="tablet-view-radio-dont-know-field-label" for="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '">' + isCheckUnderLying(completeValueObject, currentOptionIndexObj) + '</label>\n\
                            <input data-relevance-tablemapping="' + v.TableMapping + '" ' + isReadOnly + ' class="tablet-view-track-relevance" type="radio" data-is-checked="' + (currentOptionIndexObj.Default === "true" ? "true" : "") + '" value="' + currentOptionIndexObj.Value + '" name="' + completeValueObject.Label + '" id="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '" ' + (currentOptionIndexObj.Default === "true" ? "checked='true'" : "") + '>\n\
                            <div class="check"></div>\n\
                        </div>';
            }
        } else {
            return '<div class="' + typeOfInput + ' tablet-view-' + typeOfInput + '-field">\n\
                        <label class="tablet-view-' + typeOfInput + '-field-label" for="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '">' + isCheckUnderLying(completeValueObject, currentOptionIndexObj) + '</label>\n\
                        <input data-relevance-tablemapping="' + v.TableMapping + '" ' + isReadOnly + ' class="tablet-view-track-relevance" type="' + typeOfInput + '" value="' + currentOptionIndexObj.Value + '" name="' + completeValueObject.Label + '" id="' + typeOfInput + '-' + itemCount + '-' + questionNumber + '" ' + (currentOptionIndexObj.Default === "true" ? "checked='true'" : "") + '>\n\
                        <div class="check"></div>\n\
                    </div>';
        }
    };
    var modifyBase64Value = function (base64Value) {
        var splittedValue = base64Value.split(",");
        splittedValue.splice(1, 0, ";base64,");
        return "data:" + splittedValue.join("");
    };
    var validateBase64ValueType = function (base64Value) {
        var splittedValue = base64Value.split(",");
        return splittedValue[0];
    };
    var showRangeMessage = function (minRange, maxRange) {
        return '<p style="color:red;display:none;font-weight:bold;" class="tablet-view-error-input-field">Value should be between ' + minRange + ' and ' + maxRange + '</p>';
    };
    var showDateRangeMessage = function (minRangeDate, maxRangeDate, messageType) {
        return '<span id="helpBlock" class="help-block form-invalid-validation-preview-message-static">Kindly select ' + messageType + ' between ' + minRangeDate + ' and ' + maxRangeDate + '</span>';
    };
    var showTextRangeMessage = function (minRange, maxRange) {
        return '<p style="color:red;display:none;font-weight:bold;" class="tablet-view-error-input-field">Please enter minimum: ' + minRange + ' and maximum: ' + maxRange + ' characters.</p>';
    };
    var showRequiredMessage = function (completeObjectReference) {
        if (v.Required === "true") {
            var errorHtmlTemplate = '<p style="color:red;margin:-18px 0px 0px 0px;display:none;font-weight:bold;" class="tablet-view-error-required-input-field">';
            if (v.hasOwnProperty("RequiredMessage") && v.RequiredMessage !== "") {
                errorHtmlTemplate += completeObjectReference.RequiredMessage;
            } else {
                errorHtmlTemplate += "This field is required";
            }
            errorHtmlTemplate += "</p>";
            return errorHtmlTemplate;
        } else {
            return '';
        }
    };
    var printHintValue = function (hintValue) {
        if (hintValue !== "") {
            return '<div class="tablet-view-help-text-block"><span>Hint: </span>' + v.Hint + '</div>';
        } else {
            return '<div class="tablet-view-help-text-block"></div>';
        }

    };
    var assignStartAndEndDate = function (rangeObject) {
        if (rangeObject !== false && rangeObject.hasOwnProperty("Min") && rangeObject.hasOwnProperty("Max")) {
            return rangeObject.Min + '__' + rangeObject.Max;
        } else {
            return '';
        }
    };
    returnableHtmlTemplate += '<div class="form-group container-identification-' + v.SequenceOrder + '" ' + isCheckHidden(v.Hide, v.Relevance) + '>\n\
                            <label class="tablet-view-label" data-original-label-text="Q' + questionNumber + ". " + v.Label + isCheckRequired(v.Required)
            + '" data-question-pipping-tablemapping-relevance="' + v.ReferredQuestion + '">Q' + questionNumber + '. ' + v.Label + isCheckRequired(v.Required) + '</label>' + printHintValue(v.Hint);
    returnableHtmlTemplate += showRequiredMessage(v);
    switch (fieldType) {
        case "inputLabel":
            if (v.mediaImage !== "") {
                switch (v.Kind) {
                    case "mediaImage":
                        if (regexp.test(v.mediaImage)) {
                            returnableHtmlTemplate += '<img src="' + v.mediaImage + '" style="height:150px;margin: 10px;max-width: 200px;"/>';
                        } else {
                            if (validateBase64ValueType(v.mediaImage) === "image/jpeg" || validateBase64ValueType(v.mediaImage) === "image/png" || validateBase64ValueType(v.mediaImage) === "image/gif") {
                                returnableHtmlTemplate += '<img src="' + modifyBase64Value(v.mediaImage) + '" style="height:150px;margin: 10px;max-width: 200px;"/>';
                            }
                        }
                        break;
                    case "mediaAudio":
                        if (regexp.test(v.mediaImage)) {
                            returnableHtmlTemplate += '<audio controls style="margin:10px;"><source src="' + v.mediaImage + '" type="audio/mp3"></source></audio>';
                        } else {
                            if (validateBase64ValueType(v.mediaImage) === "audio/mp3") {
                                returnableHtmlTemplate += '<audio controls style="margin:10px;"><source src="' + modifyBase64Value(v.mediaImage) + '" type="audio/mp3"></source></audio>';
                            }
                        }
                        break;
                    case "mediaVideo":
                        if (regexp.test(v.mediaImage)) {
                            returnableHtmlTemplate += '<video controls style="margin:10px;"><source src="' + v.mediaImage + '" type="video/mp4"></source></video>';
                        } else {
                            if (validateBase64ValueType(v.mediaImage) === "video/mp4") {
                                returnableHtmlTemplate += '<video controls style="margin:10px;"><source src="' + modifyBase64Value(v.mediaImage) + '" type="video/mp4"></source></video>';
                            }
                        }
                        break;
                }
            }
            break;
        case "selectOne":
            var i = 1;
            $.each(v.Options, function (k1, v1) {
                returnableHtmlTemplate += printCheckBoxAndRadio(v, v1, "radio", i);
                i++;
            });
            break;
        case "selectMany":
            var i = 1;
            $.each(v.Options, function (k1, v1) {
                returnableHtmlTemplate += printCheckBoxAndRadio(v, v1, "checkbox", i);
                i++;
            });
            break;
        case "inputNumeric":
            returnableHtmlTemplate += showRangeMessage((v.Range !== "false") ? v.Range.Min : "", (v.Range !== "false") ? v.Range.Max : "")
                    + '<input data-number-type="' + v.Kind + '" data-relevance-tablemapping="' + v.TableMapping + '" type="text" onkeypress="return bindInputForNumericEntryOnly(event);" class="form-control tablet-view-input-field tablet-view-track-relevance tablet-view-number-type tablet-view-number-validation" '
                    + isCheckReadOnly(v.ReadOnly) + assignDefaultValueAttribute(v) + assignRangeAttribute(v) + '>';
            break;
        case "inputText":
            returnableHtmlTemplate += showTextRangeMessage((v.Length !== "false") ? v.Length.Min : "", (v.Length !== "false") ? v.Length.Max : "");
            returnableHtmlTemplate += '<input data-relevance-tablemapping="' + v.TableMapping + '" type="text" class="form-control tablet-view-input-field tablet-view-track-relevance" ' + isCheckReadOnly(v.ReadOnly) + assignDefaultValueAttribute(v) + assignLengthAttribute(v) + '>';
            break;
        case "inputYesNoDontKnow":
            var i = 1;
            $.each(v.Options, function (k1, v1) {
                returnableHtmlTemplate += printCheckBoxAndRadio(v, v1, "yesNoDontKnow", i);
                i++;
            });
            break;
        case "selectLocation":
            var readOnly = (v.ReadOnly === "true") ? "disabled" : "";
            returnableHtmlTemplate += '<div class="tablet-view-location-input">\n\
                                    <div class="tablet-view-location-input-splitted-fields">\n\
                                        <div class="split-1">\n\
                                            <label class="tablet-view-location-label-help">Latitude</label>\n\
                                            <input type="text" class="form-control tablet-view-input-field" disabled>\n\
                                        </div>\n\
                                        <div class="split-2">\n\
                                            <label class="tablet-view-location-label-help">Longitude</label>\n\
                                            <input type="text" class="form-control tablet-view-input-field" disabled>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div style="clear:both;"></div>\n\
                                    <div class="tablet-view-location-label-help">Enter Latitude and Longitude values in decimal-degrees.</div>\n\
                                    <div class="tablet-view-location-input-automatic-get-location">\n\
                                        <button data-is-location-input="true" data-relevance-tablemapping="' + v.TableMapping + '" class="btn btn-default tablet-view-location-use-autofeature tablet-view-track-relevance" type="button" ' + readOnly + '>\n\
                                            Pick Automatically\n\
                                        </button>\n\
                                    </div>\n\
                                </div>';
            break;
        case "selectDate":
            var defaultDateTime = "";
            if (v.IsSetCurrentDate === "true") {
                var momentHelper = moment();
                var inputFieldValue = "";
                if (v.Kind === "Date") {
                    inputFieldValue = momentHelper.format("YYYY-MM-DD");
                } else if (v.Kind === "Time") {
                    if (v.DateFormat == 12) {
                        inputFieldValue = momentHelper.format("hh:mm:ss A");
                    } else {
                        inputFieldValue = momentHelper.format("HH:mm:ss");
                    }
                } else if (v.Kind === "DateTime") {
                    if (v.DateFormat == 12) {
                        inputFieldValue = momentHelper.format("YYYY-MM-DD hh:mm:ss A");
                    } else {
                        inputFieldValue = momentHelper.format("YYYY-MM-DD HH:mm:ss");
                    }
                }
                defaultDateTime = " value='" + inputFieldValue + "'";
            }
            returnableHtmlTemplate += '<div class="inner-addon right-addon">\n\
                                    <i class="glyphicon glyphicon-calendar"></i>\n\
                                    <input data-field-type="date" data-date-range-startdate-enddate="' + assignStartAndEndDate(v.Range) + '" data-relevance-tablemapping="' + v.TableMapping + '" data-date-type="' + v.Kind + '" data-time-hourly-format="' + v.DateFormat + '" type="text" class="form-control tablet-view-input-field tablet-view-datetimepicker tablet-view-track-relevance tablet-view-date-validation" readonly' + defaultDateTime + '>\n\
                                </div>';
            if (v.Range.hasOwnProperty("Min") && v.Range.hasOwnProperty("Max")) {
                returnableHtmlTemplate += showDateRangeMessage(v.Range.Min, v.Range.Max, v.Kind);
            }
            break;
        case "getMediaImage":
            returnableHtmlTemplate += '<label for="image-upload-' + indexSequence + '" class="media-file-upload">\n\
                                    <img class="icon" src="' + base_url + 'assets/admin/images/icon-preview-form-image-upload.png" width="100" height="100">\n\
                                </label>\n\
                                <input data-relevance-tablemapping="' + v.TableMapping + '" type="file" class="form-control media-file-upload tablet-view-track-relevance" id="image-upload-' + indexSequence + '" style="display:none;">\n\
                                <div class="media-preview-container"></div>';
            break;
        case "getMediaAudio":
            returnableHtmlTemplate += '<label for="audio-upload-' + indexSequence + '" class="media-file-upload">\n\
                                    <img class="icon" src="' + base_url + 'assets/admin/images/icon-preview-form-audio-upload.png" width="100" height="100">\n\
                                </label>\n\
                                <input data-relevance-tablemapping="' + v.TableMapping + '" type="file" class="form-control media-file-upload tablet-view-track-relevance" id="audio-upload-' + indexSequence + '" style="display:none;">\n\
                                <div class="media-preview-container"></div>';
            break;
        case "getMediaVideo":
            returnableHtmlTemplate += '<label for="video-upload-' + indexSequence + '" class="media-file-upload">\n\
                                    <img class="icon" src="' + base_url + 'assets/admin/images/icon-preview-form-video-upload.png" width="100" height="100">\n\
                                </label>\n\
                                <input data-relevance-tablemapping="' + v.TableMapping + '" type="file" class="form-control media-file-upload tablet-view-track-relevance" id="video-upload-' + indexSequence + '" style="display:none;">\n\
                                <div class="media-preview-container"></div>';
            break;
        case "inputCalculate":
            returnableHtmlTemplate += '<input type="text" data-relevance-tablemapping="' + v.TableMapping + '" class="form-control tablet-view-calculation-field tablet-view-input-field tablet-view-track-relevance" readonly/>';
            break;
    }
    returnableHtmlTemplate += '</div>';
    return returnableHtmlTemplate;
};
var isQuestionPippingApplicable = function (exactValueToGetReplaced, valueToBeReplacedWith) {
    return exactValueToGetReplaced.replace("{{}}", valueToBeReplacedWith);
};
var isQuestionPippingApplicableHtmlHelper = function ($currentElementOBJREF) {
    var currentElementTableMapping = $currentElementOBJREF.attr("data-relevance-tablemapping");
    var currentElementValue = "";
    var currentElementType = $currentElementOBJREF.attr("type");
    if (currentElementType === "radio" && $currentElementOBJREF.is(":checked")) {
        currentElementValue = $currentElementOBJREF.val();
    } else if (currentElementType === "checkbox" && $currentElementOBJREF.is(":checked")) {
        currentElementValue = $currentElementOBJREF.val();
    } else if (currentElementType !== "radio" && currentElementType !== "checkbox") {
        currentElementValue = $currentElementOBJREF.val();
    }
    $(".tablet-view-form .form-group").each(function () {
        var $currentIndexLabelObject = $(this).find(".tablet-view-label");
        var currentIndexTableMapping = $currentIndexLabelObject.attr("data-question-pipping-tablemapping-relevance");
        var currentIndexOrginalLabel = $currentIndexLabelObject.attr("data-original-label-text");
        if (currentIndexTableMapping === currentElementTableMapping && currentElementValue !== "") {
            $currentIndexLabelObject.html(isQuestionPippingApplicable(currentIndexOrginalLabel, currentElementValue));
        }
    });
};
var bindOperatorRespectiveOfParameters = function (comparisonParameter) {
    switch (comparisonParameter) {
        case "equal":
            return " == ";
            break;
        case "not_equal":
            return " != ";
            break;
        case "is_empty":
            return " == '' ";
            break;
        case "not_is_empty":
            return " != '' ";
            break;
        case "less_than":
            return " < ";
            break;
        case "less_than_or_equal":
            return " <= ";
            break;
        case "greater_than":
            return " > ";
            break;
        case "greater_than_or_equal":
            return " >= ";
            break;
        case "OR":
            return " || ";
            break;
        case "AND":
            return " && ";
            break;
    }
};
var getValueWithReferencingToRelevanceTableMapping = function (objectTableMapping) {
    var matchedTableMappingValue = "";
    var checkDataTypeAssigned = function (valueToCheck) {
        if (checkForNumericDataType(valueToCheck)) {
            return valueToCheck;
        } else {
            valueToCheck = valueToCheck.toLowerCase();
            valueToCheck = valueToCheck.replace(/'/g, "\\'");
            return "'" + valueToCheck + "'";
        }
    };
    $(".tablet-view-track-relevance").each(function (i) {
        var currentElementTablemapping = $(this).attr("data-relevance-tablemapping");
        var typeOfElement = $(this).attr("type");
        if (currentElementTablemapping === objectTableMapping) {
            if (typeOfElement === "radio" && $(this).is(":checked")) {
                matchedTableMappingValue = checkDataTypeAssigned($(this).val());
            } else if (typeOfElement === "checkbox" && $(this).is(":checked")) {
                var parentClass = $(this).parent().parent().attr("class");
                parentClass = parentClass.split(" ")[1];
                var searchIDs = $('.' + parentClass + ' :input:checked').map(function () {
                    return checkDataTypeAssigned($(this).val());
                }).get();
                matchedTableMappingValue = searchIDs;
            } else if (typeOfElement !== "radio" && typeOfElement !== "checkbox" && typeOfElement !== "number" && $(this).attr("data-field-type") !== "date") {
                matchedTableMappingValue = checkDataTypeAssigned($(this).val());
            } else if ($(this).attr("data-field-type") === "date") {
                matchedTableMappingValue = Number(checkDateTimeFormatAndReturnInteger($(this).val().toLowerCase()));
            } else if (typeOfElement === "number") {
                matchedTableMappingValue = $(this).val();
            }
        }
    });
    if (matchedTableMappingValue) {
        return matchedTableMappingValue;
    } else {
        return "''";
    }
};
var getFieldTypeRespectiveToTableMapping = function (comparableTableMapping) {
    var fieldType = "";
    $(".tablet-view-track-relevance").each(function (i) {
        var tableMapping = $(this).attr("data-relevance-tablemapping");
        if (tableMapping === comparableTableMapping) {
            fieldType = $(this).attr("type");
            return false;
        }
    });
    return fieldType;
};
var applyCheckValidateAndRemoveRelevance = function (finalProductionJson, currentWorkingElementObject) {
    var currentElementsTablemapping = currentWorkingElementObject.attr("data-relevance-tablemapping");
    var currentElementsFieldType = currentWorkingElementObject.attr("type");
    var _childTableMapping = [];
    var _pluckChildTableMapping = function (currentIterableItem) {
        $.each(currentIterableItem, function (kChildTableMap, vChildTableMap) {
            _childTableMapping.push(vChildTableMap.Tablemapping);
            if (vChildTableMap.Child !== false) {
                _pluckChildTableMapping(vChildTableMap.Child);
            }
        });
    };
    var checkOperatorTypeForIsEmptyAndNotIsEmpty = function (conditionType, valueToCompareWith) {
        valueToCompareWith = valueToCompareWith.toLowerCase();
        if (conditionType === "is_empty" || conditionType === "not_is_empty") {
            return bindOperatorRespectiveOfParameters(conditionType);
        } else if (valueToCompareWith.indexOf(":") > 0 || valueToCompareWith.indexOf("-") > 0) {
            return bindOperatorRespectiveOfParameters(conditionType) + Number(checkDateTimeFormatAndReturnInteger(valueToCompareWith));
        } else {
            valueToCompareWith = valueToCompareWith.replace(/'/g, "\\'");
            if (checkForNumericDataType(valueToCompareWith)) {
                return bindOperatorRespectiveOfParameters(conditionType) + valueToCompareWith;
            } else {
                return bindOperatorRespectiveOfParameters(conditionType) + "'" + valueToCompareWith + "'";
            }
        }
    };
    var checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox = function (valueToCompareWith) {
        valueToCompareWith = valueToCompareWith.toLowerCase();
        valueToCompareWith = valueToCompareWith.replace(/'/g, "\\'");
        if (checkForNumericDataType(valueToCompareWith)) {
            return valueToCompareWith;
        } else {
            return "'" + valueToCompareWith + "'";
        }
    };
    var bindOperatorRespectiveOfParametersCheckbox = function (comparisonParameter) {
        switch (comparisonParameter) {
            case "equal":
                return " >= 0 ";
                break;
            case "not_equal":
                return " == -1 ";
                break;
        }
    };
    var makeRelevanceConditionKINDCONDITION = function (completeRelevanceObject) {
        var relevanceCondition = "";
        $.each(completeRelevanceObject, function (kR, vR) {
            if (getFieldTypeRespectiveToTableMapping(vR.Tablemapping) === "checkbox") {
                relevanceCondition += ((vR.hasOwnProperty("Join")) ? bindOperatorRespectiveOfParameters(vR.Join) : "")
                        + " $.inArray(" + checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox(vR.Value) + ",[" + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + "]) "
                        + bindOperatorRespectiveOfParametersCheckbox(vR.Condition);
            } else {
                relevanceCondition += ((vR.hasOwnProperty("Join")) ? bindOperatorRespectiveOfParameters(vR.Join) : "")
                        + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + checkOperatorTypeForIsEmptyAndNotIsEmpty(vR.Condition, vR.Value);
            }
        });
        return relevanceCondition;
    };
    var makeRelevanceConditionKINDGROUP = function (completeRelevanceObject) {
        var relevanceGROUPCondition = "";
        $.each(completeRelevanceObject, function (kR, vR) {
            if (vR.Child === false) {
                if (getFieldTypeRespectiveToTableMapping(vR.Tablemapping) === "checkbox") {
                    relevanceGROUPCondition += ((vR.hasOwnProperty("Join")) ? bindOperatorRespectiveOfParameters(vR.Join) : "")
                            + "(" + " $.inArray(" + checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox(vR.Value) + ",[" + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + "]) " + bindOperatorRespectiveOfParametersCheckbox(vR.Condition) + ")";
                } else {
                    relevanceGROUPCondition += ((vR.hasOwnProperty("Join")) ? bindOperatorRespectiveOfParameters(vR.Join) : "")
                            + "(" + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + checkOperatorTypeForIsEmptyAndNotIsEmpty(vR.Condition, vR.Value) + ")";
                }
            } else {
                var _recursiveFramedChildCondition = "";
                var recursiveMethodToFrameChildConditions = function (traversableObject, _recursiveStep) {
                    $.each(traversableObject, function (kRecChild, vRecChild) {
                        if (vRecChild.Kind === "condition") {
                            if (getFieldTypeRespectiveToTableMapping(vRecChild.Tablemapping) === "checkbox") {
                                _recursiveFramedChildCondition += bindOperatorRespectiveOfParameters(vRecChild.Join)
                                        + " $.inArray(" + checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox(vRecChild.Value) + ",[" + getValueWithReferencingToRelevanceTableMapping(vRecChild.Tablemapping) + "]) " + bindOperatorRespectiveOfParametersCheckbox(vRecChild.Condition);
                            } else {
                                _recursiveFramedChildCondition += bindOperatorRespectiveOfParameters(vRecChild.Join)
                                        + "" + getValueWithReferencingToRelevanceTableMapping(vRecChild.Tablemapping) + "" + checkOperatorTypeForIsEmptyAndNotIsEmpty(vRecChild.Condition, vRecChild.Value);
                            }
                        } else if (vRecChild.Kind === "group") {
                            if (getFieldTypeRespectiveToTableMapping(vRecChild.Tablemapping) === "checkbox") {
                                _recursiveFramedChildCondition += bindOperatorRespectiveOfParameters(vRecChild.Join)
                                        + "(" + " $.inArray(" + checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox(vRecChild.Value) + ",[" + getValueWithReferencingToRelevanceTableMapping(vRecChild.Tablemapping) + "]) " + bindOperatorRespectiveOfParametersCheckbox(vRecChild.Condition);
                            } else {
                                _recursiveFramedChildCondition += bindOperatorRespectiveOfParameters(vRecChild.Join)
                                        + "(" + getValueWithReferencingToRelevanceTableMapping(vRecChild.Tablemapping) + ""
                                        + checkOperatorTypeForIsEmptyAndNotIsEmpty(vRecChild.Condition, vRecChild.Value);
                            }
                        }
                        if (vRecChild.Child !== false) {
                            recursiveMethodToFrameChildConditions(vRecChild.Child, _recursiveStep + 1);
                            if (vRecChild.Kind === "group") {
                                _recursiveFramedChildCondition += ")";
                            }
                        } else {
                            if (vRecChild.Kind === "group") {
                                _recursiveFramedChildCondition += ")";
                            }
                            return false;
                        }
                    });
                    return _recursiveFramedChildCondition;
                };
                var _conditionConnectionReference = (vR.hasOwnProperty("Join") ? bindOperatorRespectiveOfParameters(vR.Join) : "");
                if (getFieldTypeRespectiveToTableMapping(vR.Tablemapping) === "checkbox") {
                    relevanceGROUPCondition += _conditionConnectionReference + "(" + " $.inArray(" + checkOperatorTypeForIsEmptyAndNotIsEmptyCheckbox(vR.Value) + ",[" + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + "]) "
                            + bindOperatorRespectiveOfParametersCheckbox(vR.Condition);
                } else {
                    relevanceGROUPCondition += _conditionConnectionReference + "("
                            + getValueWithReferencingToRelevanceTableMapping(vR.Tablemapping) + ""
                            + checkOperatorTypeForIsEmptyAndNotIsEmpty(vR.Condition, vR.Value);
                }
                relevanceGROUPCondition += recursiveMethodToFrameChildConditions(vR.Child, 1);
                relevanceGROUPCondition += ")";
            }
        });
        return relevanceGROUPCondition;
    };
    $.each(finalProductionJson, function (k, v) {
        if (v.Relevance !== "false") {
            $.each(v.Relevance, function (kR, vR) {
                if (vR.Child !== false && vR.Kind === "group") {
                    _pluckChildTableMapping(vR.Child);
                }
                var multipleRelevanceCondition = "";
                if (vR.Tablemapping === currentElementsTablemapping || (_childTableMapping.length > 0 && $.inArray(currentElementsTablemapping, _childTableMapping) >= 0)) {
                    vR.Value = vR.Value.toLowerCase();
                    switch (vR.Kind) {
                        case "condition":
                            multipleRelevanceCondition += makeRelevanceConditionKINDCONDITION(v.Relevance);
                            break;
                        case "group":
                            multipleRelevanceCondition += makeRelevanceConditionKINDGROUP(v.Relevance);
                            break;
                    }
                    if (multipleRelevanceCondition !== "") {
                        console.log(multipleRelevanceCondition/*, eval(multipleRelevanceCondition)*/);
                        if (multipleRelevanceCondition !== undefined && eval(multipleRelevanceCondition) === true) {
                            console.log("APPLY RELEVANCE");
                            $(".container-identification-" + v.SequenceOrder).show();
                        } else {
                            console.log("REMOVE RELEVANCE");
                            $(".container-identification-" + v.SequenceOrder).hide();
                        }
                    }
                }
            });
        }
    });
};
var getDateWithReferencingToRelevanceTableMapping = function (tableMapping) {
    if (tableMapping !== "") {
        var valueToReturn = "";
        $(".tablet-view-track-relevance").each(function (i) {
            if ($(this).attr("data-relevance-tablemapping") === tableMapping) {
                valueToReturn = $(this).val();
            }
        });
        return valueToReturn;
    }
};
var applyCheckAndRetrieveValidation = function (finalProductionJson, currentWorkingElementValue, currentWorkingElementTableMapping, currentWorkingElementObject) {
    var inValidateFieldWithMessage = function ($currentElementObjRef, framedMessage) {
        $currentElementObjRef.addClass("form-invalid-validation-preview-field").val("");
        $currentElementObjRef.after('<span id="helpBlock" class="help-block form-invalid-validation-preview-message">' + framedMessage + '</span>');
    };
    var validateFieldAndRemoveMessage = function ($currentElementObjRef) {
        $currentElementObjRef.removeClass("form-invalid-validation-preview-field");
        $currentElementObjRef.next("span").remove();
    };
    var frameValidationCondition = function (completeValidationObject, currentFocusedElementValue, $currentFocusedElementObject) {
        validateFieldAndRemoveMessage($currentFocusedElementObject);
        var compareWithValue = "";
        var preparedValue = "";
        var conditionForThisInputBox = "";
        var assignOperatorForValidation = function (operator) {
            if (operator === "=") {
                return "==";
            } else {
                return operator;
            }
        };
        var convertDateTimeTo24HourFormat = function (time) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "pm" && hours < 12) {
                hours = hours + 12;
            }
            if (AMPM == "am" && hours == 12) {
                hours = hours - 12;
            }
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) {
                sHours = "0" + sHours;
            }
            if (minutes < 10) {
                sMinutes = "0" + sMinutes;
            }
            return sHours + ':' + sMinutes;
        };
        if (completeValidationObject.ValidationKind !== "") {
            var prepared_twentyFourHourFormatDateTime = "";
            var convertDateToDateTime = function (date, $completeValObj) {
                if (date === "now") {
                    //NOW
                    date = new Date();
                    date = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                    date = new Date().manipulateHours($completeValObj.Value, $completeValObj.Operation);
                    console.log(1);
                } else if (date.indexOf(":") === -1) {
                    date += " 00:00:00";
                    date = new Date(date).manipulateHours($completeValObj.Value, $completeValObj.Operation);
                    console.log(2);
                } else if (moment(date, "HH:mm:ss").format("HH:mm:ss") === date) {
                    if ($completeValObj.Operation === "-") {
                        date = moment(date, "HH:mm:ss").subtract($completeValObj.Value, 'hours').format("HH:mm:ss");
                    } else if ($completeValObj.Operation === "+") {
                        date = moment(date, "HH:mm:ss").add($completeValObj.Value, 'hours').format("HH:mm:ss");
                    }
                    console.log(3);
                } else if (date.indexOf("AM") !== -1 || date.indexOf("PM") !== -1) {
                    var time = date.split(" ");
                    if (time[0].indexOf("-") !== -1) {
                        date = time[0] + " " + convertDateTimeTo24HourFormat(time[1] + " " + time[2].toLowerCase()) + ":" + new Date(date).getSeconds();
                        date = new Date(date).manipulateHours($completeValObj.Value, $completeValObj.Operation);
                        console.log(4);
                    } else {
                        date = convertDateTimeTo24HourFormat(time[0] + " " + time[1].toLowerCase()) + ":00";
                        var preparedTime = date.split(":");
                        var preparedHourTime = preparedTime[0];
                        if (eval(preparedHourTime + $completeValObj.Operation + $completeValObj.Value) < 0 || eval(preparedHourTime + $completeValObj.Operation + $completeValObj.Value) > 24) {
                            date = "INVALID_VALUE";
                        } else {
                            if ($completeValObj.Operation === "-") {
                                date = moment(preparedHourTime + ":" + preparedTime[1] + ":00", "HH:mm:ss").subtract($completeValObj.Value, 'hours').format("HH:mm:ss");
                            } else if ($completeValObj.Operation === "+") {
                                date = moment(preparedHourTime + ":" + preparedTime[1] + ":00", "HH:mm:ss").add($completeValObj.Value, 'hours').format("HH:mm:ss");
                            }
                        }
                    }
                } else {
                    date = new Date(date).manipulateHours($completeValObj.Value, $completeValObj.Operation);
                }
                return date;
            };
            switch (completeValidationObject.ValidationKind) {
                case "TableMapping":
                    var valueToEvaluateWith = getDateWithReferencingToRelevanceTableMapping(completeValidationObject.ValidateWith);
                    prepared_twentyFourHourFormatDateTime = convertDateToDateTime(valueToEvaluateWith, completeValidationObject);
                    break;
                case "Calender":
                    prepared_twentyFourHourFormatDateTime = convertDateToDateTime(completeValidationObject.ValidateWith, completeValidationObject);
                    break;
                case "Now":
                    var requiredFormatOfDate = $currentFocusedElementObject.attr("data-date-type");
                    if (requiredFormatOfDate === "Time") {
                        var dateObj = new Date();
                        var hoursToSet = eval(dateObj.getHours() + completeValidationObject.Operation + completeValidationObject.Value);
                        prepared_twentyFourHourFormatDateTime = hoursToSet + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
                    } else if (requiredFormatOfDate === "Date") {
                        var date = moment().format("YYYY-MM-DD 00:00:00");
                        prepared_twentyFourHourFormatDateTime = new Date(date).manipulateHours(completeValidationObject.Value, completeValidationObject.Operation);
                    } else {
                        prepared_twentyFourHourFormatDateTime = convertDateToDateTime("now", completeValidationObject);
                    }
                    break;
            }
            if (prepared_twentyFourHourFormatDateTime === "INVALID_VALUE") {
                inValidateFieldWithMessage($currentFocusedElementObject, "Invalid Value for time! Please try again.");
                return false;
            } else {
                var requiredFormatOfDate = $currentFocusedElementObject.attr("data-date-type");
                var requiredHourFormatOfDate = parseInt($currentFocusedElementObject.attr("data-time-hourly-format"));
                if (requiredFormatOfDate === "Time") {
                    var convertTimeToDateObject = function (time) {
                        var seperatedTime = time.split(":");
                        var d = new Date();
                        d.setHours(seperatedTime[0]);
                        d.setMinutes(seperatedTime[1]);
                        d.setSeconds(00);
                        return Number(d);
                    };
                    if (requiredHourFormatOfDate === 12) {
                        var convertedValue = convertDateTimeTo24HourFormat(currentFocusedElementValue.toLowerCase()) + ":00";
                        conditionForThisInputBox = convertTimeToDateObject(convertedValue) + assignOperatorForValidation(completeValidationObject.Condition) + convertTimeToDateObject(prepared_twentyFourHourFormatDateTime);
                    } else {
                        conditionForThisInputBox = convertTimeToDateObject(currentFocusedElementValue) + assignOperatorForValidation(completeValidationObject.Condition) + convertTimeToDateObject(prepared_twentyFourHourFormatDateTime);
                    }
                } else if (requiredFormatOfDate === "DateTime") {
                    conditionForThisInputBox = Number(new Date(currentFocusedElementValue)) + assignOperatorForValidation(completeValidationObject.Condition) + Number(prepared_twentyFourHourFormatDateTime);
                } else if (requiredFormatOfDate === "Date") {
                    currentFocusedElementValue = new Date(currentFocusedElementValue + " 00:00:00");
                    conditionForThisInputBox = Number(currentFocusedElementValue) + assignOperatorForValidation(completeValidationObject.Condition) + Number(prepared_twentyFourHourFormatDateTime);
                }
            }
        } else {
            compareWithValue = getValueWithReferencingToRelevanceTableMapping(completeValidationObject.ValidateWith);
            preparedValue = eval(parseInt(compareWithValue) + completeValidationObject.Operation + completeValidationObject.Value);
            conditionForThisInputBox = (currentFocusedElementValue + assignOperatorForValidation(completeValidationObject.Condition) + preparedValue);
        }
        if (!eval(conditionForThisInputBox)) {
            inValidateFieldWithMessage($currentFocusedElementObject, "Invalid Value! Please try again.");
            return false;
        } else {
            validateFieldAndRemoveMessage($currentFocusedElementObject);
        }
    };
    $.each(finalProductionJson, function (k, v) {
        if (v.TableMapping === currentWorkingElementTableMapping && v.Validation !== "false") {
            frameValidationCondition(v.Validation, currentWorkingElementValue, currentWorkingElementObject);
        }
    });
};
var applyCheckValidateAndRemoveCalculation = function (finalProductionJson, $currentWorkingElementObject) {
    var currentElementTablemapping = $currentWorkingElementObject.attr("data-relevance-tablemapping");
    var createConcatenatedText = function (calculationObject) {
        var textOperationValues = {};
        textOperationValues["calculatedValue"] = "";
        $.each(calculationObject, function (k, v) {
            if (v.Constant === "true" && v.Value !== "Space") {
                textOperationValues["calculatedValue"] += v.Value;
            } else if (v.Value === "Space") {
                textOperationValues["calculatedValue"] += " ";
            } else {
                textOperationValues["calculatedValue"] += getValueWithReferencingToRelevanceTableMapping(v.Value).replace(/["']/g, "");
            }
        });
        return textOperationValues;
    };
    var createCalculatedNumericValue = function (calculationObject) {
        var numericOperationValues = {};
        numericOperationValues["calculationCondition"] = "";
        calculationObject[0].Condition = "";
        var verifyAndReturnValueType = function (currentCalculationNode) {
            if (currentCalculationNode.Constant === "false") {
                return getValueWithReferencingToRelevanceTableMapping(currentCalculationNode.Value)/*.replace(/[']/g, "0")*/;
            } else if (currentCalculationNode.Constant === "true") {
                return currentCalculationNode.Value;
            }
        };
        var recursiveMethodToFrameNumericCondition = function (childNumericObject) {
            var framedNumericCondition = "";
            $.each(childNumericObject, function (k, v) {
                if (v.Kind === "condition") {
                    framedNumericCondition += v.Condition + verifyAndReturnValueType(v);
                } else if (v.Kind === "group") {
                    framedNumericCondition += v.Condition;
                    framedNumericCondition += " ( ";
                    framedNumericCondition += verifyAndReturnValueType(v);
                }

                if (v.Child !== "false") {
                    framedNumericCondition += recursiveMethodToFrameNumericCondition(v.Child);
                    framedNumericCondition += " ) ";
                } else {
                    if (v.Kind === "group") {
                        framedNumericCondition += " ) ";
                    }
                    return false;
                }
            });
            return framedNumericCondition;
        };
        $.each(calculationObject, function (k, v) {
            if (v.Kind === "condition") {
                numericOperationValues["calculationCondition"] += v.Condition + verifyAndReturnValueType(v);
            } else if (v.Kind === "group") {
                numericOperationValues["calculationCondition"] += v.Condition + " ( ";
                numericOperationValues["calculationCondition"] += verifyAndReturnValueType(v);
                if (v.Child !== "false") {
                    numericOperationValues["calculationCondition"] += recursiveMethodToFrameNumericCondition(v.Child);
                }
                numericOperationValues["calculationCondition"] += " ) ";
            }
        });
        return numericOperationValues;
    };
    $.each(finalProductionJson, function (k, v) {
        if (v.Type === "inputCalculate") {
            $.each(v.Calculation, function (kC, vC) {
                var $currentInputFieldReference = $(".container-identification-" + k).find(".tablet-view-calculation-field");
                var operationValues = {};
                switch (v.Kind) {
                    case "DateTime":
                        operationValues["calculatedValue"] = 0;
                        operationValues["leftHandValue"] = checkDateTimeFormatAndReturnInteger(getDateWithReferencingToRelevanceTableMapping(vC.Tablemapping).toLowerCase());
                        operationValues["operandOperator"] = vC.Condition;
                        if (vC.Constant === "true") {
                            operationValues["rightHandValue"] = vC.Value;
                        } else {
                            var tableMappedValue = getDateWithReferencingToRelevanceTableMapping(vC.Value);
                            operationValues["rightHandValue"] = checkForNumericDataType(tableMappedValue) ? parseInt(tableMappedValue) : checkDateTimeFormatAndReturnInteger(tableMappedValue.toLowerCase());
                        }
                        if (operationValues["operandOperator"] === "+") {
                            operationValues["calculatedValue"] = new Date(operationValues["leftHandValue"]).manipulateHours(operationValues["rightHandValue"], operationValues["operandOperator"]);
                            operationValues["calculatedValue"] = moment(operationValues["calculatedValue"]).format("YYYY-MM-DD hh:mm:ss A");
                        } else if (operationValues["operandOperator"] === "-") {
                            if (checkForNumericDataType(operationValues["rightHandValue"])) {
                                operationValues["calculatedValue"] = new Date(operationValues["leftHandValue"]).manipulateHours(operationValues["rightHandValue"], operationValues["operandOperator"]);
                                operationValues["calculatedValue"] = moment(operationValues["calculatedValue"]).format("YYYY-MM-DD hh:mm:ss A");
                            } else {
                                var leftHandValue = moment(operationValues["leftHandValue"]);
                                var rightHandValue = moment(operationValues["rightHandValue"]);
                                var duration = moment.duration(rightHandValue.diff(leftHandValue));
                                operationValues["calculatedValue"] = duration.asHours();
                            }
                        }
                        break;
                    case "Text":
                        operationValues["calculatedValue"] = createConcatenatedText(v.Calculation).calculatedValue;
                        break;
                    case "Numeric":
                        var numericCalculationOutcome = createCalculatedNumericValue(v.Calculation).calculationCondition;
                        operationValues["calculatedValueCondition"] = numericCalculationOutcome;
                        operationValues["calculatedValue"] = eval(numericCalculationOutcome);
                        break;
                }
                $currentInputFieldReference.val(operationValues["calculatedValue"]);
            });
        }
    });
};
var checkForNumericDataType = function (obj) {
    var reg = new RegExp('^[0-9]+$');
    if (reg.test(obj)) {
        return true;
    } else if (parseFloat(obj) == obj) {
        return true;
    } else {
        return false;
    }
};
function showPreviewLoader() {
    $(".loader-icon-container").show();
    $(".mobile-main-body").css({"opacity": 0.6});
}
function hidePreviewLoader() {
    $(".loader-icon-container").hide();
    $(".mobile-main-body").css({"opacity": 1});
}
function initiliazeTabletModal() {
    $(".mobile-main-body").css({"opacity": 0.6});
    $(".loader-icon-container").show();
    setTimeout(function () {
        $(".loader-icon-container").hide();
        $(".mobile-main-body").css({"opacity": 1});
    }, 1500);
}
function filePreview(input) {
    showPreviewLoader();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            hidePreviewLoader();
            var mediaTagToUse = "embed";
            var uploaderButtonType = input.id;
            var uploaderButtonType = uploaderButtonType.split("-");
            var previewContainer = $(input).next();
            var embedTagStyling = "";
            var supportedFormats = (input.files[0].type).split("/");
            var allowableExtensions = [];
            var allowableFileTypes = [];
            allowableFileTypes = ["image", "audio", "video"];
            allowableExtensions["image"] = ["png", "jpeg"];
            allowableExtensions["audio"] = ["mp3"];
            allowableExtensions["video"] = ["mp4"];
            var uploadValidateResponse = true;
            if ($.inArray(supportedFormats[0], allowableFileTypes) === -1 || supportedFormats[0] !== uploaderButtonType[0]) {
                uploadValidateResponse = false;
            } else if ($.inArray(supportedFormats[1], allowableExtensions[uploaderButtonType[0]]) === -1) {
                uploadValidateResponse = false;
            }
            switch (uploaderButtonType[0]) {
                case "image":
                    embedTagStyling = "style='width: 200px;height: 150px;'";
                    mediaTagToUse = "img";
                    break;
                case "audio":
                    embedTagStyling = "style='height: 60px;'";
                    mediaTagToUse = "audio";
                    break;
                case "video":
                    embedTagStyling = "style='width: 271px;'";
                    mediaTagToUse = "video";
                    break;
            }
            if (uploadValidateResponse === true) {
                switch (mediaTagToUse) {
                    case "img":
                        $(previewContainer).html("<img src='" + e.target.result + "' " + embedTagStyling + ">").show();
                        break;
                    case "audio":
                        $(previewContainer).html("<audio controls><source src=" + e.target.result + " type='audio/mp3'></source></audio>").show();
                        break;
                    case "video":
                        $(previewContainer).html("<video controls><source src=" + e.target.result + " type='video/mp4'></source></video>").show();
                        break;
                    default:
                        $(previewContainer).html("<embed src='" + e.target.result + "' " + embedTagStyling + ">").show();
                        break;
                }
            } else {
                alert("INVALID FILE");
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function resetPreviewFieldsDisplayForm() {
    $(".tablet-view-form").html("");
}
function renderPreviewFields(jsonInput) {
    resetPreviewFieldsDisplayForm();
    var objCounter = 0;
    $.each(jsonInput, function (k, v) {
        if (!$.isEmptyObject(v)) {
            $(".tablet-view-form").append(bindDynamicValueWithStaticHtmlTemplate(v.Type, v, (objCounter++)));
        }
    });
}
function validateFinalProductJSON(jsonObject) {
    var isInValidJSON = false;
    if ($.isEmptyObject(jsonObject) === false) {
        $.each(jsonObject, function (k, v) {
            if (v.Label === "" || v.TableMapping === "") {
                isInValidJSON = true;
                return false;
            }
            if (v.Relevance !== "false") {
                $.each(v.Relevance, function (kR, vR) {
                    if (vR.Value === "") {
                        if (vR.Condition !== "is_empty" && vR.Condition !== "not_is_empty") {
                            isInValidJSON = true;
                            return false;
                        }
                    }
                });
                if (isInValidJSON) {
                    return false;
                }
            }
            if (v.Type === "inputNumeric") {
                if (v.Kind === "Decimal") {
                    v.DefaultValue = parseFloat(v.DefaultValue);
                } else if (v.Kind === "Integer") {
                    v.DefaultValue = parseInt(v.DefaultValue);
                }
            }
        });
    } else {
        isInValidJSON = true;
    }
    return isInValidJSON;
}
function assignDefaultSelectedToYesAndNo(checkedType, $currentObjectReference) {
    if (checkedType !== "") {
        $currentObjectReference.parent().parent().find("input[type='radio']").parent().css({"background-color": "#656364"});
        $currentObjectReference.parent().parent().find("input[type='radio']").each(function (index) {
            if ($(this).attr("data-is-checked") === checkedType) {
                var $colorCode = "";
                if ($(this).val() === "Yes") {
                    $colorCode = "#2d9b05";
                } else if ($(this).val() === "No") {
                    $colorCode = "#e10f03";
                } else {
                    $colorCode = "#f7920d";
                }
                $(this).parent().css({"background-color": $colorCode});
            }
        });
    }
}
function validateNumericRange(currentObjRef, minRange, maxRange) {
    var currentInputValue = parseInt($(currentObjRef).val());
    $(currentObjRef).parent().find(".tablet-view-error-input-field").hide();
    if (currentInputValue < minRange) {
        $(currentObjRef).parent().find(".tablet-view-error-input-field").show();
        currentObjRef.value = '';
    } else if (currentInputValue > maxRange) {
        $(currentObjRef).parent().find(".tablet-view-error-input-field").show();
        currentObjRef.value = '';
    }
}
function validateTextRange(currentObjRef, minRange, maxRange) {
    var currentInputValue = parseInt($(currentObjRef).val().length);
    $(currentObjRef).parent().find(".tablet-view-error-input-field").hide();
    if (currentInputValue < minRange) {
        $(currentObjRef).parent().find(".tablet-view-error-input-field").show();
        currentObjRef.value = '';
    } else if (currentInputValue > maxRange) {
        $(currentObjRef).parent().find(".tablet-view-error-input-field").show();
        currentObjRef.value = '';
    }
}
function bindInputForNumericEntryOnly(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
var preApplyRelevance = function (jsonObject) {
    $(".tablet-view-track-relevance").each(function (i) {
        if ($(this).val() !== "") {
            isQuestionPippingApplicableHtmlHelper($(this));
        }
        applyCheckValidateAndRemoveRelevance(jsonObject, $(this));
    });
};
var preApplyYesAndNo = function () {
    $("input[id^='yesNoDontKnow']").each(function (index) {
        assignDefaultSelectedToYesAndNo($(this).attr("data-is-checked"), $(this));
    });
};
$(document).ready(function () {
    $("body").on("click", ".display-preview-button", function () {
        initiliazeTabletModal();
        var previewErrorMessage = "It looks like you have errors in the form which can cause preview not to look like expected, please fix them and try !";
        var _arrangePreviewFieldsAndShowModal = function (jsonObject) {
            var array = [];
            $.each(jsonObject, function (key, value) {
                array.push(value);
            });
            array.sort(function (a, b) {
                return a.SequenceOrder - b.SequenceOrder;
            });
            jsonObject = array;
            renderPreviewFields(jsonObject);
            preApplyYesAndNo();
            preApplyRelevance(jsonObject);
            $("#display-celform-preview").modal('show');
        };
        if (validateFinalProductJSON(finalJsonForPreviewFunctionality)) {
            if (confirm(previewErrorMessage)) {
                _arrangePreviewFieldsAndShowModal(finalJsonForPreviewFunctionality);
            }
        } else {
            _arrangePreviewFieldsAndShowModal(finalJsonForPreviewFunctionality);
        }
    });
    $("body").on("focus", ".tablet-view-datetimepicker", function () {
        var dateTypeKind = $(this).attr("data-date-type");
        var dateTimeHourlyFormat = parseInt($(this).attr("data-time-hourly-format"));
        var dateAndTimeMinMaxRange = [];
        var concatenatedDateRange = $(this).attr("data-date-range-startdate-enddate");
        if (concatenatedDateRange) {
            dateAndTimeMinMaxRange = ($(this).attr("data-date-range-startdate-enddate")).split("__");
        }
        switch (dateTypeKind) {
            case "Date":
                $(this).datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    container: '#display-celform-preview',
                    startDate: dateAndTimeMinMaxRange[0],
                    endDate: dateAndTimeMinMaxRange[1],
                    todayBtn: "linked"
                });
                break;
            case "Time":
                if (dateTimeHourlyFormat === 24) {
                    if (dateAndTimeMinMaxRange[0] && dateAndTimeMinMaxRange[1]) {
                        dateAndTimeMinMaxRange[0] = moment(dateAndTimeMinMaxRange[0], 'hh:mm:ss a').format('HH:mm:ss');
                        dateAndTimeMinMaxRange[1] = moment(dateAndTimeMinMaxRange[1], 'hh:mm:ss a').format('HH:mm:ss');
                    }
                    $(this).datetimepicker({
                        format: 'hh:ii:ss',
                        autoclose: true,
                        startView: 1,
                        container: '#display-celform-preview',
                        startDate: dateAndTimeMinMaxRange[0],
                        endDate: dateAndTimeMinMaxRange[1],
                        pickerPosition: 'top-right',
                        todayBtn: true
                    });
                } else {
                    $(this).datetimepicker({
                        format: 'HH:ii:ss P',
                        autoclose: true,
                        startView: 1,
                        showMeridian: true,
                        container: '#display-celform-preview',
                        startDate: dateAndTimeMinMaxRange[0],
                        endDate: dateAndTimeMinMaxRange[1],
                        pickerPosition: 'top-right',
                        todayBtn: true
                    });
                }
                break;
            case "DateTime":
                if (dateTimeHourlyFormat === 24) {
                    if (dateAndTimeMinMaxRange[0] && dateAndTimeMinMaxRange[1]) {
                        dateAndTimeMinMaxRange[0] = moment(dateAndTimeMinMaxRange[0], 'YYYY-MM-DD hh:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
                        dateAndTimeMinMaxRange[1] = moment(dateAndTimeMinMaxRange[1], 'YYYY-MM-DD hh:mm:ss a').format('YYYY-MM-DD HH:mm:ss');
                    }
                    $(this).datetimepicker({
                        format: 'yyyy-mm-dd hh:ii:ss',
                        autoclose: true,
                        container: '#display-celform-preview',
                        startDate: dateAndTimeMinMaxRange[0],
                        endDate: dateAndTimeMinMaxRange[1],
                        pickerPosition: 'top-right',
                        todayBtn: true
                    });
                } else {
                    $(this).datetimepicker({
                        format: 'yyyy-mm-dd HH:ii:ss P',
                        autoclose: true,
                        showMeridian: true,
                        container: '#display-celform-preview',
                        startDate: dateAndTimeMinMaxRange[0],
                        endDate: dateAndTimeMinMaxRange[1],
                        pickerPosition: 'top-right',
                        todayBtn: true
                    });
                }
                break;
        }
        $(this).on("changeDate", function (e) {
            var selectedDate = $(this).val();
            if (selectedDate.indexOf(":") !== -1) {
                var splittedDate = selectedDate.split(" ");
                $.each(splittedDate, function (k, v) {
                    if (v.indexOf(":") !== -1) {
                        var splittedTime = v.split(":");
                        splittedTime[2] = "00";
                        splittedTime = splittedTime.join(":");
                        splittedDate[k] = splittedTime;
                        return false;
                    }
                });
                splittedDate = splittedDate.join(" ");
                $(this).val(splittedDate);
                $(this).datetimepicker('update');
            }
            $(this).focusout();
            $(this).blur();
        });
    });
    $("body").on("click", ".tablet-view-location-use-autofeature", function () {
        navigator.geolocation.getCurrentPosition(function (position) {
            $(".split-1 input").val(position.coords.latitude);
            $(".split-2 input").val(position.coords.longitude);
            $(this).val(position.coords.latitude + "-" + position.coords.longitude);
        }, function (error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    alert("User denied the request for geolocation.");
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("Location information is unavailable.");
                    break;
                case error.TIMEOUT:
                    alert("The request to get user location timed out.");
                    break;
                case error.UNKNOWN_ERROR:
                    alert("An unknown error occurred.");
                    break;
            }
        });
    });
    $("body").on("change", ".media-file-upload", function () {
        filePreview(this);
    });
    $("body").on("click", "input[id^='yesNoDontKnow']", function () {
        var $colorCode = "";
        $(this).parent().parent().find("input[type='radio']").parent().css({"background-color": "#656364"});
        if ($(this).val() === "Yes") {
            $colorCode = "#2d9b05";
        } else if ($(this).val() === "No") {
            $colorCode = "#e10f03";
        } else {
            $colorCode = "#f7920d";
        }
        $(this).parent().css({"background-color": $colorCode});
    });
    $("body").on("blur click", ".tablet-view-track-relevance", function (e) {
        if ($(this).val() === "") {
            $(this).parent().find(".tablet-view-error-required-input-field").show();
        } else {
            $(this).parent().find(".tablet-view-error-required-input-field").hide();
        }
        isQuestionPippingApplicableHtmlHelper($(this));
        applyCheckValidateAndRemoveRelevance(finalJsonForPreviewFunctionality, $(this));
        applyCheckValidateAndRemoveCalculation(finalJsonForPreviewFunctionality, $(this));
    });
    $("body").on("blur", ".tablet-view-number-type", function () {
        var numberType = $(this).attr("data-number-type");
        var currentValue = $(this).val();
        if (numberType === "Decimal" && currentValue !== "") {
            if (currentValue.indexOf(".") === -1) {
                currentValue += ".00";
            }
        } else if (numberType === "Integer" && currentValue !== "") {
            if (currentValue.indexOf(".") !== -1) {
                currentValue = currentValue.slice(0, currentValue.indexOf("."));
            }
        }
        $(this).val(currentValue);
    });
    $("body").on("blur", ".tablet-view-number-validation, .tablet-view-date-validation", function () {
        if ($(this).val()) {
            applyCheckAndRetrieveValidation(finalJsonForPreviewFunctionality, $(this).val(), $(this).attr("data-relevance-tablemapping"), $(this));
        }
    });
});