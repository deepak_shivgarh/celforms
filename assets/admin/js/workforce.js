function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
var setTableForPersonDetailView = function (completePersonObject, scenarioOfPage) {
    var v = completePersonObject;
    var htmlTableData = "";
    var htmlTableData = '<table class="table table-bordered">';
    htmlTableData += '<tr>\n\
     <th>Saksham ID : </th>\n\
     <td>' + v.PersonCelId + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Aadhaar No : </th>\n\
     <td></td>\n\
     </tr>\n\
     <tr>\n\
     <th>First Name : </th>\n\
     <td>' + v.PersonFirstName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Mid Name : </th>\n\
     <td>' + v.PersonMidName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Last Name : </th>\n\
     <td>' + v.PersonLastName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Father\'s First Name : </th>\n\
     <td>' + v.PersonFatherFirstName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Father\'s Mid Name : </th>\n\
     <td>' + v.PersonFatherMidName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Father\'s Last Name : </th>\n\
     <td>' + v.PersonFatherLastName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Date of Birth : </th>\n\
     <td>' + v.PersonDateBirth + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Age</th>\n\
     <td>' + getAge(completePersonObject.PersonDateBirth) + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Head of the family First Name : </th>\n\
     <td>' + v.PersonAttributes.HeadOfHouseholdFirstName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Head of the family Mid Name : </th>\n\
     <td>' + v.PersonAttributes.HeadOfHouseholdMidName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Head of the family Last Name :</th>\n\
     <td>' + v.PersonAttributes.HeadOfHouseholdLastName + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Head of Family Pet Name : </th>\n\
     <td></td>\n\
     </tr>\n\
     <tr>\n\
     <th>Birth Place : </th>\n\
     <td>' + v.PersonPlaceBirth + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Gender : </th>\n\
     <td>' + v.PersonGender + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Religion : </th>\n\
     <td>' + v.PersonAttributes.PersonReligion + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Caste : </th>\n\
     <td>' + v.PersonAttributes.PersonCaste + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Mobile No : </th>\n\
     <td>' + v.PersonAttributes.PersonPhone + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Contact Person : </th>\n\
     <td></td>\n\
     </tr>\n\
     <tr>\n\
     <th>Other Mobile Number : </th>\n\
     <td></td>\n\
     </tr>\n\
     <tr>\n\
     <th>Block : </th>\n\
     <td></td>\n\
     </tr>';
    if (!scenarioOfPage) {
        htmlTableData += '<tr><th>Gram Sabha : </th>\n\
     <td>' + v.GramSabha + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Majra : </th>\n\
     <td></td>\n\
     </tr>\n\
     <tr>\n\
     <th>Place Landmark : </th>\n\
     <td>' + v.Poi.PoiAttributes.PlaceLandmark + '</td>\n\
     </tr>\n\
     <tr>\n\
     <th>Social Landmark : </th>\n\
     <td>' + v.Poi.PoiAttributes.SocialLandmark + '</td>\n\
     </tr>';
    }
    htmlTableData += "</table>";
    return htmlTableData;
};
$(document).ready(function () {
    var requiredMessage = '<p class="invalid-field-message">Kindly fill this required field.</p>';
    var warningMessage = '<p class="warning-field-message">For accurate results we recommend to fill this field.</p>';
    $("body").on("click", ".person-search-form-reset-button", function () {
        $(".person-search-form").find(":input").each(function () {
            $(this).val("");
        });
    });
    $("body").on("click", ".view-person-detail-job", function () {
        var enrolmentInstanceId = $(this).attr("data-job-enrolment-instance");
        showLoader();
        $.ajax({
            url: base_url + "projects/getEnrolmentUsersByInstanceId",
            type: "POST",
            dataType: "json",
            data: {
                enrolmentInstanceId: enrolmentInstanceId
            },
            success: function (r) {
                var personArray = r[0].Person;
                $(".dynamic-table-person-data").html(setTableForPersonDetailView(personArray, 'enrolmentGetJob'));
                $("#person-display-modal").modal('show');
                hideLoader();
            }
        });
    });
    $("body").on("click", ".view-person-detail", function () {
        var personEntityId = $(this).attr("data-person-entity-id");
        var requestJSON = {};
        requestJSON["GetPOI"] = "true";
        requestJSON["PersonEntityId"] = personEntityId;
        requestJSON = JSON.stringify(requestJSON);
        showLoader();
        $.ajax({
            url: base_url + "workforce/getPersonAllDetails",
            type: "POST",
            data: {
                requestJson: requestJSON
            },
            success: function (r) {
                var personMatchedRecord = JSON.parse(r);
                $(".dynamic-table-person-data").html(setTableForPersonDetailView(personMatchedRecord, ''));
                $("#person-display-modal").modal('show');
                hideLoader();
            }
        });
    });
    $("body").on("click", ".person-search-form-button", function () {
        var validFlag = true;
        var promptFlag = false;
        $(".person-search-form").find(":input").each(function () {
            $(this).removeClass("invalid-field").next(".invalid-field-message").remove();
            $(this).removeClass("warning-field").next(".warning-field-message").remove();
            if ($(this).hasClass("validate-required") && !$(this).val()) {
                $(this).addClass("invalid-field").focus().after(requiredMessage);
                validFlag = false;
                return false;
            } else if ($(this).hasClass("validate-warning") && !$(this).val()) {
                promptFlag = true;
                $(this).addClass("warning-field").after(warningMessage);
            }
        });
        if (validFlag) {
            if (promptFlag) {
                if (confirm("Are you sure you want to submit the form without recommended fields?")) {
                    $(".person-search-form").submit();
                }
            } else {
                $(".person-search-form").submit();
            }
        }
    });
    $("body").on("blur", ".person-search-form-field", function () {
        if ($(this).val()) {
            $(this).removeClass("invalid-field").next(".invalid-field-message").remove();
        }
    });
    $("body").on("click", ".add-workforce-button", function () {
        showLoader();
        var $currentObjectRef = $(this);
        $.ajax({
            url: base_url + "workforce/workforceRoleGet",
            type: "POST",
            dataType: "json",
            success: function (r) {
                hideLoader();
                var roles = "";
                var status = "";
                $.each(r.roles, function (k, v) {
                    roles += "<option value='" + v.WorkforceRoleEntityId + "'>" + v.WorkforceRoleName + "</option>";
                });
                $.each(r.status, function (k, v) {
                    if (v.StatusName === 'ACTIVE' || v.StatusName === 'INACTIVE') {
                        status += "<option value='" + v.StatusEntityId + "'>" + v.StatusName + "</option>";
                    }
                });
                $(".role-dropdown").html(roles);
                $(".status-dropdown").html(status);
                $(".person-entity-id").val($currentObjectRef.attr("data-person-entity-id"));
                $("#add-job-master-form").modal('show');
            },
            error: function (r) {}
        });
    });
    $("body").on("click", ".add-workforce-form-json", function () {
        var finalJson = {};
        var validationFlag = true;
        var workForceAddJson = {
            "WorkforceOperatorId": "",
            "Password": "",
            "PersonEntityId": "",
            "WorkforceRoleEntityId": "", 
            "WorkforceAttributes": [{
                    "EmailAddress": "",
                    "MobileNumber": ""
                }],
            "GetPerson": "true",
            "Token": ""
        };
        $("#add-workforce-form").find(":input").each(function () {
            $(this).removeClass("invalid-field");
            if ($(this).hasClass("is-required") && $(this).val() === "") {
                $(this).addClass("invalid-field").focus();
                validationFlag = false;
                return false;
            }
        });
        if (validationFlag) {
            $("#add-workforce-form").find(":input").each(function () {
                if ($(this).attr("data-object-key")) {
                    if ($(this).hasClass("workforce-attributes-object")) {
                        workForceAddJson.WorkforceAttributes[0][$(this).attr("data-object-key")] = $(this).val();
                    } else if ($(this).hasClass("workforce-password")) {
                        workForceAddJson[$(this).attr("data-object-key")] = md5($(this).val());
                    } else {
                        workForceAddJson[$(this).attr("data-object-key")] = $(this).val();
                    }
                }
            });
            workForceAddJson = JSON.stringify(workForceAddJson);
            console.log(workForceAddJson); 
            showLoader();
            $.post(base_url + 'ajax/workforceAdd', {requestJson: workForceAddJson}).done(function (data) {
                console.log(data);
                window.location.href = base_url + 'workforce';
            });
        }
    });
    $("body").on("click", ".assigned-job-button", function () {
        var jobEntityIdsGetAssign = [];
        $(".assign-job-checkbox").each(function () {
            if ($(this).is(":checked")) {
                jobEntityIdsGetAssign.push($(this).val());
            }
        });
        console.log(jobEntityIdsGetAssign);
        $("#list-all-workforcess-modal").modal('show');
    });
});