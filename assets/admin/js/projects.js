var projectAddJson = {
    "ProjectEntityId": "",
    "ProjectName": "",
    "StatusStatusEntityId": "",
    "ProjectAttributes": {
        "AllowableProperties": [],
        "EligibilityCriteria": [],
        "ClosureCriteria": [], 
    },
    "Token": ""
};
var validateBlockOfCondition = function (blockId) {
    var isInvalid = false;
    var showContentMessage = function (scenarioOfMessage) {
        if (scenarioOfMessage === undefined) {
            return 'select';
        } else {
            return 'fill';
        }
    };
    $("#" + blockId).find(":input").each(function () {
        if ($(this).hasClass("form-field-invalid")) {
            $(this).removeClass("form-field-invalid");
            $(this).next('.form-field-invalid-message').remove();
        }
        if ($.trim(this.value) === '' && this.id !== 'target-project-input-field') {
            $(this).addClass("form-field-invalid");
            $(this).after('<span id="helpBlock" class="help-block form-field-invalid-message">Kindly ' + showContentMessage($(this).attr("type")) + ' the required field.</span>');
            this.focus();
            isInvalid = true;
            return false;
        }
    });
    if (isInvalid) {
        return false;
    } else {
        return true;
    }
};
function resetAllValueInputFields(currentWorkingDiv) {
    $(currentWorkingDiv).find(".date-sub-type-input").remove();
    $(currentWorkingDiv).find(".number-sub-type-input").remove();
    $(currentWorkingDiv).find(".date-type-input").remove();
    $(currentWorkingDiv).find(".dynamic-condition-selector").remove();
    $(currentWorkingDiv).find(".others-input").remove();
    if ($(currentWorkingDiv).find(".value-condition-based-input").is(":hidden")) {
        $(currentWorkingDiv).find(".value-condition-based-input").children().children().val("");
        $(currentWorkingDiv).find(".value-condition-based-input").show();
    }
}
function renderDateInputs(currentDivContainer, datePreFilledType) {
    var isSelected = [];
    var filteredPreFilledData = "";
    isSelected["number"] = "";
    isSelected["date"] = "";
    var extractDateFromText = function (str) {
        var ret = "";
        if (/'/.test(str)) {
            ret = str.match(/'(.*?)'/)[1];
        } else {
            ret = str;
        }
        return ret;
    }
    if (datePreFilledType != "") {
        filteredPreFilledData = extractDateFromText(datePreFilledType);
        var filteredPreFilledDataFormat = Date.parse(filteredPreFilledData);
        if (isNaN(filteredPreFilledDataFormat)) {
            isSelected["number"] = "selected='selected'";
        } else {
            isSelected["date"] = "selected='selected'";
        }
    }
    var currentDiv = "#" + currentDivContainer;
    var templateOfDateInputs = '<div class="form-group date-type-input">\n\
                                        <label for="value" class="col-sm-2 control-label">Date/Number</label>\n\
                                        <div class="col-sm-10">\n\
                                            <select class="form-control date-type-input-field" data-date-pre-selected="' + filteredPreFilledData + '">\n\
                                                <option value="">--SELECT--</option>\n\
                                                <option value="date" ' + isSelected["date"] + '>Date</option>\n\
                                                <option value="number" ' + isSelected["number"] + '>Number</option>\n\
                                            </select>\n\
                                        </div>\n\
                                    </div>';
    $(currentDiv).find(".value-condition-based-input").hide();
    $(currentDiv).find(".value-condition-based-input").children().children().val("this value to be removed");
    $(currentDiv).find(".date-sub-type-input").remove();
    $(currentDiv).find(".number-sub-type-input").remove();
    $(currentDiv).find(".value-type-input").after(templateOfDateInputs);
    if (datePreFilledType != "") {
        $(currentDiv).find(".date-type-input").children().children().change();
    }

}
 
/* HTML INJECTION IN THE FORM ON BASIS OF VALUE TYPE USING FUNCTION [ENDS HERE] */

function getProperties(tableName, divLevel) {
    if (tableName != "") {
        showLoader();
        $.post(base_url + 'ajax', {tableName: tableName, dataSelected: $('#' + divLevel).children().children().children('.tableName').attr('data-selected')})
                .done(function (data) { 
                    hideLoader();
                    $('#' + divLevel).children().children().children('.Condition').html(data);
                    $('#' + divLevel).children().children().children('.Condition').prepend('<option value="">--SELECT--</option>');
                    $('#' + divLevel).children().children().children('.Condition').change();
                });
    }
}

function getDataType(dataTypeString, dataSelected, divLevel, dataPreFilledForJsonAttribute, dateTypePreFilledForDateInput) {
    var dataType = dataTypeString.substring(0, dataTypeString.lastIndexOf("("));
    var midVal = '';
    if ($.trim(dataType) == '') {
        dataType = dataTypeString;
    } else {
        midVal = dataTypeString.substring(dataTypeString.lastIndexOf("(") + 1);
        midVal = midVal.substring(0, midVal.lastIndexOf(")"));
    }
    resetAllValueInputFields('#' + divLevel); 
    switch (dataType) {
        case 'int':
        case 'bigInt':
        case 'number':
        case 'binary':
            $('#' + divLevel).children().children('.dtype').html('<input type="text" class="form-control" name="' + dataSelected + '" id="value" value="' + dataSelected + '"  data-key="Value" is-object="true">');
            break;
        case 'datetime':
            renderDateInputs(divLevel, dateTypePreFilledForDateInput);
             break;
        case 'enum':
            var myarray = midVal.split(',');
            var opt = '';
            var selected = '';
            for (var i = 0; i < myarray.length; i++) {
                selected = (dataSelected == myarray[i].slice(1, -1) ? 'selected="selected"' : '');
                opt += '<option value="' + myarray[i].slice(1, -1) + '" ' + selected + '>' + myarray[i].slice(1, -1) + '</option>';
            }
            $('#' + divLevel).children().children('.dtype').html('<select class="form-control" id="value" data-key="Value" is-object="true">' + opt + '</select>');
            break;
        case 'json':
            /* DYNAMIC TABLE MAPPING FORMATION USING JSON KEYS OF RESPECTIVE TABLES */
            var tableName = $("#" + divLevel).children().find(".tableName").val();
            /* VARIABLE CALLBACK FOR TEMPLATING THE ATTRIBUTE SELECTOR */
            var newAttributeInputField = function (propertyKeyResponses) {
                return '<div class="form-group dynamic-condition-selector">\n\
                                        <label for="value" class="col-sm-2 control-label">Select Attribute</label>\n\
                                        <div class="col-sm-10 dtype" >\n\
                                            <select class="form-control dynamic-condition-attribute-selector"><option value="">--SELECT--</option>' + propertyKeyResponses + '</select>\n\
                                        </div>\n\
                                    </div>'
            };
            /* AJAX REQUEST BASED ON CONDITION AND REQUEST PARAMETER NAMED AS "tableName" */
            $.ajax({
                beforeSend: function () {
                    showLoader();
                },
                complete: function () {
                    hideLoader();
                },
                url: base_url + 'ajax/getPropertiesJsonAddProject',
                type: "post",
                data: "tableName=" + tableName + "&dataPreSelected=" + dataPreFilledForJsonAttribute,
                success: function (r) {
                    $("#" + divLevel).find(".condition-group-input").after(newAttributeInputField(r));
                    $("#" + divLevel).find(".condition-group-input").next().find(".dynamic-condition-attribute-selector").change();
                },
                error: function () {
                    console.log("ERROR OCCURED");
                }
            });
            $('#' + divLevel).children().children('.dtype').html('<input type="text" class="form-control" name="" id="value" value="' + dataSelected + '" data-key="Value" is-object="true">');
            break;
        default:
            $('#' + divLevel).children().children('.dtype').html('<input type="text" class="form-control" name="" id="value" value="' + dataSelected + '" data-key="Value" is-object="true">');
            break;
    }
}

$(window).load(function () {
    $('.tableName').change();
    checkRemoveCondition();
});
$('body').on('change', '.tableName', function () {
    var divLevel = $(this).parent().parent().parent().attr('id');
    getProperties($(this).val(), divLevel);
});
$('body').on('change', '.Condition', function () {
    var divLevel = $(this).parent().parent().parent().attr('id');
    var dataTypeString = $('option:selected', this).attr('type');
    getDataType(dataTypeString, $(this).attr('data-selected'),
            divLevel,
            ($(this).attr('data-prefilled') != undefined ? $(this).attr('data-prefilled') : ""),
            ($(this).attr('data-date-type-selected') != undefined ? $(this).attr('data-date-type-selected') : ""));
 });
$('body').on('click', '.validate', function (e) {
    var containerDiv = $(this).attr('container');
    var ID = $(this).attr('id');
    var inputData = '';
    var flag = true;
    var showContentMessage = function (scenarioOfMessage) {
        if (scenarioOfMessage === undefined) {
            return 'select';
        } else {
            return 'fill';
        }
    }; 
    $('#' + containerDiv).find(':input').each(function (i, elem) {
        if ($(this).hasClass("form-field-invalid")) {
            $(this).removeClass("form-field-invalid");
            $(this).next('.form-field-invalid-message').remove();
        }
        if ($.trim(this.value) == '' && this.id != 'target-project-input-field') {
            $(this).addClass("form-field-invalid");
            $(this).after('<span id="helpBlock" class="help-block form-field-invalid-message">Kindly ' + showContentMessage($(this).attr("type")) + ' the required field.</span>');
            this.focus();
            flag = false;
            return false;
        }
    });
    if (ID == 'validate-project-initials' || ID == 'validate-project-initials-update') { 
        var projectName = $(".project-name").val();
        var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`=";
        var checkForSpecialCharacter = function (string) {
            for (i = 0; i < specialChars.length; i++) {
                if (string.indexOf(specialChars[i]) > -1) {
                    return true;
                }
            }
            return false;
        };
        if (ID != 'validate-project-initials-update' && $.inArray(projectName, allowedProjectNames) >= 0) {
            $(".project-name").addClass("form-field-invalid");
            $(".project-name").after('<span id="helpBlock" class="help-block form-field-invalid-message">This project already exists.</span>');
            $(".project-name").val("").focus();
            flag = false;
            return false;
        } else if (checkForSpecialCharacter(projectName)) {
            $(".project-name").addClass("form-field-invalid");
            $(".project-name").after('<span id="helpBlock" class="help-block form-field-invalid-message">Only characters and numeric values are acceptable.</span>');
            $(".project-name").val("").focus();
            flag = false;
            return false;
        }
    }
    if (flag == true) {
        $('.' + containerDiv).click();
        $('.wizardinfo li').removeClass('active');
        $('.' + $(this).attr('data-id')).addClass('active');
        // GET JSON OF ANY FORM IN KEY : VALUE PAIR
        var formId = '#' + $('#' + containerDiv).children('form').attr('id');
        /* node where newJson object will be apend or add IF node is empty newJson will be override to root nodes */
        var node = $('#' + containerDiv).children('form').attr('data-node');
        var key = 'data-key';
        var newJson = getInputJson(formId, key, 'is-object', 'multilevel');
        var type = $(formId).attr('data-type');
        var finalJson = mergeJson(projectAddJson, node, newJson, type);
        if (ID == 'save') {
            showLoader();
            $.post(base_url + 'ajax/projectAdd', {action: $('#action').val(), requestJson: finalJson})
                    .done(function (data) {
                        console.log(data);
                        window.location.href = base_url + 'projects';
                    }); 
        } else if (ID == 'update-eligibility-criteria-job-master') {
            finalJson = JSON.parse(finalJson);
            finalJson = finalJson.ProjectAttributes.EligibilityCriteria;
            jobMasterAddJson.JobMasterSpecs.EligibilityCriteria = finalJson; 
            updateEligibiltyCriteriaJobMaster();
        }
    }
});
$('body').on('click', '#newcondition', function (e) {
    var divId = $('.ec-container').parent().children('div').last().attr('id').replace('level_', '');
    if (!validateBlockOfCondition("level_" + divId)) {
        return false;
    }
    var allowablePropertyHtml = "";
    if (!$(this).hasClass("update-jobmaster-add-new-condition")) {
        allowablePropertyHtml = $("#level_0").find(".allowable-property-type-input").html();
    }
    var newPosition = parseInt(divId) + 1;
    var joinHtml = '<div class="form-group remove-condition-button-container" style="margin-top: -7px;"><a class="pull-right remove-condition-button" data-remove-id="' + newPosition + '">Remove this Condition</a></div><h4 style="margin-top: -7px;" class="eligibility-condition-heading">Condition - ' + (newPosition + 1) + '</h4><div class="form-group"><label for="JoinType" class="col-sm-2 control-label">Join Type</label><div class="col-sm-10">  <select class="form-control JoinType" data-key="JoinType" data-selected="" is-object="true"><option value="AND"> AND </option><option value="OR"> OR </option></select></div></div>';
    var defaultNewConditionLayoutFirstTwoFields = '<div class="form-group table-name-input">' + $("#level_0").children().first().html() + '</div><div class="form-group condition-group-input">' + $("#level_0").children().first().next().html() + '</div>';
    var defaultNewConditionLayoutLastTwoFields = '<div class="form-group allowable-property-type-input">'
            + allowablePropertyHtml
            + '</div><div class="form-group target-project-input">'
            + $("#level_0").find(".target-project-input").html() + '</div>';
    var defaultNewConditionLayoutBaseFields = defaultNewConditionLayoutFirstTwoFields + '<div class="form-group">\n\
                                        <label for="operatorName" class="col-sm-2 control-label">Select Operator <span>*</span></label>\n\
                                        <div class="col-sm-10">\n\
                                            <select class="form-control" id="operatorName" data-key="Operator" is-object="true">\n\
                                                <option value="">--SELECT--</option>\n\
                                                <option value="<"> < </option>\n\
                                                <option value="<="> <= </option>\n\
                                                <option value=">"> > </option>\n\
                                                <option value=">="> >= </option>\n\
                                                <option value="<>"> <> </option>\n\
                                                <option value="="> = </option> \n\
                                            </select>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div class="form-group value-type-input">\n\
                                        <label for="valueType" class="col-sm-2 control-label">Value Type <span>*</span></label>\n\
                                        <div class="col-sm-10">\n\
                                            <select class="form-control typeValueSelectorManipulation" id="valueType" data-key="ValueType" is-object="true">\n\
                                                <option value="">--SELECT--</option>\n\
                                                <option value="Single">Single</option>\n\
                                            </select>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div class="form-group value-condition-based-input" style="display:none;">\n\
                                        <label for="value" class="col-sm-2 control-label">Value <span>*</span></label>\n\
                                        <div class="col-sm-10 dtype" ></div>\n\
                                    </div>' + defaultNewConditionLayoutLastTwoFields + '';
    /*REMOVE BUTTON HTML TEMPLATE WITH CUSTOM ATTRIBUTE AND CLASS*/
    var removeButtonHtml = '';
    $('.ec-container').parent().append('<div class="border-bottom ec-container" id="level_' + newPosition + '">' + joinHtml + '' + defaultNewConditionLayoutBaseFields + '</div>');
    checkRemoveCondition();
});
$('body').on('click', '#removecondition', function (e) {
    if ($('.ec-container').length > 1) {
        $('.ec-container').parent().children('div').last().remove();
    }
    checkRemoveCondition();
});
/* REMOVE BUTTON EVENT MANIPULATION STARTS HERE*/
$('body').on('click', '.remove-condition-button', function () {
    if (confirm("Are you sure that you want to remove this condition ?")) {
        var currentDivSelector = '#level_' + $(this).attr("data-remove-id");
        $(currentDivSelector).remove();
        checkRemoveCondition();
    }
});
/* REMOVE BUTTON EVENT MANIPULATION ENDS HERE*/

/*TYPE VALUE EVENT MANIPULATION STARTS HERE*/
$('body').on('change', '.typeValueSelectorManipulation', function () {
    renderValueInputs(this);
});
/*TYPE VALUE EVENT MANIPULATION ENDS HERE*/

/*DYNAMIC ATTRIBUTE SELECTOR ON OTHER INPUT MANIPULATION [STARTS HERE] FOR RESPETIVE DIV */
$('body').on('change', '.dynamic-condition-attribute-selector', function () {
    var currentDivLevel = $(this).parent().parent().parent().attr("id");
    var otherInputSelector = '<div class="form-group others-input">\n\
     <label for="value" class="col-sm-2 control-label">Other</label>\n\
     <div class="col-sm-10 dtype" >\n\
     <input type="text" class="form-control others-input-field"/>\n\
        </div>\n\
     </div>';
    if ($(this).find('option:selected').text() == "Other") {
        $("#" + currentDivLevel).find(".dynamic-condition-selector").after(otherInputSelector);
        var otherContent = $("#" + currentDivLevel).find(".condition-group-input").children().children().attr("data-prefilled");
        if (otherContent != undefined) {
            $("#" + currentDivLevel).find(".others-input").children().children().val(otherContent.split(".")[2]);
        }
    } else if ($("#" + currentDivLevel).find(".dynamic-condition-selector").next().is(".others-input") === true) {
        $("#" + currentDivLevel).find(".dynamic-condition-selector").next().remove();
    }
});
/*DYNAMIC ATTRIBUTE SELECTOR ON OTHER INPUT MANIPULATION [ENDS HERE] FOR RESPETIVE DIV */

/*DYNAMIC NUMBER/DATE FIELD BASED MANIPULATION [STARTS HERE]*/
$("body").on("change", ".date-type-input-field", function () {
    var preFilledContent = [];
    preFilledContent["dateOrNumber"] = "";
    preFilledContent["unitOfDate"] = "";
    var preFillDateNumber = function (dataPreSelected) {
        if (dataPreSelected != "") {
            var checkDateFormat = Date.parse(dataPreSelected);
            if (isNaN(checkDateFormat)) {
                preFilledContent["unitOfDate"] = dataPreSelected.split('')[(dataPreSelected.split('')).length - 1];
                preFilledContent["dateOrNumber"] = dataPreSelected.substring(1, (dataPreSelected.length - 1));
            } else {
                preFilledContent["dateOrNumber"] = dataPreSelected;
            }
        }
    };
    var currentSelectedValue = $(this).val();
    preFillDateNumber($(this).attr("data-date-pre-selected"));
    var currentWorkingDiv = "#" + $(this).parent().parent().parent().attr("id");
    if (currentSelectedValue !== "") {
        var printValueRelatedInputField = function (selectorType) {
            var labelOfExtraField = function (selectorType) {
                if (selectorType === "number") {
                    return "Select Number";
                } else {
                    return "Select Date";
                }
            }
            var renderExtraField = function (toRenderOnSpecificValue) {
                if (toRenderOnSpecificValue === "number") {
                    return '<div class="col-sm-3 number-sub-unit-type-input">\n\
                            <select class="form-control">\n\
                                <option value="">--SELECT--</option>\n\
                                <option value="D" ' + (preFilledContent["unitOfDate"] === 'D' ? "selected='selected'" : "") + '>Day</option>\n\
                                <option value="M" ' + (preFilledContent["unitOfDate"] === 'M' ? "selected='selected'" : "") + '>Month</option>\n\
                                <option value="Y" ' + (preFilledContent["unitOfDate"] === 'Y' ? "selected='selected'" : "") + '>Year</option>\n\
                            </select>\n\
                        </div>';
                } else {
                    return '';
                }
            }
            return '<div class="form-group ' + selectorType + '-sub-type-input">\n\
     <label for="value" class="col-sm-2 control-label">' + labelOfExtraField(selectorType) + '</label>\n\
     <div class="col-sm-' + (selectorType === "number" ? 7 : 10) + ' ' + selectorType + '-sub-type-input-container">\n\
            <input type="' + (selectorType === "number" ? "number" : "text") + '" class="form-control ' + selectorType + '-sub-input-field" id="' + selectorType + '-sub-id-input-field" value="' + preFilledContent["dateOrNumber"] + '"/>\n\
     </div>'
                    + renderExtraField(selectorType) + '\n\
     </div>';
        };
        $(currentWorkingDiv).find(".date-sub-type-input").remove();
        $(currentWorkingDiv).find(".number-sub-type-input").remove();
        $(this).parent().parent().after(printValueRelatedInputField(currentSelectedValue));
    } else {
        $(currentWorkingDiv).find(".date-sub-type-input").remove();
        $(currentWorkingDiv).find(".number-sub-type-input").remove();
    }
});
/*DYNAMIC NUMBER/DATE FIELD BASED MANIPULATION [ENDS HERE]*/




function checkRemoveCondition() {
    if ($('.ec-container').length > 1)
        $('#removecondition').removeClass('hide');
    else
        $('#removecondition').addClass('hide');
}

function getInputJson(containerDiv, key, condition, isMultilevel) {
    var newJson = {}; 
    if ($(containerDiv).attr(isMultilevel) == 'false') {
        jQuery(containerDiv).find(':input').each(function (i, elem) {
            if ($.trim(condition) != '') {
                if (this.getAttribute(condition) == 'true')
                    newJson[this.getAttribute(key)] = this.value;
            } else {
                newJson[this.id] = this.value;
            }
        });
    } else {
        var arr = [];
        jQuery(containerDiv).find('.ec-container').each(function (j, elem) {
            var subArray = {};
            $(this).find(':input').each(function (i, elem) {
                if ($.trim(condition) != '') {
                    if (this.getAttribute(condition) == 'true') { 
                        subArray[this.getAttribute(key)] = this.value; 
                    }
                } else {
                    subArray[this.id] = this.value;
                }
            });
            /* DYNAMIC INJECTION IN JSON FOR VALUE TYPE, ATTRIBUTE MANIPULATION, TARGET PROJECT AND DATE/NUMBER MANIPULATION [STARTS HERE] */
            if ($(this).find(".dynamic-condition-selector").children().children().val() != undefined) {
                var attributeSelectorValue = $(this).find(".dynamic-condition-selector").children().children().val();
                var attributeSelectorOptionSelectedValue = $(this).find(".dynamic-condition-selector").children().children().find("option:selected").text();
                if (attributeSelectorOptionSelectedValue == "Other") {
                    subArray["Condition"] = $(this).find(".condition-group-input").children().children().val() + "."
                            + $(this).find(".dynamic-condition-selector").next().children().children().val();
                } else {
                    subArray["Condition"] = $(this).find(".dynamic-condition-selector").children().children().val();
                }
            }
            if ($(this).find(".date-type-input").children().children().val() != undefined) {
                subArray["ValueType"] = "Eval";
                var finalEvalPhpOutput = "";
                var dateTypeInput = $(this).find(".date-type-input").children().children().val();
                switch (dateTypeInput) {
                    case "date":
                        var selectedDateValue = $(this).find(".date-sub-type-input").children().children().val();
                        subArray["ValueType"] = "Single";
                        finalEvalPhpOutput = selectedDateValue;
                        subArray["Value"] = finalEvalPhpOutput;
                        break;
                    case "number":
                        var convertNumberToISO = function (currentObjectReference) {
                            var numberDateValue = $(currentObjectReference).find(".number-sub-type-input").find(".number-sub-type-input-container").children().val();
                            var numberDateUnit = $(currentObjectReference).find(".number-sub-type-input").find(".number-sub-unit-type-input").children().val();
                            return "P" + numberDateValue + numberDateUnit;
                        };
                        finalEvalPhpOutput = "(new DateTime())->sub(new DateInterval('" + convertNumberToISO(this) + "'))->format('Y-m-d');";
                        subArray["Value"] = {
                            "php": finalEvalPhpOutput,
                        };
                        break;
                }
            }
            subArray["TargetProject"] = $(this).find(".target-project-input").children().children().val();
            /* DYNAMIC INJECTION IN JSON FOR VALUE TYPE, ATTRIBUTE MANIPULATION, TARGET PROJECT AND DATE/NUMBER MANIPULATION [ENDS HERE] */
            arr.push(subArray);
        });
        newJson = arr;
    }
    return newJson;
}

/* add / merge json data according to each step to the default json skelton */

function mergeJson(defaultJson, node, newJson, type) {
    // console.log('node='+node);
    if ($.trim(node) != '') {
        defaultJson.ProjectAttributes.EligibilityCriteria = newJson;
    } else {
        for (var _obj in defaultJson)
            defaultJson[_obj ] = defaultJson[_obj];
        for (var _obj in newJson)
            defaultJson[_obj ] = newJson[_obj];
    }
    var AllowableProperties = [];
    $.each(projectAddJson.ProjectAttributes.EligibilityCriteria, function (key, value) {
        var allowablePropertyTmp = {
            "Property": projectAddJson.ProjectAttributes.EligibilityCriteria[key].Condition,
            "PropertyType": projectAddJson.ProjectAttributes.EligibilityCriteria[key].PropertyType
        };
        delete projectAddJson.ProjectAttributes.EligibilityCriteria[key].PropertyType;
        if (projectAddJson.ProjectAttributes.EligibilityCriteria[key].TargetProject == "") {
            delete projectAddJson.ProjectAttributes.EligibilityCriteria[key].TargetProject;
        }
        if (projectAddJson.ProjectAttributes.EligibilityCriteria[key].ValueType == "Eval") {
            if (projectAddJson.ProjectAttributes.EligibilityCriteria[key].Value.php == "") {
                delete projectAddJson.ProjectAttributes.EligibilityCriteria[key].Value.php;
            } else if (projectAddJson.ProjectAttributes.EligibilityCriteria[key].Value.java == "") {
                delete projectAddJson.ProjectAttributes.EligibilityCriteria[key].Value.java;
            }
        }
        AllowableProperties.push(allowablePropertyTmp);
    });
    defaultJson.ProjectAttributes.AllowableProperties = AllowableProperties;
    return JSON.stringify(defaultJson);
}

function showLoader() {
    $('.loader').removeClass('hide');
}

function hideLoader() {
    $('.loader').addClass('hide');
}

